<?php

	class CCommonTagTemplate extends CBaseTagTemplate
	{
		function CCommonTagTemplate($indata, $intype, $inenc, $cache="")
		{
			$this->CBaseTagTemplate($indata, $intype, $inenc, $cache);
		}

		function CommonReplace()
		{
			// システム全体で共通置換するものはここに記述

			// ページ内リンク用
			$this->Replace('uri', $_SERVER['REQUEST_URI']);

			// 自分
			$this->Replace('prog', $_SERVER['SCRIPT_NAME']);

			// プロトコル移動用
			$this->Replace('SERVER_NAME_HTTP', CRT_SERVER_NAME_HTTP);
			$this->Replace('SERVER_NAME_HTTPS', CRT_SERVER_NAME_HTTPS);

		}


	}
?>