<?php

	class CCommonWebappLogic extends CBaseWebappLogic
	{

		function CCommonWebappLogic($config_file)
		{
			// スーパークラスコンストラクタ
			$this->CBaseWebappLogic($config_file);

		}


		function Execute()
		{

			return parent::Execute();
		}


		
		function errMsgGen($type, $params)
		{
			switch($type)
			{
				case CRT_ERRMSG_NODATA:
					$sysmsg = '<$name$> is required.<br />';
					break;
				case CRT_ERRMSG_NOSEL:
					$sysmsg = '<$name$> is required.<br />';
					break;
				case CRT_ERRMSG_INVTYPE:
					if(Mis_empty($params['zhk']) != 1)
					{
						$sysmsg = '<$name$>は' . $params['zhk'] . 'で入力してください<br />';
					}else{
						$sysmsg = '<$name$> is wrong.<br />';
					}
					break;
				case CRT_ERRMSG_DATALEN:
					$sysmsg = '<$name$> should be less than <$len$> chars.<br />';
					break;
				case CRT_ERRMSG_FILELEN:
					$sysmsg = '<$name$>: filesize surpasses its limit(<$len$> bytes).<br />';
					break;
				case CRT_ERRMSG_RANGE:
					$sysmsg = '<$name$> unkown entry.<br />';
					break;
				case CRT_ERRMSG_INVEXT:
					$sysmsg = 'Invalid file extension.(<$name$>)<br />';
					break;
				case CRT_ERRMSG_INVMETHOD:
					$sysmsg = 'method now allowed<br />';
					break;
				default:
					$sysmsg = 'Unexpected error<br />';
					break;
			}
		
			$msg = new CTagTemplate($sysmsg, CTAGTEMPLATE_INTYPE_DATA, CTAGTEMPLATE_ENCODE_UTF8);
			if(is_array($params))
			{
				foreach($params as $name => $value)
				{
					$msg->Replace($name, $value);
				}
			}
		
			return $msg->Generate();
		}
		
	}
?>