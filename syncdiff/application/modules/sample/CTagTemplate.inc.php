<?php

	class CTagTemplate extends CCommonTagTemplate
	{
		// コンストラクタ
		function CTagTemplate($indata, $intype, $inenc, $cache="")
		{
			$this->CCommonTagTemplate($indata, $intype, $inenc, $cache);
		}

		function CommonReplace()
		{
			global $g_name_flgmark;

			if(Mis_empty($_SERVER['HTTPS']) != 1)
			{
				$proto = 'https://';
			}else{
				$proto = 'http://';
			}
			$basetag = $proto . $_SERVER['HTTP_HOST'] . XD_SUBSYS_DIRNAME . '/';
			$this->Replace('BASEURL', '<base href="' . $basetag . '" />');

			$this->Replace('host', $_SERVER['HTTP_HOST']);


			// 親クラス
			parent::CommonReplace();
			
			
		}



	}
?>