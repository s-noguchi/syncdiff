<?php

	/**
	 * modules.inc
	 *
	 * モジュールリストファイル
	 *
	 **/

	// モジュール名とフォルダを関連付ける

	// 一次要素キー：タスク名
	// 一次要素値　：モジュール配列
	// 二次要素キー：モジュール名
	// 二次要素値　：モジュール名;フラグ
	//  ※フラグはオプション　省略時はセミコロンも省略

	// モジュール一覧
	$DISPATCH_CLASS_NAMES = array(
		'main'		=> array(
					'error'				=> 'A000_main_error;0',
					'index'				=> 'A001_main_index;0'
		), 
		'greeting'	=> array(
					'HelloWorld'		=> '001_greeting_HelloWorld;0'
	)
	);

	// デフォルトコンテキスト
	if((Mis_empty($name_task) == 1) || (Mis_empty($name_module) == 1))
	{
		$name_task = 'main';
		$name_module = 'index';
	}



?>