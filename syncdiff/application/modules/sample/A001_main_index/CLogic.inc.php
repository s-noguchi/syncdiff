<?php

	class CLogic extends CWebappLogic
	{

		// Constructor
		function CLogic($config_file)
		{
			// Super class's constructor
			$this->CWebappLogic($config_file);

			// Input encoding
			$this->SetInEncoding(INPUT_ENCODING_UTF8);

			// Permitted method
			$this->SetMethod(CRT_HTTP_METHOD_BOTH);

			// Start
			$this->doTask();

		}

		function initParam()
		{
			// Initialize in super class initialization
			if(!parent::initParam())
			{
				return false;
			}

			/*
			$allows = array('csv', 'txt');
			$this->SetUploadConfig(1, 0, DATA_DIR, DATA_DIR, DATA_DIR, DATA_DIR, $allows);
			*/


			return true;

		}


		function Execute()
		{

			// Call super class Execute
			if(!parent::Execute())
			{
				return true;
			}

			return true;
		}


		function additionalInputValidation($errmsg)
		{
			return $errmsg;
		}

		function handleInvalidate()
		{
			$this->viewRedirect('main', 'error', array('msg' => $this->m_InvalidErrMsg));
			return false;
		}



	}
?>