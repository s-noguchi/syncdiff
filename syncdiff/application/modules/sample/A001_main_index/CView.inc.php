<?php

	class CView extends CWebappView
	{

		function CView($context, $config_file, $template_file, &$testobj)
		{
			// Super class's constructor
			$this->CWebappView($context, $config_file, $template_file, $testobj);

			// Start
			$this->doTask();
		}

		function initParam()
		{
			// Initialize in super class initialization
			if(!parent::initParam())
			{
				return false;
			}

			// template encoding
			$this->SetTemplateEncoding(TEMPLATE_ENCODING_UTF8);

			return true;

		}


		function Display()
		{

			if($this->m_Template)
			{

				// Error message part
				if(Mis_empty($this->m_Result['msg']) != 1)
				{
					$msg_area = new CTagTemplate($this->m_Template->GetContainer('msg_area'), CTAGTEMPLATE_INTYPE_DATA, CTAGTEMPLATE_ENCODE_UTF8);
					$msg_area->Replace('msg', $this->m_Result['msg']);
					$this->m_Template->ReplaceContainer('msg_area', $msg_area->Generate());
				}else{
					$this->m_Template->ContainerDeactivate('msg_area');
				}


				// Commonly replacing
				$this->m_Template->CommonReplace();

				// Flush to client
				$this->m_Template->PrintHTML();
			}

			return true;
		}
	}
?>