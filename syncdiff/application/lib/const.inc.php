<?php

	// ---------------------------------------------
	// const.inc
	// 定数管理ファイル
	//
	// 制作：株式会社オフィストゥルーワン
	// 野口真一 s-noguchi@trueone.co.jp
	// 2005/05/01
	// ---------------------------------------------

	// ---------------------------------------------
	// Constraint
	// ---------------------------------------------

	// デバッグモード ( true / false )
	define("CRT_ON_DEBUG", true, FALSE);


	// INエンコーディング
	define("INPUT_ENCODING_EUC", 'EUC-JP', FALSE);
	define("INPUT_ENCODING_SJIS", 'SJIS', FALSE);
	define("INPUT_ENCODING_UTF8", 'UTF-8', FALSE);
	define("INPUT_ENCODING", INPUT_ENCODING_UTF8, FALSE);

	// 出力テンプレートエンコーディング
	define("TEMPLATE_ENCODING_EUC", 'EUC-JP', FALSE);
	define("TEMPLATE_ENCODING_SJIS", 'SJIS', FALSE);
	define("TEMPLATE_ENCODING_UTF8", 'UTF-8', FALSE);
	define("TEMPLATE_ENCODING", TEMPLATE_ENCODING_UTF8, FALSE);


	// プロトコル
	define("CRT_PROTOCOL_HTTP", 'http://', FALSE);
	define("CRT_PROTOCOL_HTTPS", 'https://', FALSE);

	// メソッド
	define("CRT_HTTP_METHOD_GET", 'GET', FALSE);
	define("CRT_HTTP_METHOD_POST", 'POST', FALSE);
	define("CRT_HTTP_METHOD_BOTH", 'GETPOST', FALSE);


	// ホスト名等
	define("CRT_SERVER_NAME", $_SERVER['HTTP_HOST'], FALSE);
	define("CRT_SERVER_NAME_HTTP", CRT_PROTOCOL_HTTP . CRT_SERVER_NAME, FALSE);
	define("CRT_SERVER_NAME_HTTPS", CRT_PROTOCOL_HTTPS . CRT_SERVER_NAME, FALSE);


	// 環境振り分け
	if(is_dir('/var/www/vh/mta.rnaitest.trueone.co.jp'))
	{

		// 案件開発環境
		define("CRT_DATABASE_HOST", 'localhost', FALSE);
		define("CRT_DATABASE_PORT", 5432, FALSE);
		define("CRT_DATABASE_NAME", '', FALSE);
		define("CRT_DATABASE_USER", '', FALSE);
		define("CRT_DATABASE_PWD", '', FALSE);

	}else{

		// 本番環境
		define("CRT_DATABASE_HOST", 'localhost', FALSE);
		define("CRT_DATABASE_PORT", 5432, FALSE);
		define("CRT_DATABASE_NAME", '', FALSE);
		define("CRT_DATABASE_USER", '', FALSE);
		define("CRT_DATABASE_PWD", '', FALSE);

	

	}


	// データベース列挙
	define("CRT_DATABASE_SYSTEM_MYSQL", 'mysql', FALSE);
	define("CRT_DATABASE_SYSTEM_PGSQL", 'pgsql', FALSE);

	// データベース指定（利用するデータベースを上から選択して設定します）
	define("CRT_DATABASE_SYSTEM", CRT_DATABASE_SYSTEM_MYSQL, FALSE);

	// メール
	define("CRT_SENDMAIL", '/usr/sbin/sendmail', FALSE);


?>