<?php

	// セッション関連
	// セッションの利用環境を設定してください。
	// セッション名
	define("XD_SESSION_NAME", '_xdsesname', FALSE);
	define("XD_SESSION_LIFETIME", 0, FLASE);
	define("XD_SESSION_PATH", "/", FALSE);
	define("XD_SESSION_DOMAIN", "", FALSE);
	define("XD_SESSION_SECURE", false, FALSE);


	// なんらかの条件が一致したときに実行するようにする。
	if(false)
	{

		func_session_init();
	}



	function func_session_init()
	{
		// セッション設定
		session_set_cookie_params(XD_SESSION_LIFETIME, XD_SESSION_PATH, XD_SESSION_DOMAIN, XD_SESSION_SECURE);
		// セッション名
		session_name(XD_SESSION_NAME);

		// モバイル等クッキー以外のセッションの場合はGET/POST から取得
		// if(isset($_REQUEST[XD_SESSION_NAME]))
		// {
		// 	session_id($_REQUEST[XD_SESSION_NAME]);
		// }

		// セッション開始
		if(!session_start())
		{
			func_session_error();
		}

	}

	function func_session_error($msg = "")
	{
		echo '<!-- SESSION ERROR -->';
		echo '<!-- ' . $msg . ' -->';
		exit;
	}

?>