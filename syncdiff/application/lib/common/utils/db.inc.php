<?php
	// データベース接続
	// スクリプト開始時に実行されます。

	// グローバルDBハンドラ
	$db_conn = "";
	$g_snsmgr = "";

	// なんらかの条件が一致したときに実行するようにする。
	if(false)
	{
		func_db_connect();


	}



	function func_db_connect()
	{
		global $db_conn;
		global $g_snsmgr;

		// MySQL
		if(CRT_DATABASE_SYSTEM == CRT_DATABASE_SYSTEM_MYSQL)
		{

			$db_conn = mysql_connect(CRT_DATABASE_HOST, CRT_DATABASE_USER, CRT_DATABASE_PWD);
			if(!$db_conn)
			{
				func_database_error();
			}

			// データベース選択
			if(!mysql_select_db(CRT_DATABASE_NAME, $db_conn))
			{
				func_database_error();
				exit;
			}
			// エンコーディング選択 >= MySQL4.1
			if(!mysql_query("SET NAMES 'utf8'", $db_conn))
			{
				func_database_error();
			}
		}

		// PostgreSQL
		if(CRT_DATABASE_SYSTEM == CRT_DATABASE_SYSTEM_PGSQL)
		{
			$pgsql_constr = 'host=' . CRT_DATABASE_HOST . ' port=' . CRT_DATABASE_PORT . ' dbname=' . CRT_DATABASE_NAME .' user=' . CRT_DATABASE_USER.' password=' . CRT_DATABASE_PWD;

			$db_conn = pg_connect($pgsql_constr);
			if(!$db_conn)
			{
				func_database_error();
			}

		}

		// 接続失敗
		if(!$db_conn)
		{
			func_database_error();
		}



	}

	function func_database_error($msg = 'SYSTEM ERROR')
	{
		echo '<!-- DATABASE ERROR -->';
		echo '<!-- ' . $msg . ' -->';
		exit;
	}



?>