<?php
	//---------------------------------------------
	// WA_Auth.inc
	// ユーザー認証提供モジュール
	//---------------------------------------------

	//--------------------------------------------------------
	// WA_Auth_Basic
	// 当該PHPファイルに基本認証をかける
	//--------------------------------------------------------
	function WA_Auth_Basic($userid, $passwd, $title){
	
		if (!isset($_SERVER["PHP_AUTH_USER"])) {
			header("WWW-Authenticate: Basic realm=\"".$title."\"");
			header("HTTP/1.0 401 Unauthorized");
			die("Authorization Required");
		}else if (isset($_SERVER["PHP_AUTH_USER"])) {
			if($_SERVER["PHP_AUTH_USER"] != $userid || $_SERVER["PHP_AUTH_PW"] != $passwd ) {
			header('WWW-Authenticate: Basic realm="'.$title.'"');
			header('HTTP/1.0 401 Unauthorized');
			die("Authorization Required");
			}
		}
	}

	//--------------------------------------------------------
	// WA_Auth
	// アプリケーションの標準の方法でユーザー認証を行う
	// 正しい場合のみ1を返す。
	//--------------------------------------------------------
	function WA_Auth_User($userid, $passwd){
		// 配布時に方針決定後実装
		// 開発中は常に1を返す
		
		return 1;
	}
	
	//--------------------------------------------------------
	// WA_Auth_HashedID
	// 認証状態を表現するのCookie値のうち、
	// ユーザーIDから片方ハッシュで一意に特定できるハッシュ値
	// を生成する。
	//
	// 【重要】$key値はアプリケーションインストール時に
	// 秘密裏に決定しておくべきなるべく長い文字列
	// (8バイト未満のものはエラーで返す）
	//--------------------------------------------------------
	function WA_Auth_HashedID($userid, $key){
		if(Mis_empty($userid) == 1){
			return "";
		}
		if(Mis_empty($key) == 1){
			return "";
		}
		if(strlen($key) < 8){
			return "";
		}

		$src = $key . $userid;
		$dst = md5($src);

		return $dst;
	}

	//--------------------------------------------------------
	// WA_Auth_HashedIDByIP
	// 認証状態を表現するのCookie値のうち、
	// ユーザーIDから片方ハッシュで一意に特定できるハッシュ値
	// を生成する。IPアドレスを加味するためより端末識別に近い。
	//
	// 【重要】$key値はアプリケーションインストール時に
	// 秘密裏に決定しておくべきなるべく長い文字列
	// (8バイト未満のものはエラーで返す）
	//--------------------------------------------------------
	function WA_Auth_HashedIDByIP($userid, $key, $ipaddr){
		if(Mis_empty($userid) == 1){
			return "";
		}
		if(Mis_empty($key) == 1){
			return "";
		}
		if(Mis_empty($ipaddr) == 1){
			return "";
		}
		if(strlen($key) < 8){
			return "";
		}

		$src = $key . $userid . $ipaddr;
		$dst = md5($src);

		return $dst;
	}

	//--------------------------------------------------------
	// WA_Auth_HashedTERM
	// 認証状態を表現するのCookie値のうち、
	// ユーザーIDと、指定されたインターバル以内を示す値から
	// 一意に特定できるハッシュ値
	//
	// 【重要】$key値はアプリケーションインストール時に
	// 秘密裏に決定しておくべきなるべく長い文字列
	// (8バイト未満のものはエラーで返す）
	// $term は、認証の有効期間（秒）
	//--------------------------------------------------------
	function WA_Auth_HashedTERM($userid, $key, $interval){

		if(Mis_empty($userid) == 1){
			return "";
		}
		if(Mis_empty($key) == 1){
			return "";
		}
		if(Mis_empty($interval) == 1){
			return "";
		}
		if(strlen($key) < 8){
			return "";
		}
		if(Mchk_IsNumber($interval) != 1){
			return "";
		}
		if($interval < 60){
			// 最小インターバルは60秒
			return "";
		}

		$val =  WA_Auth_HashedInterval($interval, 0);

		$src = $key . $userid . $val;
		$dst = md5($src);

		return $dst;

	}

	//--------------------------------------------------------
	// WA_Auth_HashedInterval
	// 与えられたインターバルに基づいた時刻値を返す
	// $offset に指定された分だけ時刻をずらして取得する。
	//--------------------------------------------------------
	function WA_Auth_HashedInterval($interval, $offset){

		// 対象時刻を特定
		$time_target = time() + $interval * $offset;

		// 丸め
		$time_target = $time_target - ($time_target % $interval);

		return $time_target;
	}
		
	//--------------------------------------------------------
	// WA_Auth_ChkID
	// 与えられたIDと値が正しい組み合わせかどうかを認証
	//--------------------------------------------------------
	function WA_Auth_ChkID($userid, $userval, $key){

		if(Mis_empty($userid) == 1){
			return 0;
		}
		if(Mis_empty($userval) == 1){
			return 0;
		}
		if(Mis_empty($key) == 1){
			return 0;
		}
		if(strlen($key) < 8){
			return 0;
		}

		$src = $key . $userid;
		$testee = md5($src);

		if($testee == $userval){
			return 1;
		}else{
			return 0;
		}
	}

	//--------------------------------------------------------
	// WA_Auth_ChkIDByIP
	// 与えられたIDと値が正しい組み合わせかどうかを認証
	// IPアドレスを加味
	//--------------------------------------------------------
	function WA_Auth_ChkIDByIP($userid, $userval, $key, $ipaddr){

		if(Mis_empty($userid) == 1){
			return 0;
		}
		if(Mis_empty($userval) == 1){
			return 0;
		}
		if(Mis_empty($key) == 1){
			return 0;
		}
		if(Mis_empty($ipaddr) == 1){
			return 0;
		}
		if(strlen($key) < 8){
			return 0;
		}

		$src = $key . $userid . $ipaddr;
		$testee = md5($src);

		if($testee == $userval){
			return 1;
		}else{
			return 0;
		}
	}
		
	//--------------------------------------------------------
	// WA_Auth_ChkTERM
	// 与えられたIDと値が正しい組み合わせかどうかを認証
	//--------------------------------------------------------
	function WA_Auth_ChkTERM($userid, $userval, $interval, $key){

		
		if(Mis_empty($userid) == 1){
			return 0;
		}
		if(Mis_empty($userval) == 1){
			return 0;
		}
		if(Mis_empty($key) == 1){
			return 0;
		}
		if(Mis_empty($interval) == 1){
			return 0;
		}
		if($interval < 60){
			return 0;
		}
		if(strlen($key) < 8){
			return 0;
		}

		$term_prev = WA_Auth_HashedInterval($interval, -1);
		$term_curr = WA_Auth_HashedInterval($interval, 0);

		$src = $key . $userid . $term_curr;
		$testee = md5($src);

		if($testee == $userval){
			return 1;
		}

		$src = $key . $userid . $term_prev;
		$testee = md5($src);

		if($testee == $userval){
			return 1;
		}

		return 0;
	}


	//--------------------------------------------------------
	// WA_Auth_MAKE_COOKIE
	// ブラウザに送信する認証Cookieを生成する
	// $userid: ユーザー識別子
	// $interval: 認証状態有効期間基底値（最大、この値の倍の期間）
	// $key: 8バイト以上のサイト識別子（秘密キー）
	//--------------------------------------------------------
	function WA_Auth_MAKE_COOKIE($userid, $interval, $key){

		// Format;
		// ID<>HID<>HTERM

		if(Mis_empty($userid) == 1){
			return "";
		}
		if(Mis_empty($interval) == 1){
			return "";
		}
		if(Mchk_IsNumber($interval) != 1){
			return "";
		}
		if($interval < 60){
			return "";
		}
		if(Mis_empty($key) == 1){
			return "";
		}
		if(strlen($key) < 8){
			return "";
		}

		// デリミタをエスケープ
		$escape_src = '<>';
		$escape_dst = '&lt;&gt;';

		$userid = str_replace($escape_src, $escape_dst, $userid);

		$part_id = $userid;
		$part_hashedid = WA_Auth_HashedID($userid, $key);
		if(Mis_empty($part_hashedid) == 1){
			return "";
		}
		$part_hashedterm = WA_Auth_HashedTERM($userid, $key, $interval);
		if(Mis_empty($part_hashedterm) == 1){
			return "";
		}

		$dst = $part_id . '<>' . $part_hashedid . '<>' . $part_hashedterm;
		return $dst;

	}
	
	//--------------------------------------------------------
	// WA_Auth_AUTH_COOKIE
	// ブラウザから送信されたCookieを認証する
	// $userid: ユーザー識別子
	// $interval: 認証状態有効期間基底値（最大、この値の倍の期間）
	// $key: 8バイト以上のサイト識別子（秘密キー）
	// $userval: ユーザーCookie
	//--------------------------------------------------------
	function WA_Auth_AUTH_COOKIE($interval, $key, $userval){

		if(Mis_empty($interval) == 1){
			return 0;
		}
		if(Mchk_IsNumber($interval) != 1){
			return 0;
		}
		if($interval < 60){
			return 0;
		}
		if(Mis_empty($key) == 1){
			return 0;
		}
		if(strlen($key) < 8){
			return 0;
		}

		if(Mis_empty($userval) == 1){
			return 0;
		}

		$testees = split('<>', $userval);

		// デリミタを復帰
		$escape_src = '<>';
		$escape_dst = '&lt;&gt;';
		$testees[0] = str_replace($escape_dst, $escape_src, $testees[0]);


		
		if(Mis_empty($testees[0]) == 1){
			return 0;
		}
		if(Mis_empty($testees[1]) == 1){
			return 0;
		}
		if(Mis_empty($testees[2]) == 1){
			return 0;
		}

		if(WA_Auth_ChkID($testees[0], $testees[1], $key) != 1){
			return 0;
		}
		if(WA_Auth_ChkTERM($testees[0], $testees[2], $interval, $key) != 1){
			return 0;
		}
		
		return 1;
	}


	//--------------------------------------------------------
	// WA_Auth_MAKE_COOKIE_ByIP
	// ブラウザに送信する認証Cookieを生成する
	// $userid: ユーザー識別子
	// $interval: 認証状態有効期間基底値（最大、この値の倍の期間）
	// $key: 8バイト以上のサイト識別子（秘密キー）
	// $ipaddr: IPアドレス
	//--------------------------------------------------------
	function WA_Auth_MAKE_COOKIE_ByIP($userid, $interval, $key, $ipaddr){

		// Format;
		// ID<>HID<>HTERM

		if(Mis_empty($userid) == 1){
			return "";
		}
		if(Mis_empty($interval) == 1){
			return "";
		}
		if(Mchk_IsNumber($interval) != 1){
			return "";
		}
		if(Mis_empty($ipaddr) == 1){
			return "";
		}
		if($interval < 60){
			return "";
		}
		if(Mis_empty($key) == 1){
			return "";
		}
		if(strlen($key) < 8){
			return "";
		}

		// デリミタをエスケープ
		$escape_src = '<>';
		$escape_dst = '&lt;&gt;';

		$userid = str_replace($escape_src, $escape_dst, $userid);

		$part_id = $userid;
		$part_hashedid = WA_Auth_HashedIDByIP($userid, $key, $ipaddr);
		if(Mis_empty($part_hashedid) == 1){
			return "";
		}
		$part_hashedterm = WA_Auth_HashedTERM($userid, $key, $interval);
		if(Mis_empty($part_hashedterm) == 1){
			return "";
		}

		$dst = $part_id . '<>' . $part_hashedid . '<>' . $part_hashedterm;
		return $dst;

	}
	
	//--------------------------------------------------------
	// WA_Auth_AUTH_COOKIE_ByIP
	// ブラウザから送信されたCookieを認証する
	// $userid: ユーザー識別子
	// $interval: 認証状態有効期間基底値（最大、この値の倍の期間）
	// $key: 8バイト以上のサイト識別子（秘密キー）
	// $userval: ユーザーCookie
	// $ipaddr: IPアドレス
	//--------------------------------------------------------
	function WA_Auth_AUTH_COOKIE_ByIP($interval, $key, $userval, $ipaddr){

		if(Mis_empty($interval) == 1){
			return 0;
		}
		if(Mchk_IsNumber($interval) != 1){
			return 0;
		}
		if($interval < 60){
			return 0;
		}
		if(Mis_empty($key) == 1){
			return 0;
		}
		if(strlen($key) < 8){
			return 0;
		}

		if(Mis_empty($userval) == 1){
			return 0;
		}
		if(Mis_empty($ipaddr) == 1){
			return 0;
		}

		$testees = split('<>', $userval);

		// デリミタを復帰
		$escape_src = '<>';
		$escape_dst = '&lt;&gt;';
		$testees[0] = str_replace($escape_dst, $escape_src, $testees[0]);


		
		if(Mis_empty($testees[0]) == 1){
			return 0;
		}
		if(Mis_empty($testees[1]) == 1){
			return 0;
		}
		if(Mis_empty($testees[2]) == 1){
			return 0;
		}

		if(WA_Auth_ChkIDByIP($testees[0], $testees[1], $key, $ipaddr) != 1){
			return 0;
		}
		if(WA_Auth_ChkTERM($testees[0], $testees[2], $interval, $key) != 1){
			return 0;
		}
		
		return 1;
	}	
	
	
	function Lite_MakeKey($user_id, $key)
	{
		// Format userid<>md5
		
		$sTmp = $user_id . $key;
		return md5($sTmp);
	}
	
	function Lite_AuthKey($user_id, $key, $inkey)
	{
		if($inkey == Lite_MakeKey($user_id, $key))
		{
			return 1;
		}else{
			return 0;
		}
	}
	
		
?>