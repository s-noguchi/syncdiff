<?php

	/**
	 * エラーハンドリング
	 */

	// エラーハンドラセット
	set_error_handler('func_error_handler');

	// エラー定数
	define("ERRCST_NOTIFY_TO", 			'error@s-noguchi.trueone.co.jp', FALSE);
	define("ERRCST_NOTIFY_SUB", 		'[ERROR NOTICE]', FALSE);
	define("ERRCST_NOTIFY_FROM", 		'web@dev.trueone.co.jp', FALSE);

	define("ERRCST_ERROR_LOG", 			MISC_DIR . '/tmp/php_error_log', FALSE);





	function func_error_handler($errno, $errstr, $errfile, $errline, $variables)
	{

		// 定義済みエラーステータス
 		$errortype = array (
               E_ERROR              => 'Error',
               E_WARNING            => 'Warning',
               E_PARSE              => 'Parsing Error',
               E_NOTICE            => 'Notice',
               E_CORE_ERROR        => 'Core Error',
               E_CORE_WARNING      => 'Core Warning',
               E_COMPILE_ERROR      => 'Compile Error',
               E_COMPILE_WARNING    => 'Compile Warning',
               E_USER_ERROR        => 'User Error',
               E_USER_WARNING      => 'User Warning',
               E_USER_NOTICE        => 'User Notice',
               E_STRICT            => 'Runtime Notice',
               E_RECOVERABLE_ERROR  => 'Catchable Fatal Error'
		);

		// 処理すべきエラーハンドラ
		$error_procs = array(E_ERROR, E_WARNING, E_PARSE, E_USER_ERROR, E_USER_WARNING);

		if(!in_array($errno, $error_procs))
		{
//			return;
		}



		// 日時
		$datetime = date('Y-m-d H:i:s');

		// メッセージ
		$msg  = "";
		$msg .= $datetime . "\n\n";

		$msg .= 'ERRSTR:' . "\n";
		$msg .= $errno . "\n";
		$msg .= $errstr . "\n\n";

		$msg .= 'LOCATION:' . "\n";
		$msg .= $errfile . ':' . $errline . "\n\n";

		$msg .= 'VARS:' . "\n";
		$msg .= print_r($variables, true) . "\n\n";

		$msg .= "_GET:" . "\n";
		$msg .= print_r($_GET, true) . "\n\n";
		$msg .= "_POST:" . "\n";
		$msg .= print_r($_POST, true) . "\n\n";
		$msg .= "_SESSION:" . "\n";
		$msg .= print_r($_SESSION, true) . "\n\n";
		$msg .= "_SERVER:" . "\n";
		$msg .= print_r($_SERVER, true) . "\n\n";
		$msg .= "_COOKIE:" . "\n";
		$msg .= print_r($_COOKIE, true) . "\n\n";

		// エラー通知
		//func_error_notify($msg);
		// エラー記録
		func_error_record($msg);

		// 終了処理
		if(($errno == E_ERROR) || ($errno == E_USER_ERROR))
		{
			func_error_terminate($msg);
		}
	}



	/**
	 * エラー通知
	 *
	 * @param unknown_type $msg
	 */
	function func_error_notify($msg)
	{

		// 最低限動作確保のため低レベル関数での実施
		$msg = mb_convert_encoding($msg, 'JIS', 'EUC-JP');

		$additionals = "";
		$additionals .= 'Content-type: text/plain;charset=iso-2022-jp' . "\n";
		$additionals .= 'From: ' . ERRCST_NOTIFY_FROM . "\n";

		 mail(ERRCST_NOTIFY_TO, ERRCST_NOTIFY_SUB, $msg, $additionals);

	}

	/**
	 * エラー記録
	 *
	 * @param unknown_type $msg
	 * @return unknown
	 */
	function func_error_record($msg)
	{

		$line_sep = '------------------------';

		$fp = @fopen(ERRCST_ERROR_LOG, "a");
		if(!$fp)
		{
			return false;
		}else{
			flock($fp, 2);
		}
		fputs($fp, $msg, strlen($msg));
		fputs($fp, $line_sep, strlen($line_sep));
		fclose($fp);

		return true;

	}


	// ここで終了すべき処理をして終了
	function func_error_terminate($msg)
	{
		echo $msg;
		echo 'SYSTEM ERROR';
		exit;
	}

?>