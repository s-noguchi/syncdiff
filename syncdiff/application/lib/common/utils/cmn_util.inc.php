<?php

	/**
	 * cmn_util.inc
	 * 汎用サブルーチン集
	 *
	 * システム全体から利用されるグローバルな汎用サブルーチン集
	 *
	 **/

	//--------------------------------------------------------
	// Mis_empty
	// 与えられた値が、空白文字以外を含むかどうかを返す
	//--------------------------------------------------------
	function Mis_empty($param)
	{
		if(strlen($param) == 0)
		{
			return 1;
		}
		$param = str_replace(" ", "", $param);
		$param = str_replace("\r\n", "", $param);
		$param = str_replace("\n", "", $param);
		$param = str_replace("\r", "", $param);
		$param = str_replace("\t", "", $param);

		if($param == ''){
			return 1;
		}else{
			return 0;
		}
	}
	//--------------------------------------------------------
	// Mis_empty
	// 与えられた値が、空白文字以外を含むかどうかを返す
	//--------------------------------------------------------
	function Mis_emptyZero($param)
	{
		if(strlen($param) == 0)
		{
			return 1;
		}
		$param = str_replace(" ", "", $param);
		$param = str_replace("\r\n", "", $param);
		$param = str_replace("\n", "", $param);
		$param = str_replace("\r", "", $param);
		$param = str_replace("\t", "", $param);

		if(strlen($param) == 0){
			return 1;
		}else{
			return 0;
		}
	}

	//--------------------------------------------------------
	// Mis_array
	// 配列でないか、もしくは要素0の配列だった場合0
	//--------------------------------------------------------
	function Mis_array($param)
	{
		if(!is_array($param))
		{
			return 0;
		}

		if(count($param)  == 0)
		{
			return 0;
		}

		return 1;

	}



	//--------------------------------------------------------
	// Mg_collect
	// ディレクトリ内で指定時間よりも前に作成されたファイルを削除
	// $theDir: 対象ディレクトリ
	// $interval: 猶予期間
	//--------------------------------------------------------
	function Mg_collect($theDir, $interval)
	{
		if(!file_exist($theDir))
		{
			return 0;
		}

		$target_base = Mslash_trailling($theDir);

		// ディレクトリを開く
		if ($dir = @opendir($theDir))
		{
            while (($file = readdir($dir)) !== false)
            {
            	// 自己と上位ディレクトリは除外
                if(($file != '.') && ($file != '..'))
                {
                	// 対象
					$target = $target_base . $file;

					$fstamp = filemtime($target);
					if($fstamp + $interval > time())
					{
						unlink($target);
					}
				}
			}
		}else{
			return 0;
		}

		return 1;
	}

	//--------------------------------------------------------
	// Mr_rmdir
	// ディレクトリを再帰的に削除する
	//--------------------------------------------------------
	function Mr_rmdir($theDir, &$status)
	{
		if(!file_exists($theDir))
		{
			return 1;
		}

		$target_base = Mslash_trailing($theDir);

		// ディレクトリを開く
		if ($dir = @opendir($theDir))
		{
            while (($file = readdir($dir)) !== false)
            {
            	// 自己と上位ディレクトリは除外
                if(($file != '.') && ($file != '..'))
                {
                	// 対象
					$target = $target_base . $file;

					if(is_dir($target))
					{
						// ディレクトリの場合は再帰呼び出し
						$bRet = Mr_rmdir($target, $status);
						if($bRet != 1)
						{
							return 0;
						}
					}else{
						// ファイルの場合はそのまま削除
						if(!unlink($target))
						{
							$status = 'failed to unlink '.$target;
							return 0;
						}
					}
                }

            }
			closedir($dir);
			if(!rmdir($theDir))
			{
				$status = 'failed to rmdir '.$theDir;
				return 0;
			}

			return 1;
		}else{

			// ディレクトリのオープンに失敗
			$status = 'Failed to opendir. '.$theDir;
			return 0;
		}
	}

	//--------------------------------------------------------
	// Mredirect_to
	// 指定ページへリダイレクトする。
	//--------------------------------------------------------
	function Mredirect_to($page){
		header("Location: $page");
		exit;
	}

	//--------------------------------------------------------
	// Mhtmlspecialchars
	// HTMLエンティティに変換するとともに、改行も<BR>に変換
	//--------------------------------------------------------
	function Mhtmlspecialchars($param){
		$sTemp = htmlspecialchars($param, ENT_QUOTES);
		$sTemp = str_replace("\r\n", '<br />', $sTemp);
		$sTemp = str_replace("\n", '<br />', $sTemp);
		$sTemp = str_replace("\r", '<br />', $sTemp);


		return $sTemp;
	}

	//--------------------------------------------------------
	// Mhtmlspecialchars
	// HTMLエンティティに変換するとともに、改行も<BR>に変換
	//--------------------------------------------------------
	function MhtmlspecialcharsEx($param){
		$sTemp = htmlspecialchars($param);
		$sTemp = str_replace("\r\n", '<br />', $sTemp);
		$sTemp = str_replace("\n", '<br />', $sTemp);
		$sTemp = str_replace("\r", '<br />', $sTemp);

		// $result = mb_ereg_replace('\[([^\[\]]+):(http://|https://|mailto:|ftp://)([^<>[:space:]]+[[:alnum:]/])\]', '<a href="\2\3" target="_blank">\1</a>', $sTemp);
		// クリック数カウント対応  okuno@timedia.co.jp
		$result = mb_ereg_replace('\[([^\[\]]+):(http://|https://|mailto:|ftp://)([^<>\][:space:]]+[[:alnum:]/])\]', '<a href="\2\3" target="_blank" onClick="javascript:urchinTracker(\'/outgoing/\3\');">\1</a>', $sTemp);

		return $result;
	}



	//--------------------------------------------------------
	// Mhtml_quote
	// HIDDEN値用に、ダブルクォーテーションのみをエスケープ
	//--------------------------------------------------------
	function Mhtml_quote($param)
	{
		$sTmp = $param;
		$param = str_replace('&', '&amp;', $param);
		$param = str_replace('"', '&quot;', $param);
		return $param;
	}

	//--------------------------------------------------------
	// Mhtml_crlf
	// 改行文字のみをHTMLエスケープ
	//--------------------------------------------------------
	function Mhtml_crlf($param)
	{
		$sTmp = $param;
		$param = str_replace("\r\n", "\n", $param);
		$param = str_replace("\r", "\n", $param);
		$param = str_replace("\n", '<br />', $param);

		return $param;
	}

	// --------------------------------------------------------
	// Mis_slash_trailed
	// 文字列がスラッシュで終了し手いるかどうかを判断
	// --------------------------------------------------------
	function Mis_slash_trailed($dir)
	{
		if(ereg('\/$', $dir))
		{
			return 1;
		}else{
			return 0;
		}
	}

	// --------------------------------------------------------
	// Mslash_trailing
	// 文字列がスラッシュで終了していない場合はスラッシュを追加
	// --------------------------------------------------------
	function Mslash_trailing($dir)
	{
		if(Mis_slash_trailed($dir))
		{
			return $dir;
		}else{
			return $dir.'/';
		}
	}


	// --------------------------------------------------------
	// MSQL_Safe
	// --------------------------------------------------------
	function MSQL_Safe($param, $dtype)
	{
		if(Mis_empty($param) == 1)
		{
			return ' NULL ';
		}

		if($dtype == 1)
		{
			// 数値
			if(Mchk_IsNumber($param) == 1)
			{
				return $param;
			}else{
				return ' NULL ';
			}
		}else{
			// 文字列
			$slashed = addslashes($param);
			$slashed = " '" . $slashed . "' ";

			return $slashed;
		}
	}

	// --------------------------------------------------------
	// MSQL_SafeDQ
	// --------------------------------------------------------
	function MSQL_SafeDQ($param, $dtype)
	{
		if(Mis_empty($param) == 1)
		{
			return ' NULL ';
		}

		if($dtype == 1)
		{
			// 数値
			if(Mchk_IsNumber($param) == 1)
			{
				return $param;
			}else{
				return ' 0 ';
			}
		}else{
			// 文字列
			$bq = '\\';
			$param = str_replace($bq, $bq.$bq, $param);
			$param = str_replace('"', $bq.'"', $param);
//			$param = str_replace("'", $bq . "'", $param);
			// $slashed = addslashes($param);

			// 最後にもう一回
			$param = str_replace($bq, $bq.$bq, $param);
			// さらにシングルクォート管理
			$param = str_replace("'", $bq . "'", $param);

			$slashed = ' "' . $param . '" ';

			return $slashed;
		}
	}

	// --------------------------------------------------------
	// Min_array
	// 対象が配列かどうかを確認した後にin_array を実施
	// --------------------------------------------------------
	function Min_array($needle, $ar)
	{
		if(!isset($ar))
		{
			return false;
		}
		if(!is_array($ar))
		{
			return false;
		}
		return in_array($needle, $ar);

	}

	// --------------------------------------------------------
	// Mbr_array
	// 改行で区切って配列に（空行無視）
	// --------------------------------------------------------
	function Mbr_array($str, &$ar)
	{
		if(Mis_empty($str) == 1)
		{
			return 0;
		}

		$str = str_replace("\r\n", "\n", $str);
		$str = str_replace("\r", "\n", $str);

		$tmpar = split("\n", $str);

		foreach($tmpar as $line)
		{
			if(Mis_empty($line) != 1)
			{
				$ar[] = $line;
			}
		}
		return count($ar);

	}


	// --------------------------------------------------------
	// Mhas_same_member
	// 二つの配列が同じメンバーを持つかどうか
	// --------------------------------------------------------
	function Mhas_same_member($ar1, $ar2)
	{
		if(Mis_array($ar1) == 0)
		{
			return true;
		}
		if(Mis_array($ar2) == 0)
		{
			return true;
		}

		$cnt1 = count($ar1);
		$cnt2 = count($ar2);

		if($cnt1 < $cnt2)
		{
			foreach($ar1 as $mbr)
			{
				if(Min_array($mbr, $ar2))
				{
					return true;
				}
			}
		}else{
			foreach($ar2 as $mbr)
			{
				if(Min_array($mbr, $ar1))
				{
					return true;
				}
			}

		}
		return false;

	}


	// --------------------------------------------------------
	// MCreateUniqFile
	// まだ利用されていないファイル名を予約する。
	// $target_dir: 作成するディレクトリ
	// $suffix: ファイル名の固定後方文字列(ピリオド付き拡張子）
	// --------------------------------------------------------
	function MCreateUniqFile($target_dir, $suffix)
	{
		// 時刻+カウント

		$time_str = date('Ymd_His_');
		$cnt_str = 1;

		// スラッシュ管理
		if(!ereg('\/$', $target_dir))
		{
			$target_dir = $target_dir . '/';
		}

		$basefilename = $time_str.$cnt_str.$suffix;
		$testee = $target_dir.$basefilename;

		while(file_exists($testee))
		{
			$cnt_str = $cnt_str + 1;
			$basefilename = $time_str.$cnt_str.$suffix;
			$testee = $target_dir.$basefilename;
		}

		$fp = fopen($testee, "w");
		if(!$fp)
		{
			return "";
		}else{
			fclose($fp);
		}

		return $basefilename;
	}

	// --------------------------------------------------------
	// MCreateUniqFile
	// まだ利用されていないディレクトリを予約する。
	// $target_dir: 作成するディレクトリ
	// $suffix: ファイル名の固定後方文字列(ピリオド付き拡張子）
	// --------------------------------------------------------
	function MCreateUniqDir($target_dir)
	{
		// 時刻+カウント

		$time_str = date('Ymd_His_');
		$cnt_str = 1;

		// スラッシュ管理
		if(!ereg('\/$', $target_dir))
		{
			$target_dir = $target_dir . '/';
		}

		$basedirname = $time_str.$cnt_str;
		$testee = $target_dir.$basedirname;

		while(file_exists($testee))
		{
			$cnt_str = $cnt_str + 1;
			$basedirname = $time_str.$cnt_str;
			$testee = $target_dir.$basedirname;
		}

		if(mkdir($testee, 0755))
		{
			return $basedirname;
		}else{
			return "";
		}
	}

	// 受信データの正規化
	// stripslashes とエンコード変換
	function RequestRegEx($param, $is_quote, $in_enc)
	{
		// トリム
		$param = trim($param);

		if($in_enc != 'UTF-8')
		{
			$sTmp = mb_convert_encoding($param, 'UTF-8', $in_enc);
		}else{
			$sTmp = $param;
		}

		if($is_quote == 1)
		{
			$sTmp = stripslashes($sTmp);
		}else{
			$sTmp = $sTmp;
		}

		// 半角カナの全角化
		$sTmp = mb_convert_kana($sTmp, 'KV', 'UTF-8');



		return $sTmp;
	}


	// 拡張子取得
	function GetFileExt($filename)
	{
		if(Mis_empty($filename) == 1)
		{
			return "";
		}

		$parts = split("\.", basename($filename));
		$ub = count($parts);

		if($ub == 1)
		{
			return "";
		}
		return $parts[$ub - 1];
	}


	// ファイルアップロード
	function RetrieveUpFile($field_name, $dir_name, $banned)
	{


		if((Mis_empty($field_name) == 1) || (Mis_empty($dir_name) == 1))
		{
			return "";
		}
		if(ereg("/$", $dir_name))
		{
			$dir_name = ereg_replace("/$", "", $dir_name);
		}

    	if((file_exists($_FILES[$field_name]['tmp_name'])) && (filesize($_FILES[$field_name]['tmp_name']) > 0))
    	{
    		// ファイルがアップロードされている


    		// 拡張子
    		$ufile_ext = GetFileExt($_FILES[$field_name]['name']);
    		if(Mis_empty($ufile_ext) != 1)
    		{
    			$ufile_ext = '.' . $ufile_ext;
    		}

			// 拡張子制限
			if(is_array($banned))
			{
				$ext_ok_flg = 0;
				foreach($banned as $each_ext)
				{
					if(eregi($each_ext . '$', $ufile_ext))
					{
						$ext_ok_flg = 1;
						break;
					}
				}
				if($ext_ok_flg != 1)
				{
					return "inv";
				}
			}

    		// ユニークファイル
    		$theFile = MCreateUniqFile($dir_name, $ufile_ext);
    		if(!$theFile)
    		{
    			return "";
    		}

    		// アップロードファイルの妥当性確認
    		if(is_uploaded_file($_FILES[$field_name]['tmp_name']))
    		{
    			// ﾂﾅﾅ? -> ･｢･ﾃ･ﾗ･?ｼ･ﾉ･ﾕ･｡･､･?ﾎﾎｰ隍ﾋ･ｳ･ﾔ｡ｼ
    			copy($_FILES[$field_name]['tmp_name'], $dir_name . '/' . $theFile);
				return $theFile;

    		}else{
    			// システムエラー
    			return "";
    		}
    	}else{
			return "";
    	}
    }

	// ファイルアップロードDB
	function RetrieveUpFileDB($field_name, $dir_name, $publish, $banned)
	{


		if(Mis_empty($field_name) == 1)
		{
			return "";
		}

    	if((file_exists($_FILES[$field_name]['tmp_name'])) && (filesize($_FILES[$field_name]['tmp_name']) > 0))
    	{
    		// ファイルがアップロードされている


    		// 拡張子
    		$ufile_ext = GetFileExt($_FILES[$field_name]['name']);
    		if(Mis_empty($ufile_ext) != 1)
    		{
    			$ufile_ext = '.' . $ufile_ext;
    		}

			// 拡張子制限
			if(is_array($banned))
			{
				$ext_ok_flg = 0;
				foreach($banned as $each_ext)
				{
					if(eregi($each_ext . '$', $ufile_ext))
					{
						$ext_ok_flg = 1;
						break;
					}
				}
				if($ext_ok_flg != 1)
				{
					return "inv";
				}
			}

    		// アップロードファイルの妥当性確認
    		if(is_uploaded_file($_FILES[$field_name]['tmp_name']))
    		{
    			// 妥当 -> DBにインポート
    			global $db_conn_enq;
    			require_once COMMON_DIR . '/CPgFileMgr.inc';

    			$pgfile = new CPgFileMgr($db_conn_enq);
				$pgfile->SetFileSource($_FILES[$field_name]['tmp_name']);
				$pgfile->SetFileSourceType($_FILES[$field_name]['type']);;
				$pgfile->SetFileSourceLocal(mb_convert_encoding($_FILES[$field_name]['name'], 'EUC-JP'));
				if($publish)
				{
					// アップするファイルは公開可能
					$pgfile->SetPublish($publish);
				}


				if(!$pgfile->Import(array()))
				{
					// エラー
					return "";

				}
				return $pgfile->GetOID();

    		}else{
    			// システムエラー
    			return "";
    		}
    	}else{
			return "";
    	}
    }

	// ------------------------------------------------------------
	// Mchk_IsNumber
	// 数値チェック
	// ------------------------------------------------------------
	function Mchk_IsNumber($param)
	{
		if(ereg("[^0-9]", $param))
		{
			return 0;
		}else{
			return 1;
		}
	}

	// ------------------------------------------------------------
	// Mchk_IsNumberSigned
	// 数値チェック
	// ------------------------------------------------------------
	function Mchk_IsNumberSigned($param)
	{
		if(substr($param, 0, 1) == '-')
		{
			$chkParam = substr($param, 1);
		}else{
			$chkParam = $param;
		}
		if(ereg("[^0-9]", $chkParam))
		{
			return 0;
		}else{
			return 1;
		}
	}

	// ------------------------------------------------------------
	// Mchk_IsNumberEx
	// 数値チェック
	// 許可する文字を指定できる
	// ------------------------------------------------------------
	function Mchk_IsNumberEx($param, $permis)
	{
		if(is_array($permis))
		{
			foreach($permis as $permi)
			{
				$param = str_replace($permi, '', $param);
			}
		}

		if(ereg("[^0-9]", $param))
		{
			return 0;
		}else{
			return 1;
		}
	}

	// ------------------------------------------------------------
	// Mchk_IsNumberReal
	// 数値チェック
	// 小数点コミ
	// ------------------------------------------------------------
	function Mchk_IsNumberReal($param, $smallpart)
	{
		$params = split('\.', $param);
		if(count($params) > 2)
		{
			return false;
		}
		if(count($params) == 2)
		{
			// 小数点の前後は必須
			if((strlen($params[0]) == 0) || (strlen($params[1]) == 0))
			{
				return false;
			}
			// 小数点の前は符号付き整数
			if(Mchk_IsNumberSigned($params[0]) != 1)
			{
				return false;
			}
			// 小数点の後は整数
			if(Mchk_IsNumber($params[1]) != 1)
			{
				return false;
			}
			// 小数点の桁数
			if(strlen($params[1]) > $smallpart)
			{
				return false;
			}
			return true;
		}else{
			return Mchk_IsNumberSigned($params[0]);
		}

	}


	// ------------------------------------------------------------
	// Mchk_IsNumberRealEx
	// 数値チェック
	// 整数部、小数部それぞれ桁数チェック
	// ------------------------------------------------------------
	function Mchk_IsNumberRealEx($param, $integralpart, $smallpart)
	{
		$param = trim($param);
		if (ereg('^(\-?[0-9]{0,' . $integralpart . '})\.([0-9]{0,' . $smallpart . '})$', $param))
		{
			return Mchk_IsNumberReal($param, $smallpart);
		}
		elseif (ereg('^(\-?[0-9]{0,' . $integralpart . '})$', $param))
		{
			return Mchk_IsNumberSigned($param, $smallpart);
		}
		else {
			return false;
		}
	}


	// ------------------------------------------------------------
	// Mchk_IsAlpha
	// アルファベットチェック
	// ------------------------------------------------------------
	function Mchk_IsAlpha($param)
	{
		if(ereg("[^a-zA-Z]", $param))
		{
			return 0;
		}else{
			return 1;
		}
	}

	// ------------------------------------------------------------
	// Mchk_IsAlNum
	// アルファベット数値チェック
	// ------------------------------------------------------------
	function Mchk_IsAlNum($param)
	{
		if(ereg("[^a-zA-Z0-9]", $param))
		{
			return 0;
		}else{
			return 1;
		}
	}

	// ------------------------------------------------------------
	// Mchk_IsAlNumAll
	// ASCII文字チェック
	// ------------------------------------------------------------
	function Mchk_IsAlNumAll($param)
	{
		if(ereg("[^\x20-\x7e]", $param))
		{
			return 0;
		}else{
			return 1;
		}
	}

	// ------------------------------------------------------------
	// Mchk_IsMailaddr
	// メールアドレスチェック
	// ------------------------------------------------------------
	function Mchk_IsMailaddr($param)
	{
		// if (!eregi("^[_a-z0-9-]+(\.+[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*$", $param)){
		//if (!eregi("^[\._a-z0-9\-]*@[a-z0-9\-]+(\.[a-z0-9\-]+)*$", $param)){
		if(!eregi("^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~.-]+@[a-zA-Z0-9!#$%&'*+/=?^_`{|}~.-]+$", $param))
		{
			return 0;
		} else {
			return 1;
		}
	}


// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------



	// ------------------------------------------------------------
	// MEnc_EUCJP
	// SJIS、EUCのいずれにせよEUCエンコーディングに変換
	// ------------------------------------------------------------
	function MEnc_EUCJP($param)
	{

//		$this_enc = mb_detect_encoding($param, "auto");
//		$retstr = mb_convert_encoding($param, "EUC-JP", $this_enc);

		return $param;
	}


	// ------------------------------------------------------------
	// Mchk_IsMultibyte
	// 簡易2バイト文字チェック（リードバイトが\x80未満でないこと）
	// ------------------------------------------------------------
	function Mchk_IsMultibyte($param)
	{
		return Mchk_IsNoAscii($param);
	}

	// ------------------------------------------------------------
	// Mchk_IsNoAscii
	// 半角文字が含まれないこと
	// ------------------------------------------------------------
	function Mchk_IsNoAscii($param, $enc = "")
	{

		// UTF-8でなければUTFに変換
		if($enc != '')
		{
			$param = mb_convert_encoding($para, 'UTF-8', $enc);
		}

		if(strlen($param) == 0)
		{
			return 1;
		}
		for($iCnt = 0; $iCnt < strlen($param); $iCnt++)
		{
			if(ord(substr($param, $iCnt, 1)) < 128)
			{
				// 1バイト文字発見
				return 0;
			}
		}
		return 1;
	}

	// ------------------------------------------------------------
	// Mchk_IsHiragana
	// ------------------------------------------------------------
	function Mchk_IsHiragana($param, $enc = "")
	{
		if($enc != "")
		{
			$param = mb_convert_encoding($param, 'UTF-8', $enc);
		}

		// 文字列バイト長が3以上で3の倍数
		if(strlen($param) < 3)
		{
			return 0;
		}
		if((strlen($param) % 3) != 0)
		{
			return 0;
		}

		$pos = 0;
		for($iCnt = 0; $iCnt < strlen($param); $iCnt++)
		{
			$pos++;
			if(($pos % 3) == 1)
			{
				// 3バイトのうち先頭は227固定
				if(ord(substr($param, $iCnt, 1)) != 227)
				{
					return 0;
				}
			}elseif(($pos % 3) == 2)
			{
				switch(ord(substr($param, $iCnt, 1)))
				{
					case 128:
						// 2バイト目が128の時は3バイト目が128必須
						// 全角スペース
						if(ord(substr($param, $iCnt+1, 1)) != 128)
						{
							return 0;
						}
						break;

					case 129:
						// 2バイト目が129の時は3バイト目が129以上必須
						if(ord(substr($param, $iCnt+1, 1)) < 129)
						{
							return 0;
						}
						break;

					case 130:
						// 2バイト目が130の時は3バイト目が155以下必須
						if(ord(substr($param, $iCnt+1, 1)) > 155)
						{
							return 0;
						}
						break;

					case 131:
						// 2バイト目が131の時は3バイト目が188必須
						// 「ー」（長印）
						if(ord(substr($param, $iCnt+1, 1)) != 188)
						{
							return 0;
						}
						break;

					default:
						return 0;
						break;
				}
			}

		}

		// Ok.
		return 1;
	}

	// ------------------------------------------------------------
	// Mchk_IsKatakana
	// ------------------------------------------------------------
	function Mchk_IsKatakana($param, $enc = "")
	{
		if($enc != "")
		{
			$param = mb_convert_encoding($param, 'UTF-8', $enc);
		}

		// 文字列バイト長が3以上で3の倍数
		if(strlen($param) < 3)
		{
			return 0;
		}
		if((strlen($param) % 3) != 0)
		{
			return 0;
		}

		$pos = 0;
		for($iCnt = 0; $iCnt < strlen($param); $iCnt++)
		{
			$pos++;
			if(($pos % 3) == 1)
			{
				// 3バイトのうち先頭は227もしくは239固定
				if((ord(substr($param, $iCnt, 1)) != 227) && (ord(substr($param, $iCnt, 1)) != 239))
				{
					return 0;
				}
			}elseif(($pos % 3) == 2)
			{
				if(ord(substr($param, $iCnt-1, 1)) == 227)
				{
					switch(ord(substr($param, $iCnt, 1)))
					{
						case 128:
							// 2バイト目が128の時は3バイト目が128必須
							// 全角スペース
							if(ord(substr($param, $iCnt+1, 1)) != 128)
							{
								return 0;
							}
							break;

						case 130:
							// 2バイト目が129の時は3バイト目が129以上必須
							if(ord(substr($param, $iCnt+1, 1)) < 161)
							{
								return 0;
							}
							break;

						case 131:
							// 2バイト目が130の時は3バイト目が155以下必須
							if(ord(substr($param, $iCnt+1, 1)) > 188)
							{
								return 0;
							}
							break;

						default:
							return 0;
							break;
					}
				}else{
					// 半角
					switch(ord(substr($param, $iCnt, 1)))
					{
						case 189:
							// 2バイト目が189の時は3バイト目が166以上必須
							if(ord(substr($param, $iCnt+1, 1)) < 166)
							{
								return 0;
							}
							break;

						case 190:
							// 2バイト目が190の時は3バイト目が158以下必須
							if(ord(substr($param, $iCnt+1, 1)) > 158)
							{
								return 0;
							}
							break;

						default:
							return 0;
							break;
					}
				}
			}

		}

		// Ok.
		return 1;
	}

	// ------------------------------------------------------------
	// Mchk_IsDate
	// 日付として正しいかどうか
	// ------------------------------------------------------------
	function Mchk_IsDate($year, $mon, $mday)
	{
		$fullmon = array(1,3,5,7,8,10,12);
		$halfmon = array(4,6,9,11);

		if((Mis_empty($year) == 1) || (Mis_empty($mon) == 1) || (Mis_empty($mday) == 1))
		{
			return 0;
		}
		if((Mchk_IsNumber($year) != 1) || (Mchk_IsNumber($mon) != 1) || (Mchk_IsNumber($mday) != 1))
		{
			return 0;
		}

		$i_year = (int)$year;
		$i_mon = (int)$mon;
		$i_mday = (int)$mday;



		if(in_array($i_mon, $fullmon))
		{
			if(($i_mday > 0) && ($i_mday < 32))
			{
				return 1;
			}else{
				return 0;
			}
		}else if(in_array($i_mon, $halfmon))
		{
			if(($i_mday > 0) && ($i_mday < 31))
			{
				return 1;
			}else{
				return 0;
			}
		}else if($i_mon == 2)
		{
			if(($i_year % 400) == 0)
			{
				// 400の倍数年　→　うるう年
				if(($i_mday < 30) && ($i_mday > 0))
				{
					return 1;
				}else{
					return 0;
				}
			}else if(($i_year % 100) == 0)
			{
				// 100の倍数年　→　非うるう年
				if(($i_mday < 29) && ($i_mday > 0))
				{
					return 1;
				}else{
					return 0;
				}
			}else if(($i_year % 4) == 0)
			{
				// 4の倍数年　→　うるう年
				if(($i_mday < 30) && ($i_mday > 0))
				{
					return 1;
				}else{
					return 0;
				}
			}else{
				if(($i_mday < 29) && ($i_mday > 0))
				{
					return 1;
				}else{
					return 0;
				}
			}
		}else{
			// 月が範囲外
			return 0;
		}
	}

	function TextSafe($param, $rep_hash = array("\t" => " "))
	{
		$param = str_replace("\r\n", "\n", $param);
		$param = str_replace("\r", "\n", $param);
		$param = str_replace("\n", " ", $param);

		$param = str_replace("\t", " ", $param);

		return $param;
	}

	// --------------------------------------------------------
	// パスワード生成
	// --------------------------------------------------------
	function PwdGenerator($len, $seed = NULL)
	{

		if(is_null($seed))
		{
//			$seed = time();
		}

		$strRet = "";

		//乱数初期化
//		srand($seed);


		for( $i = 0; $i < $len; $i++ ){
			// 文字種の判定
			$iTmp = rand(1, 3);

			if($iTmp == 1){
				$strRet .= chr(rand(ord('a'), ord('z')));
			}else if($iTmp == 2){
				$strRet .= chr(rand(ord('A'), ord('Z')));
			}else{
				$strRet .= chr(rand(ord('1'), ord('9')));
			}
		}

		return $strRet;
	}

	function copy_recursive($src, $dst) {

		if (is_dir($src)) {
			mkdir($dst);

			$files = scandir($src);
			foreach ($files as $file) {
				if (($file != ".") && ($file != "..")) {
					copy_recursive("$src/$file", "$dst/$file");
				}
			}
		}
		else if (file_exists($src)) {
			copy($src, $dst);
		}
	}

	// ------------------------------------------------------------
	// PageTop
	// 行総数、頁行数、頁、先頭番号から先頭行数を返す
	// 2006.09.13 岩井
	// ------------------------------------------------------------
	function PageTop($full, $line, $page, $top = 0)
	{
		// トップ頁なら先頭番号を返す
		if($page == 1)
		{
			return $top;
		}

		// 総頁数より頁が大きければNULLを返す
		if(ceil($full / $line) < $page)
		{
			return NULL;
		}

		// 先頭行取得
		$num = $line * ($page - 1);
		$num = $num + $top;

		// 総行数より行数が多ければNULLを返す
		if($full <= $num)
		{
			return NULL;
		}

		return $num;
	}

	// ------------------------------------------------------------
	// PageSet
	// 行総数、頁行数、頁、前頁数、次頁数から頁配列を返す
	// array('prev' => array(1,2,3...), 'next' => array(6,7,8...))
	// 2006.09.13 岩井
	// ------------------------------------------------------------
	function PageSet($full, $line, $page, $cntP = 1, $cntN = 1)
	{
		// 変換用配列初期化
		$Re = array('prev' => array(), 'next' => array());

		// 総頁数
		$pageCnt = ceil($full / $line);

		// 前頁
		for($i = 1; $i <= $cntP; $i++)
		{
			if(($page - $i) < 1)
			{
				break;
			}
			if(($page - $i) < $pageCnt)
			{
				$prev[] = $page - $i;
			}
		}
		if(is_array($prev))
		{
			$prev = array_reverse($prev);
			$Re['prev'] = $prev;
		}

		// 次頁
		for($i = 1; $i <= $cntN; $i++)
		{
			if($pageCnt < ($page + $i))
			{
				break;
			}
			$next[] = $page + $i;
		}
		if(is_array($next))
		{
			$Re['next'] = $next;
		}

		return $Re;
	}

	// ------------------------------------------------------------
	// DayDate
	// 曜日文字列、SUN,MON,TUE,WED,THU,FRI,SATから
	// 今日から指定回数目の曜日日付を返す。
	// 2006.10.13 岩井
	// ------------------------------------------------------------
	function DayDate($day, $target)
	{
		$days = array(
			'SUN' => 0,
			'MON' => 1,
			'TUE' => 2,
			'WED' => 3,
			'THU' => 4,
			'FRI' => 5,
			'SAT' => 6
		);

		// 入力チェック
		if((!array_key_exists($day, $days)) || (!is_numeric($target)))
		{
			return false;
		}

		// 今日の日付
		$thisdate = date("Y-m-d");

		// 前後パラメータ
		if($target < 0)
		{
			$operator = -1;
		}else{
			$operator = 1;
		}

		// 作成ループ
		$i = 0;
		while($cnt < abs($target))
		{
			$i++;
			$param = $i * $operator;

			// 曜日が一致したら日付作成
			if($days[$day] == date("w", strtotime($param .' day' . $thisdate)))
			{
				$Redate = date("Y-m-d", strtotime($param .' day' . $thisdate));
				$cnt++;
			}
		}

		return $Redate;
	}

	// ------------------------------------------------------------
	// DistMailDomain
	// メールドメインの判別
	// 2006.10.17 岩井
	// ------------------------------------------------------------
	function DistMailDomain($mail, $mails, $op = 'deny')
	{
		// 戻り値の設定
		if($op == 'allow')
		{
			// マッチしたら許可
			$match = 0;
			$unmatch = 1;
		}else{
			// マッチしたら不許可(デフォルト)
			$match = 1;
			$unmatch = 0;
		}

		// 判別用配列ループ
		foreach($mails as $val)
		{
			// ワイルドカード置換
			$val = eregi_replace("\*", "(.+)", $val);
			$val = eregi_replace("\?", "(.)", $val);

			// 判別
			if(eregi('^(.+)@' . $val, $mail))
			{
				return $match;
			}
		}

		// マッチしない場合
		return $unmatch;
	}


	// ------------------------------------------------------------
	// DeployArray
	// 多次元配列をスカラ情報に変換
	// ------------------------------------------------------------
	function DeployArray($ar)
	{
		if(Mis_array($ar) == 0)
		{
			return "";
		}

		// PHP4.3 以上の場合のprint_r出力リダイレクトを利用
		return print_r($ar, true);

	}

	/**
	 * ファイルをスカラに格納
	 *
	 * @param unknown_type $file
	 * @return unknown
	 */
	function MReadFile($file)
	{
		$ret = "";
		if(file_exists($file))
		{
			$lines = file($file);
			if(Mis_array($lines) == 1)
			{
				foreach($lines as $line)
				{
					$ret .= $line;
				}
			}
			return $ret;
		}

	}


	// DEBUG
	function debug_dump($msg)
	{
		global $db_conn_mstr;
		require_once COMMON_DIR . '/loglib/CLogMgr.inc';
		$lgm = new CLogMgr($db_conn_mstr);
		$lgm->SetNotifyTo(array(CRT_ERR_NOTIFY_TO));
		$lgm->AppendLog(LOGMGR_ERRLEVEL_DEBUG, $msg);

		echo 'NOTICE.<!-- SYSTEM ERROR -->';
		exit;

	}

	// 404ヘッダーを返す
	function issue_404($page = null)
	{
		//header("Not Found", true, 404);
		header("HTTP/1.0 404 Not Found");
		header('Content-type: text/html;charset=utf-8');

		if(!$page)
		{
			echo '<html><body><h1>404 Not Found.</h1><p>お探しのデータは削除されたか、有効期限が切れたか、URLに誤りがある可能性があります。</p></bod></html>';
		}else{
			echo $page;
		}
		exit;
	}


	// 半角化
	function ConvZH($param)
	{
		$param = mb_convert_kana($param, 'r');
		$param = mb_convert_kana($param, 'n');
		$param = mb_ereg_replace('．', '.', $param);
		$param = mb_ereg_replace('ー', '-', $param);
		$param = mb_ereg_replace('－', '-', $param);
		$param = mb_ereg_replace('＿', '_', $param);
		$param = mb_ereg_replace('＠', '@', $param);

		return $param;
	}

	// ハイフン除去
	function ConvTelNumbers($param)
	{
		$param = ConvZH($param);
		$param = str_replace('-', '', $param);

		return $param;
	}

	// 数字のみを抽出する（全角数字は半角に変換)
	function ExtractionNumber($param)
	{
		$param = mb_convert_kana($param, 'n');
		$param = mb_eregi_replace("[^0-9]", "", $param);

		return $param;
	}

	//
	function gen_error($errno, $errmsg, $filename, $linenum, $vars)
	{



	    // エラーエントリのタイムスタンプ
	    $dt = date("Y-m-d H:i:s");

	    // define an assoc array of error string
	    // in reality the only entries we should
	    // consider are E_WARNING, E_NOTICE, E_USER_ERROR,
	    // E_USER_WARNING and E_USER_NOTICE
	    $errortype = array (
	                E_ERROR           => "Error",
	                E_WARNING         => "Warning",
	                E_PARSE           => "Parsing Error",
	                E_NOTICE          => "Notice",
	                E_CORE_ERROR      => "Core Error",
	                E_CORE_WARNING    => "Core Warning",
	                E_COMPILE_ERROR   => "Compile Error",
	                E_COMPILE_WARNING => "Compile Warning",
	                E_USER_ERROR      => "User Error",
	                E_USER_WARNING    => "User Warning",
	                E_USER_NOTICE     => "User Notice",
	                E_STRICT          => "Runtime Notice"
	                );
	    // set of errors for which a var trace will be saved
	    $user_errors = array(E_USER_ERROR, E_USER_WARNING, E_ERROR, E_WARNING);

		debug_dump($errmsg . "\n" . $filename . "\n" . $linenum . "\n" . print_r($vars, true));

	}

	// エラーハンドラ設定
	// 独自エラーハンドラ
	// $old_error_handler = set_error_handler("gen_error");


	/**
	 * Excel読み込みCSV用変換
	 *
	 * @param unknown_type $parma
	 */
	function _excel_safe($param)
	{
		// ダブルクォート二重化
		$param = str_replace('"', '""', $param);
		// ダブルクォートで挟む
		$param = '"' . $param . '"';

		return $param;

	}

	/**
	 * 単純カンマ区切り用変換
	 *
	 * @param unknown_type $param
	 */
	function _csv_safe($param)
	{
		$param = str_replace("\r\n", "\n", $param);
		$param = str_replace("\r", "\n", $param);
		$param = str_replace("\n", " ", $param);
		$param = str_replace(',', '.', $param);

		return $param;

	}

	/**
	 * 単純タブ区切り用変換
	 *
	 * @param unknown_type $param
	 * @return unknown
	 */
	function _tsv_safe($param)
	{
		$param = str_replace("\r\n", "\n", $param);
		$param = str_replace("\r", "\n", $param);
		$param = str_replace("\n", " ", $param);
		$param = str_replace("\t", ' ', $param);

		return $param;

	}

	// --------------------------------------------------------
	// MCreateUniqFileName
	// まだ利用されていないファイル名を作る。
	// $target_dir: 作成するディレクトリ
	// $suffix: ファイル名の固定後方文字列(ピリオド付き拡張子）
	// --------------------------------------------------------
	function MCreateUniqFileName($target_dir, $suffix)
	{
		// 時刻+カウント

		$time_str = date('Ymd_His_');
		$cnt_str = 1;

		// スラッシュ管理
		if(!ereg('\/$', $target_dir))
		{
			$target_dir = $target_dir . '/';
		}

		$basefilename = $time_str.$cnt_str.$suffix;
		$testee = $target_dir.$basefilename;

		while(file_exists($testee))
		{
			$cnt_str = $cnt_str + 1;
			$basefilename = $time_str.$cnt_str.$suffix;
			$testee = $target_dir.$basefilename;
		}

		return $basefilename;
	}

	// ------------------------------------------------------------
	// M_Age
	// 日付、誕生日から年齢を返す
	// $Ty,$Tm,$Td = 年月日、$By,$Bm,$Bd = 生年月日
	// 2006.08.05 岩井
	// ------------------------------------------------------------
	function M_Age($Ty,$Tm,$Td,$By,$Bm,$Bd)
	{
		if((Mchk_IsDate($Ty,$Tm,$Td) == 0) || (Mchk_IsDate($By,$Bm,$Bd) == 0))
		{
			return false;
		}
		$age = $Ty - $By;
		if($Tm < $Bm)
		{
			return $age - 1;
		}
		if($Tm == $Bm && $Td < $Bd)
		{
			return $age - 1;
		}
		return $age;
	}

	// ------------------------------------------------------------
	// M_Generation
	// 日付、誕生日から世代を返す
	// $BirthDay = 生年月日、$ToDay = 年月日、$Step = 範囲
	// 2006.11.23 岩井
	// ------------------------------------------------------------
	function M_Generation($BirthDay, $ToDay = "", $Step = 10)
	{
		// 今日日付
		if($ToDay == "")
		{
			$ToDay = date("Y-m-d");
		}

		list($Ty, $Tm ,$Td) = explode("-", $ToDay);
		list($By, $Bm ,$Bd) = explode("-", $BirthDay);

		$Age = M_Age($Ty,$Tm,$Td,$By,$Bm,$Bd);
		if(!$Age)
		{
			return false;
		}

		for($i = 0; $i < 200; $i = $i + $Step)
		{
			for($t = $i; $t < ($i + $Step); $t++)
			{
				if($t == $Age)
				{
					return $i;
				}
			}
		}

		return false;
	}


	function _dprint($msg)
	{
		if(($_SERVER['REMOTE_ADDR'] == '219.2.223.60') || ($_SERVER['REMOTE_ADDR'] == '124.86.171.36'))
		{
			print_r($msg);
			exit;
		}
	}


	// 画像設定ファイルの読込み
	function Read_ImgConfig($imgconfig)
	{
		// 画像設定ファイルの存在確認
		if((file_exists($imgconfig)) && (filesize($imgconfig) > 0))
		{
			// 画像設定ファイル読込み
			$file_array = file($imgconfig);
			if((is_array($file_array)) && (count($file_array) > 0))
			{
				foreach($file_array as $val)
				{
					$line = explode("\t", rtrim($val));

					if((is_array($line)) && (count($line) == 5))
					{
						$ImgConfig[$line[0]] = array($line[1], $line[2], $line[3], $line[4]);
					}
				}
			}
		}
		if(is_array($ImgConfig))
		{
			return $ImgConfig;
		}else{
			return "";
		}
	}

	// 画像ファイルの妥当性
	function Mis_upload_img($colname, $banned, $SizeImg)
	{
		define("IMAGE_TYPE_GIF", 1, FALSE);
		define("IMAGE_TYPE_JPG", 2, FALSE);
		define("IMAGE_TYPE_PNG", 3, FALSE);
		define("IMAGE_TYPE_SWF", 4, FALSE);

		// 画像設定はあるか
		if((is_array($SizeImg)) && (count($SizeImg) > 0))
		{
			// 画像アップロードが設定されているか？
			foreach($banned as $each_ext)
			{
				switch($each_ext)
				{
				case 'gif':
					$type[] = IMAGE_TYPE_GIF;
					break;
				case 'jpg':
				case 'jpeg':
					$type[] = IMAGE_TYPE_JPG;
					break;
				case 'png':
					$type[] = IMAGE_TYPE_PNG;
					break;
				case 'swf':
					$type[] = IMAGE_TYPE_SWF;
					break;
				default:
					break;
				}
			}
			if(is_array($type))
			{
				if(Mchk_IsImage($_FILES[$colname]['tmp_name'], $type, $SizeImg[$colname]) != 1)
				{
					return 0;
				}
			}

		}

		return 1;
	}


	// イメージチェック関数
	function Mchk_IsImage($file, $type, $size)
	{
		// ファイルの存在確認
		if((!file_exists($file)) || (filesize($file) <= 0))
		{
			return 1;
		}

		// ファイル情報取得
		$imginfo = getimagesize($file);
		if(!$imginfo)
		{
			return 0;
		}
		list($inWidth, $inHeight, $inType, $tag) = $imginfo;

		// ファイルタイプ
		if(!in_array($inType, $type))
		{
			return 0;
		}

		// 幅 下限
		if($inWidth < $size[0])
		{
			return 0;
		}

		// 幅 上限
		if($inWidth > $size[1])
		{
			return 0;
		}

		// 高さ 下限
		if($inHeight < $size[2])
		{
			return 0;
		}

		// 高さ 上限
		if($inHeight > $size[3])
		{
			return 0;
		}

		return 1;
	}


	/**
	 * 氏名を返す
	 *
	 * @mode = 1 の場合はフリガナ
	 * @return unknown
	 */
	function GetFullNameByRec($rec, $mode = 0)
	{
		if($mode == 0)
		{
			if(Mis_empty($rec['lname']) == 1)
			{
				return $rec['lfname'];
			}else{
				return $rec['lname'] . ' ' . $rec['fname'];
			}
		}else{
			if(Mis_empty($rec['lnamekana']) == 1)
			{
				return $rec['lfnamekana'];
			}else{
				return  $rec['lnamekana'] . ' ' . $rec['fnamekana'];
			}
		}
	}


	function splitSpaces($param)
	{
		$cur_enc = mb_regex_encoding();
		mb_regex_encoding('EUC-JP');

		$param = mb_ereg_replace('　', '', $param);
		$param = str_replace(' ', '', $param);

		mb_regex_encoding($cur_enc);

		return $param;
	}

	/**
	 * 一定バイト数で文字列先頭を切り出し（マルチバイト対応）
	 *
	 * @param unknown_type $param
	 * @param unknown_type $len
	 * @return unknown
	 */
	function splice_string($param, $len)
	{
		$length = strlen($param);
		$result_str = "";

		for($iCnt = 0; $iCnt < $len; $iCnt++)
		{
			$cur_char = substr($param, $iCnt, 1);
			$cur_num = ord($cur_char);

			$result_str .= $cur_char;

			if($cur_num > 127)
			{
				$iCnt++;
				$result_str  .= substr($param, $iCnt, 1);
			}
		}

		return $result_str;
	}

	/**
	 * 一定バイト数で文字列先頭を切り出し（マルチバイト対応）＋省略文字
	 *
	 * @param unknown_type $param
	 * @param unknown_type $len
	 * @return unknown
	 */
	function splice_stringEx($param, $len, $ry=NULL)
	{
		if ($ry === NULL)
		{
			$ry = SPLICE_STRINGEX_RYCHAR;
		}

		$length = strlen($param);
		if ($length > $len)
		{
			return splice_string($param, $len) . $ry;
		}
		else {
			return $param;
		}
	}

	/**
	 * 配列からランダムに指定の数値分の要素を取り出す
	 *
	 * @param unknown_type $num
	 * @param unknown_type $ar
	 * @param unknown_type $ret
	 * @return unknown
	 */
	function array_random_pickup($num, $ar, &$ret)
	{
		// 配列でない場合はエラー
		if(Mis_array($ar) != 1)
		{
			return false;
		}

		// 配列より要求数があればすべてそのままコピー
		if(count($ar) <= $num)
		{
			$upper = count($ar);
		}else{
			$upper = $num;
		}

		shuffle($ar);
		for($iCnt = 0; $iCnt < $upper; $iCnt++)
		{
			$ret[] = $ar[$iCnt];
		}
		return true;

	}

	/**
	 * エスケープされてクォートされたCSVを読む
	 *
	 * "\"aaa"
	 * "\\5,000","\\2,000"
	 * @param unknown_type $param
	 */
	function _readEscapedCSV($param, &$ar)
	{
		$len = strlen($param);

		$in_data = false;
		$in_escape = false;
		$no_quotes = false;
		$current_word = "";

		// データがない場合
		if(Mis_empty($param) == 1)
		{
			return;
		}
		// クォートがない場合
		if(substr($param, 0, 1) != '"')
		{
			$tmpAr = array();
			$tmpAr = split(',', $param);
			if(Mis_array($tmpAr) == 1)
			{
				foreach($tmpAr as $idx => $nm)
				{
					$ar[$idx] = trim($nm);
				}
			}
			return;
		}

		for($iCnt = 0; $iCnt < $len; $iCnt++)
		{
			$chk_byte = substr($param, $iCnt, 1);

			switch ($chk_byte)
			{
				case '"':
					if(!$in_data)
					{
						// データ開始文字

						// データ部開始
						$in_data = true;

						// バッファクリア
						$current_word = "";
						break;
					}else{
						if($in_escape)
						{
							// エスケープされていたら文字列として追加
							$current_word .= '"';
							// エスケープ解除
							$in_escape = false;
							break;

						}else{
							// エスケープされていなければ終了

							// 配列に追加
							$ar[] = $current_word;

							// バッファクリア
							$current_word = "";

							// データ部終了
							$in_data = false;
							break;

						}
					}
					break;

				case '\\':
					if(!$in_data)
					{
						break;
					}else{
						if($in_escape)
						{
							$current_word .= '\\';

							// エスケープ解除
							$in_escape = false;
						}else{
							// エスケープ部開始
							$in_escape = true;
						}
						break;
					}
					break;

				case ',':
					if($in_data)
					{
						// データ中のカンマは追記
						$current_word .= ',';
					}
					break;

				default:
					$current_word .= $chk_byte;
					break;
			}

		}

	}

	/**
	 * 年齢-年齢の範囲を指定する
	 * SQL条件式を返す
	 *
	 * $colname = カラム名, $lower = 年齢下限, $uper = 年齢上限
	 * SQL_BirthGen('birthday', 20, 29, '2006-11-22') // 20代の誕生日範囲を返す
	 */
	function SQL_BirthGen($colname, $lower = "", $uper = "", $date = "")
	{
		$sql = "";

		// 基準日
		if(Mis_empty($date) == 1)
		{
			$y = date("Y");
			$m = date("n");
			$d = date("j");
		}else{
			List($y, $m, $d) = explode("-", $date);
			$y = sprintf("%d", $y);
			$m = sprintf("%d", $m);
			$d = sprintf("%d", $d);
		}

		// 下限
		$Ltmp = "";
		if(Mis_empty($lower) != 1)
		{
			$Ltmp .= sprintf("%04d-", $y - $lower);
			$Ltmp .= sprintf("%02d-", $m);
			$Ltmp .= sprintf("%02d", $d);
			$sql .= " " . $colname . " <= " . MSQL_Safe($Ltmp, 0) . " ";
		}
		// 上限
		$Utmp = "";
		if(Mis_empty($uper) != 1)
		{
			if(Mis_empty($sql) != 1)
			{
				$sql .= " AND ";
			}
			$Utmp .= sprintf("%04d-", $y - ($uper + 1));
			$Utmp .= sprintf("%02d-", $m);
			$Utmp .= sprintf("%02d", $d);
			$sql .= " " . $colname . " > " . MSQL_Safe($Utmp, 0) . " ";

		}

		return $sql;
	}

	/**
	 * nullを0に変換
	**/
	function _null_zero($param)
	{
		if(is_null($param))
		{
			return 0;
		}

		return $param;
	}

	// ------------------------------------------------------------
	// MonthLastDate
	// 年-月-日の月の最終日を返す
	// 2006.11.24 岩井
	// ------------------------------------------------------------
	function MonthLastDate($date)
	{
		list($Y, $m, $d) = explode("-", $date);

		if(MChk_IsDate($Y,$m,$d) == 0)
		{
			return false;
		}

		$y = sprintf("%d", $y);
		$m = sprintf("%d", $m);
		$d = sprintf("%d", $d);

		$big = array(1,3,5,7,8,10,12);
		$sml = array(4,6,9,11);

		// 大の月
		if(in_array($m, $big))
		{
			$Redate = sprintf("%04d", $Y) . "-" . sprintf("%02d", $m) . "-31";
			return $Redate;
		}
		// 小の月
		if(in_array($m, $sml))
		{
			$Redate = sprintf("%04d", $Y) . "-" . sprintf("%02d", $m) . "-30";
			return $Redate;
		}
		// ２月
		if(($Y % 400) == 0)
		{
			// 400の倍数年　→　うるう年
			$Redate = sprintf("%04d", $Y) . "-" . sprintf("%02d", $m) . "-29";
			return $Redate;
		}
		if(($Y % 100) == 0)
		{
			// 100の倍数年　→　非うるう年
			$Redate = sprintf("%04d", $Y) . "-" . sprintf("%02d", $m) . "-28";
			return $Redate;
		}
		if(($Y % 4) == 0)
		{
			// 4の倍数年　→　うるう年
			$Redate = sprintf("%04d", $Y) . "-" . sprintf("%02d", $m) . "-29";
			return $Redate;
		}

		$Redate = sprintf("%04d", $Y) . "-" . sprintf("%02d", $m) . "-28";
		return $Redate;

	}


	/**
	 * 月初のタイムスタンプを得る(奥野)
	 *
	 * $date_or_ts = 日付文字列またはタイムスタンプ, $shift = 月の相対指定
	 * MonthFirstTS(mktime(), 1)  // 来月の月初
	 */
	function MonthFirstTS($date_or_ts, $shift=0)
	{
		if (is_string($date_or_ts))
		{
			$date_or_ts = strtotime($date_or_ts);
			// PHP 5.1.0以前は-1、その後はfalseを返す
			if (!$date_or_ts) {
				return false;
			}
		}

		return mktime(0, 0, 0, date('m', $date_or_ts) + $shift, 1, date('y', $date_or_ts));
	}

	/**
	 * 月末のタイムスタンプを得る(奥野)
	 *
	 * $date_or_ts = 日付文字列またはタイムスタンプ, $shift = 月の相対指定
	 * MonthLastTS(mktime(), -1)  // 先月の月末
	 * "-1 month"をstrtotimeにかけると12/31の場合12/1になってしまうので、
	 * こちらの関数を利用する。
	 *
	 * サンプルコード
	 * for ($i = 0; $i > -48; $i --)
	 * {
	 * 	echo date('Y-m-d H:i:s', MonthFirstTS(mktime(), $i)) . " to ";
	 * 	echo date('Y-m-d H:i:s', MonthLastTS(mktime(), $i)) . "\n";
	 * }
	 *
	 */
	function MonthLastTS($date_or_ts, $shift=0)
	{
		if (is_string($date_or_ts))
		{
			$date_or_ts = strtotime($date_or_ts);
			// PHP 5.1.0以前は-1、その後はfalseを返す
			if (!$date_or_ts) {
				return false;
			}
		}

		return mktime(23, 59, 59, date('m', $date_or_ts) + 1 + $shift, 0, date('y', $date_or_ts));
	}

	/**
	 * SQLで使用できるタイムスタンプ文字列にする(奥野)
	 * $date_or_ts = 日付文字列またはタイムスタンプ
	 */
	function SQLDateTimeStr($date_or_ts)
	{
		if (is_string($date_or_ts))
		{
			$date_or_ts = strtotime($date_or_ts);
			// PHP 5.1.0以前は-1、その後はfalseを返す
			if (!$date_or_ts) {
				return false;
			}
		}

		return date('Y-m-d H:i:s', $date_or_ts);
	}

	/**
	 * 1文字ごとに改行を挿入して疑似縦書き
	 * @param unknown_type $param
	 */
	function SplitCRLF($param, $in_enc = "")
	{
		if(Mis_empty($in_enc) == 1)
		{
			// 文字コード自動判定
			$in_enc = mb_detect_encoding($param);
		}
		$param = mb_convert_encoding($param, 'EUC-JP', $in_enc);

		$strLength = strlen($param);
		$strRet = "";

		for($iCnt = 1; $iCnt <= $strLength; $iCnt++)
		{
			$chkChar = substr($param, ($iCnt - 1), 1);
			$strRet .= $chkChar;
			if(ord($chkChar) > 127)
			{
				$iCnt++;
				$chkChar = substr($param, ($iCnt - 1), 1);
				$strRet .= $chkChar;
			}
			$strRet .= "\n";
		}
		return $strRet;
	}

	/**
	 * 入力されたURLの確認
	 *
	 * @param unknown_type $url
	 */

	function Check_url($url) {

		$split_str = "";
		$split_str = spliti("http://",$url);

		if (trim($split_str[1]) != "") {
			return $url;
		} else {
			return "";
		}

	}

	/**
	 * K値を生成、キーはランダムで先頭8文字
	 * @param unknown_type $param
	 */
	function k_make($param)
	{
		$seed = PwdGenerator(8);
		$result = $seed . (md5($seed . $param . XD_KEY_SEED_COMMON));

		return substr($result, 0, 32);
	}

	/**
	 * 先頭8文字がキー
	 * @param unknown_type $param
	 * @param unknown_type $k
	 */
	function k_check($param, $k)
	{
		if(strlen($k) < 32)
		{
			return false;
		}
		$seed = substr($k, 0, 8);
		$result = substr($k, 8);

		$chkstr = substr(md5($seed . $param . XD_KEY_SEED_COMMON), 0, (32 - 8));

		if($result != $chkstr)
		{
			return false;
		}else{
			return true;
		}
	}

	// トリムと小文字化
	function lowcmp($param)
	{
		return trim(strtolower($param));
	}

	// マルチバイト短縮
	function mb_shorten($param, $len, $enc = 'UTF-8')
	{
		if(mb_strlen($param, $enc) > $len)
		{
			return mb_substr($param, 0, ($len - 1), $enc) . '…';
		}else{
			return $param;
		}
	}

	// メール用強制改行
	function insertLfCode( $str, $n, $enc ){
		//空であれば空を返す
		if( strlen( $str ) <= 0 ){
			return( "" );
		}
		$str_join = "";
		$str_char = "";
		$cnt = 0;
		for( $i = 0; $i < mb_strlen( $str, $enc ); $i++ ){
			$str_char = mb_substr( $str, $i, 1, $enc );
			//セットする文字が半角か全角であるか判別しカウント
			$cnt += checkHankaku( $str_char, $enc ) ? 1 : 2;
			//カウント数が規定文字数以上になっていれば
			if( $cnt >= $n ){
				$cnt = 0;
				$str_join .= "\n";
			}
			//一文字ずつ末尾にセット
			$str_join .= $str_char;
		}
		return( $str_join );
	}
	function json_xencode($value, $options = 0, $unescapee_unicode = true)
	{
		$v = json_encode($value, $options);
		if ($unescapee_unicode) {
			$v = unicode_encode($v);
			// スラッシュのエスケープをアンエスケープする
			$v = preg_replace('/\\\\\//', '/', $v);
		}
		return $v;
	}
	// UTF-8文字列をUnicodeエスケープする。ただし英数字と記号はエスケープしない。
	function unicode_decode($str) {
		return preg_replace_callback("/((?:[^\x09\x0A\x0D\x20-\x7E]{3})+)/", "decode_callback", $str);
	}

	function decode_callback($matches) {
		$char = mb_convert_encoding($matches[1], "UTF-16", "UTF-8");
		$escaped = "";
		for ($i = 0, $l = strlen($char); $i < $l; $i += 2) {
			$escaped .=  "\u" . sprintf("%02x%02x", ord($char[$i]), ord($char[$i+1]));
		}
		return $escaped;
	}

	// Unicodeエスケープされた文字列をUTF-8文字列に戻す
	function unicode_encode($str) {
		return preg_replace_callback("/\\\\u([0-9a-zA-Z]{4})/", "encode_callback", $str);
	}

	function encode_callback($matches) {
		$char = mb_convert_encoding(pack("H*", $matches[1]), "UTF-8", "UTF-16");
		return $char;
	}

	function if_sp()
	{
		$ua=$_SERVER['HTTP_USER_AGENT'];

		if((strpos($ua,'iPhone')!==false)||(strpos($ua,'iPod')!==false)||(strpos($ua,'Android')!==false)) {
			return true;
		}else{
			return false;
		}
	}


	function number_format_nozero($name, $dec = 0, $point = ".", $sep = "")
	{
    	$name = number_format ($name, $dec, $point, "");
    	while(strpos ($name,$point)!= false && substr ($name,strlen ($name)-1,1)=="0")
    	{
	        $name = substr ($name, 0, strlen ($name) - 1);
	        $dec = $dec - 1;
	    }
    	return  number_format ($name, $dec, $point, $sep);
	}
	function number_format_nozero_disp($name, $dec = 0, $point = ".", $sep = ",")
	{
		$name = number_format ($name, $dec, $point, "");
		while(strpos ($name,$point)!= false && substr ($name,strlen ($name)-1,1)=="0")
		{
			$name = substr ($name, 0, strlen ($name) - 1);
			$dec = $dec - 1;
		}
		return  number_format ($name, $dec, $point, $sep);
	}
	function decimalby2($param)
	{
		return number_format_nozero_disp(sprintf('%0.2f',$param), 2);
	}

	// cURL汎用関数
	function curl_get_contents($url, $params=NULL, $post=FALSE, $auth_user=NULL, $auth_pass=NULL, $sslverify=FALSE, $conntimeout=5, $timeout=60)
	{

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);

		if (is_array($params))
		{
			$query = array();
			foreach ($params as $name => $value) {
				$query[] = urlencode($name) . '=' . urlencode($value);
			}
			$query_string = implode('&', $query);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $query_string);
		}
		curl_setopt($ch, CURLOPT_POST, $post);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $sslverify);
		if ($auth_user and $auth_pass)
		{
			curl_setopt($ch, CURLOPT_USERPWD, $auth_user . ':' . $auth_pass);
		}
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $conntimeout);
		curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);

		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_MAXREDIRS, 8);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);



		$contents = curl_exec($ch);
		curl_close($ch);

		return $contents;
	}

	function curl_get_contents_header($url, $params=NULL, $post=FALSE, $auth_user=NULL, $auth_pass=NULL, &$header)
	{
	//	$url = 'http://chkupon.wedding.trueone.co.jp/test/sub.php';

		$last_url = $url;

		$agent = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)";

		$ch = curl_init();
		$options = array(
				CURLOPT_URL            => $url,
				CURLOPT_HEADER         => TRUE,
				CURLOPT_NOBODY         => FALSE,
				CURLOPT_RETURNTRANSFER => TRUE,
				CURLOPT_USERAGENT      => $agent);
		curl_setopt_array($ch, $options);


		curl_setopt($ch, CURLOPT_POST, $post);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		if ($auth_user and $auth_pass)
		{
			curl_setopt($ch, CURLOPT_USERPWD, $auth_user . ':' . $auth_pass);
		}
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_MAXREDIRS, 8);




		$header_data = curl_exec($ch);
		curl_close($ch);

		preg_match("/^HTTP\/[01\.]+ ([0-9]{3}[A-Za-z\(\)\- ]+)/",$header_data,$getcode);
		//$header['base'] = $header_data;
		$header_tmp = $header_data;

		$header_tmp = str_replace("\r\n", "\n", $header_tmp);
		$header_tmp = str_replace("\r", "\n", $header_tmp);
		$header_list = split("\n", $header_tmp);


		foreach($header_list as $line)
		{
			$header['base'] .= $line . "\n";
			if(preg_match('/' . preg_quote('<html', '/') . '/', $line))
			{
				break;
			}
			if(preg_match('/' . preg_quote('<head', '/') . '/', $line))
			{
				break;
			}

			if(preg_match('/^Location:/', $line))
			{
				$line = trim($line);
				$line = str_replace('Location:', '', $line);
				$line = trim($line);
				$last_url = $line;
				$header['last_url'] = $line;
			}

			if(preg_match("/^HTTP\/[01\.]+ ([0-9]{3})/", $line, $matches))
			{
				$header['last_code'] = $matches[1];
				$current_code = $matches[1];
			}
			$match = preg_quote('Content-type:', '/');
			if(preg_match('/^' . $match . '/i', $line))
			{
				$line = trim($line);
				$line = str_replace('Content-type:', '', $line);
				$line = trim($line);
				$last_url = $line;
				$header['last_mime'] = $line;
			}
		}

		return $getcode[1];
	}



?>