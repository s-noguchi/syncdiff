<?php

	// -------------------------------------------------------------
	// CMailSend.inc
	// メール送信管理クラス
	//
	// 2005/02/03
	// -------------------------------------------------------------

	class CMailSendEx extends CMailSend
	{
		// 対象ファイル名
		var $m_Template;
		var $m_Enc;

		// メールヘッダ項目
		var $m_From_Addr;
		var $m_FromJ;
		var $m_Subject;
		var $m_Cc_Addr;
		var $m_Bcc_Addr;
		var $m_ReplyTo_Addr;
		var $m_ReturnPath_Addr;

		var $m_MsgBody;


		// コンストラクタ
		function CMailSendEx($tmpl, $enc = 'EUC-JP')
		{
			// ファイル名
			if(file_exists($tmpl))
			{
				$this->m_Template = $tmpl;
				$this->m_Enc = $enc;
				$this->_readTemplate();

			}
		}

		// スーパークラスコンストラクタ用意
		function Prepare()
		{
			$this->CMailSend($this->m_Subject, $this->m_MsgBody, $this->m_From_Addr, $this->m_ReplyTo_Addr, $this->m_ReturnPath_Addr);
			if(Mis_empty($this->m_FromJ) != 1)
			{
				$this->AddFromJ($this->m_FromJ);
			}

		}

		// 本文取得
		function GetBody()
		{
			return $this->m_MsgBody;
		}
		// 設定
		function SetBody($body)
		{
			$this->m_MsgBody = $body;
		}
		function SetFrom($from)
		{
			$this->m_From_Addr = $from;
		}
		function SetFromJ($fromj)
		{
			$this->m_FromJ = $fromj;
		}
		function SetCC($cc)
		{
			$this->m_Cc_Addr = $cc;
		}
		function SetReplyTo($replyto)
		{
			$this->m_ReplyTo_Addr = $replyto;
		}
		function SetReturnPath($return)
		{
			$this->m_ReturnPath_Addr = $return;
		}


		function _readTemplate()
		{
			$in_header = true;

			$fp = fopen($this->m_Template, "r");
			if(!$fp)
			{
				return false;
			}
			while($line = fgets($fp, 4096))
			{
				$line = mb_convert_encoding($line, 'EUC-JP', $this->m_Enc);

				if($in_header)
				{
					if(eregi('^From:', $line))
					{
						$this->m_From_Addr = trim(substr($line, 6));
					}else if(eregi('^FromJ:', $line))
					{
						$this->m_FromJ = trim(substr($line, 7));
					}else if(eregi('^Subject:', $line))
					{
						$this->m_Subject = trim(substr($line, 9));
					}else if(eregi('^CC:', $line))
					{
						$this->m_Cc_Addr = trim(substr($line, 4));
					}else if(eregi('^BCC:', $line))
					{
						$this->m_Bcc_Addr = trim(substr($line, 5));
					}else if(eregi('^Reply-To:', $line))
					{
						$this->m_ReplyTo_Addr = trim(substr($line, 10));
					}else if(eregi('^Return-Path:', $line))
					{
						$this->m_ReturnPath_Addr = trim(substr($line, 13));
					}

					if(Mis_empty($line) == 1)
					{
						$in_header = false;
					}

				}else{

					// 本文
					$this->m_MsgBody .= $line;
				}
			}
			return true;
		}


		// 送信
		function Send()
		{
			if(is_array($this->m_DeliverTo))
			{
				foreach($this->m_DeliverTo as $val)
				{

					$this->MSend_Mail_Lite($val, $this->m_Cc_Addr, $this->m_Bcc_Addr, $this->m_From, $this->m_From_J, $this->m_ReplyTo, $this->m_MailFrom, $this->m_Sub, $this->m_Body);
				}
			}
		}

		// 送信コマンド
		function MSend_Mail_Lite($to, $cc, $bcc, $from, $fromj, $replyto, $mailfrom, $sub, $body)
		{

	    	// 形式チェック
	    	if(Mchk_IsMailaddr($from) != 1)
	    	{
	    		return 0;
	    	}
	    	if(Mchk_IsMailaddr($to) != 1)
	    	{
	    		return 0;
	    	}
			if(Mchk_IsMailaddr($replyto) != 1)
	    	{
	    		return 0;
	    	}
			if(Mchk_IsMailaddr($mailfrom) != 1)
	    	{
	    		return 0;
	    	}

	    	// 件名のエンコーディング
	    	$sub  = mb_convert_encoding($sub, "JIS", "EUC-JP");
	        $sub  = base64_encode($sub);
	        $sub  = '=?ISO-2022-JP?B?' . $sub . '?=';


			// 本文のJIS化
			$body = mb_convert_encoding($body, 'JIS', 'EUC-JP');

			// from の日本語化
			if(Mis_empty($fromj) != 1)
			{
				$fromj = mb_convert_encoding($fromj, "JIS", "EUC-JP");
				$fromj = base64_encode($fromj);
				$fromj = '=?ISO-2022-JP?B?' . $fromj . '?=';

				$from = $fromj . ' <' . $from . '>';
			}

			if (define('MAILSEND_STOP_TO') and MAILSEND_STOP_TO) { $to = MAILSEND_STOP_TO; }
			if (define('MAILSEND_STOP_CC') and MAILSEND_STOP_CC) { $cc = MAILSEND_STOP_CC; }
			if (define('MAILSEND_STOP_BCC') and MAILSEND_STOP_BCC) { $bcc = MAILSEND_STOP_BCC; }

			return mail($to, $sub, $body, "From: $from\nReply-To: $replyto\nCC: $cc\nBcc: $bcc\nContent-type: text/plain;charset=\"ISO-2022-JP\"", " -f".$mailfrom);

		}


	} // End of class CMailSend definition.

?>
