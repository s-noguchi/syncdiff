<?php

	// -------------------------------------------------------------
	// CMailSend.inc
	// メール送信管理クラス
	//
	// 2005/02/03
	// -------------------------------------------------------------

	class CMailSend{

		// メンバー変数
		var $m_DeliverTo;			// 送信者配列
		var $m_Sub;					// 件名
		var $m_Body;				// 本文
		var $m_From;				// 差出人
		var $m_From_J;				// 日本語差出人名
		var $m_ReplyTo;				// 返信先
		var $m_MailFrom;			// エンベロープFROM

		var $m_CSV;


		// コンストラクタ
		function CMailSend($sub, $body, $from, $replyto, $mailfrom)
		{
			$body = str_replace("\r\n", "\n", $body);
			$body = str_replace("\r", "\n", $body);

			$this->m_Sub = $sub;
			$this->m_Body = $body;

			if(Mchk_IsMailaddr($from) == 1)
			{
				$this->m_From = $from;
			}
			if(Mchk_IsMailaddr($replyto) == 1)
			{
				$this->m_ReplyTo = $replyto;
			}
			if(Mchk_IsMailaddr($mailfrom) == 1)
			{
				$this->m_MailFrom = $mailfrom;
			}

			unset($this->m_DeliverTo);

		}

		// CSVデータの追加
		function SetCSV($param)
		{
			$this->m_CSV = $param;
		}

		// 日本語差出人名の追加
		function AddFromJ($fromj)
		{
			$this->m_From_J = $fromj;
		}

		// 送信先の追加
		function AddTarget($mailto)
		{
			if(Mchk_IsMailaddr($mailto) == 1)
			{
				$this->m_DeliverTo[] = $mailto;
			}
		}


		// 送信
		function Send()
		{

			if(is_array($this->m_DeliverTo))
			{
				foreach($this->m_DeliverTo as $val)
				{

					$this->MSend_Mail_Lite($val, $this->m_From, $this->m_From_J, $this->m_ReplyTo, $this->m_MailFrom, $this->m_Sub, $this->m_Body);
				}
			}
		}

		// 送信コマンド
		function MSend_Mail_Lite($to, $from, $fromj, $replyto, $mailfrom, $sub, $body)
		{
	    	// 形式チェック
	    	if(Mchk_IsMailaddr($from) != 1)
	    	{
	    		return 0;
	    	}
	    	if(Mchk_IsMailaddr($to) != 1)
	    	{
	    		return 0;
	    	}
			if(Mchk_IsMailaddr($replyto) != 1)
	    	{
	    		return 0;
	    	}
			if(Mchk_IsMailaddr($mailfrom) != 1)
	    	{
	    		return 0;
	    	}

	    	// 件名のエンコーディング
	    	$sub  = mb_convert_encoding($sub, "JIS", "UTF-8");
	        $sub  = base64_encode($sub);
	        $sub  = '=?ISO-2022-JP?B?' . $sub . '?=';


			// 本文のJIS化
			$body = mb_convert_encoding($body, 'JIS', 'UTF-8');

			// from の日本語化
			if(Mis_empty($fromj) != 1)
			{
				$fromj = mb_convert_encoding($fromj, "JIS", "UTF-8");
				$fromj = base64_encode($fromj);
				$fromj = '=?ISO-2022-JP?B?' . $fromj . '?=';

				$from = $fromj . ' <' . $from . '>';
			}

			// if (define('MAILSEND_STOP_TO') and MAILSEND_STOP_TO) { $to = MAILSEND_STOP_TO; }

			return mail($to, $sub, $body, "From: $from\nReply-To: $replyto\nContent-type: text/plain;charset=\"ISO-2022-JP\"", " -f".$mailfrom);
//			return $this->mail_ex($to, $sub, $body, $from, $this->m_CSV);

		}


  		function mail_ex($to, $sub, $body, $from, $csv)
  		{
  			// スプリッタ
			$boundary = "-*-*-*-*-*-*-*-*-Boundary_" . uniqid("b");
			// 添付ファイルのエンコーディング
			$attach = chunk_split(base64_encode($csv));

			// パイプ経由
		    $mp = popen(CRT_SENDMAIL . " -f $from $to", "w");

		    ########################## メールの組み上げ
		    ### 全体のヘッダ
		    fputs($mp, "MIME-Version: 1.0\n");
		    fputs($mp, "Content-Type: Multipart/Mixed; boundary=\"$boundary\"\n");
		    fputs($mp, "Content-Transfer-Encoding:Base64\n");
		    fputs($mp, "From: $from\n");
		    fputs($mp, "To: $to\n");
		    fputs($mp, "Subject: $sub\n");

		    ### メール本文のパート
		    fputs($mp, "--$boundary\n");
		    fputs($mp, "Content-Type: text/plain; charset=\"ISO-2022-JP\"\n");
		    fputs($mp, "\n");
		    fputs($mp, "$body\n");

		    ### 添付ファイルのパート
		    fputs($mp, "--$boundary\n");
		    fputs($mp, "Content-Type: application/octet-stream; name=\"present.csv\"\n");
		    fputs($mp, "Content-Transfer-Encoding: base64\n");
		    fputs($mp, "Content-Disposition: attachment; filename=\"present.csv\"\n");
		    fputs($mp, "\n");
		    fputs($mp, "$attach\n");
		    fputs($mp, "\n");

		    ### マルチパートのおわり。
		    fputs($mp, "--$boundary" . "--\n");
		    pclose($mp);
		}


	} // End of class CMailSend definition.

?>
