<?php

	/**
	 * 配列を復元するキャッシュファイルを作成、管理するクラス
	 *
	 */

	// 設定ファイルをキャッシュにした場合のファイル名プリフィクス
	define("XD_CACHE_PREFIX_CONFIG_IN", 'CONFIG_IN_', FALSE);
	define("XD_CACHE_PREFIX_CONFIG_OUT", 'CONFIG_OUT_', FALSE);
	define("XD_CACHE_PREFIX_TEMPLATE", 'CONFIG_TEMPLATE_', FALSE);

	class CCacheCtrl extends CBaseObject
	{
		// メンバ変数
		var $m_CacheName;

		// キャッシュファイル本体
		var $m_Body;

		// コンストラクタ
		function CCacheCtrl($cache)
		{
			$this->m_CacheName = $cache;
		}



		// キャッシュ作成
		function makeCache($varname, &$target)
		{
			// 変数名
			if(is_array($varname))
			{
				$vname = '$' . $varname[0] . '->' . $varname[1];
			}else{
				$vname = $varname;
			}

			$strOut  = "";
			$strOut .= ' unset(' . $vname . '); ' . "\n";
			$strOut  = $this->_lineArray($strOut, $vname, $target);

			$this->m_Body .= $strOut;
		}

		function flush()
		{

			$this->m_Body = '<?php' . "\n" . $this->m_Body . "\n";
			$this->m_Body .= "?>";

			$fp = @fopen(XD_CACHE_DIR . '/' . $this->m_CacheName . '.php', "w");
			if(!$fp)
			{
				return false;
			}
			flock($fp, 2);
			fputs($fp, $this->m_Body, strlen($this->m_Body));
			fclose($fp);
			return true;

		}

		/**
		 * キャッシュファイル内の配列設定部分
		 * （再帰呼び出しあり）
		 *
		 * @param unknown_type $strOut
		 * @param unknown_type $vname
		 * @param unknown_type $ar
		 * @return unknown
		 */
		function _lineArray($strOut, $vname, &$ar)
		{
			if(is_array($ar))
			{
				foreach($ar as $key => $val)
				{
					if(is_array($val))
					{
						// エントリが配列だった場合は再帰呼び出し
						$strOut = $this->_lineArray($strOut, $vname . "['" . addslashes($key) . "']", $val);
					}else{
						// スカラだった場合はコード記述
						$strOut .= " " . $vname . "['" . addslashes($key) . "'] = '" . $this->_addslashes($val) . "'; \n";
					}
				}
			}
			return $strOut;

		}

		function _addslashes($param)
		{
			// \ と' のみをエスケープする
			$param = str_replace('\\', '\\\\', $param);
			$param = str_replace('\'', '\\\'', $param);

			return $param;

		}
	}
?>