<?php

	class CFileMgr extends CBaseObject
	{

		var $m_File;		// 管理対象ファイル
		var $m_FileInfo;	// 管理対象ファイルの情報

		function CFileMgr($file = "")
		{
			// スーパークラスコンストラクタ
			$this->CBaseObject();

			// ファイルがあれば属性情報取得
			if(Mis_empty($file) != 1)
			{
				$this->m_File = $file;
				$this->_stat();
			}
		}

		/**
		 * ファイル情報取得
		 *
		 */
		function _stat()
		{
			if(file_exists($this->m_File))
			{
				$this->m_FileInfo = stat($this->m_File);
			}
		}

		/**
		 * 最終アクセス時間
		 *
		 * @return unknown
		 */
		function getLastmod()
		{
			return $this->m_FileInfo['mtime'];
		}

		/**
		 * ファイルのバイトサイズ取得
		 *
		 */
		function getSize()
		{
			return $this->m_FileInfo['size'];
		}
	}