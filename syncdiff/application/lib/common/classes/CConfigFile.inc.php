<?php

	// -----------------------------------------------
	// CConfigFile.inc
	// PHP Webアプリケーションフレームワーク
	// フォーム定義ファイル管理クラス
	// 2005.04.10
	// -----------------------------------------------

	//フォーム定義
	// HTML名
	// ラベル
	// コントロール
	// 必須
	// 入力タイプ
	// 最大長
	// 選択肢（値）
	// 選択肢（ラベル）
	// デフォルト値

	define("WEBAPP_FORM_CONTROL_NONE",	 	'0', FALSE);
	define("WEBAPP_FORM_CONTROL_TEXT", 		'1', FALSE);
	define("WEBAPP_FORM_CONTROL_RADIO", 	'2', FALSE);
	define("WEBAPP_FORM_CONTROL_CHECK", 	'3', FALSE);
	define("WEBAPP_FORM_CONTROL_SELECT",	'4', FALSE);
	define("WEBAPP_FORM_CONTROL_LIST", 		'5', FALSE);
	define("WEBAPP_FORM_CONTROL_PASSWORD",	'6', FALSE);
	define("WEBAPP_FORM_CONTROL_FILE", 		'7', FALSE);
	define("WEBAPP_FORM_CONTROL_TEXTAREA", 	'8', FALSE);

	define("WEBAPP_FORM_MAND_NO", 			'0', FALSE);
	define("WEBAPP_FORM_MAND_YES", 			'1', FALSE);

	define("WEBAPP_FORM_VALIDATION_NONE", 	'0', FALSE);
	define("WEBAPP_FORM_VALIDATION_NUM", 	'1', FALSE);
	define("WEBAPP_FORM_VALIDATION_ALP", 	'2', FALSE);
	define("WEBAPP_FORM_VALIDATION_ALN", 	'3', FALSE);
	define("WEBAPP_FORM_VALIDATION_ASC", 	'4', FALSE);
	define("WEBAPP_FORM_VALIDATION_MAIL", 	'5', FALSE);
	define("WEBAPP_FORM_VALIDATION_ZEN", 	'6', FALSE);
	define("WEBAPP_FORM_VALIDATION_HIRA", 	'7', FALSE);
	define("WEBAPP_FORM_VALIDATION_KATA", 	'8', FALSE);


	define("XD_CONFIGCACHE_FILEPREFIX", 	'CONFIG_', FALSE);


	class CConfigFile extends CBaseObject
	{
		// メンバー変数
		var $m_Config;			// 設定情報
		var $m_ConfigFile;		// 設定ファイル
		var $m_NameCache;		// 設定ファイルキャッシュ


		// コンストラクタ
		function CConfigFile()
		{
			// スーパークラス
			$this->CBaseObject();

			// 初期化
			unset($this->m_Config);

		}

		// ファイル設定
		function SetConfigFile($config_file)
		{
			if(file_exists($config_file))
			{
				$this->m_ConfigFile = $config_file;
			}
		}

		/**
		 * キャッシュ用コンフィグ位置情報
		 *
		 * @param unknown_type $app
		 * @param unknown_type $task
		 * @param unknown_type $module
		 */
		function setConfigNames($app, $task, $module)
		{
			$this->m_NameCache = $app . '_' . $task . '_' . $module;
		}



		// ファイルから設定を読み込み
		function ReadConfig()
		{

			if(!file_exists($this->m_ConfigFile))
			{
				$this->_set_error('設定ファイルがありません' . "\n");
				return 0;
			}

			// キャッシュを読むか再読込か
			if(file_exists(XD_CACHE_DIR . '/' . $this->m_NameCache . '.php'))
			{
				// 最終更新時刻を取得
				$cachefile_stat = stat(XD_CACHE_DIR . '/' . $this->m_NameCache . '.php');
				$configfile_stat = stat($this->m_ConfigFile);

				if($cachefile_stat['mtime'] >= $configfile_stat['mtime'])
				{
					// キャッシュを読み込む
					require_once(XD_CACHE_DIR . '/' . $this->m_NameCache . '.php');
					return 1;
				}
			}


			$fp = fopen($this->m_ConfigFile, "r");
			if(!$fp)
			{
				$this->_set_error('設定ファイルのOPENに失敗しました' . "\n");
				return 0;
			}

			$current_line = 0;
			$format_error = 0;
			while($line = fgets($fp, 8192))
			{
				$line = rtrim($line);

				if(Mis_empty($line) != 1)
				{
					$current_line++;

					// UTF-8対応
					$line = mb_convert_encoding($line, 'UTF-8', mb_detect_encoding($line));
					$rec = split("\t", $line);

					if($this->_chk_line($rec) != 1)
					{
						$this->_set_error('Invalid file format at line ' . $current_line);
						$format_error = 1;
						break;
					}

					$this->m_Config[$rec[0]]['name'] = $rec[0];
					$this->m_Config[$rec[0]]['label'] = $rec[1];
					$this->m_Config[$rec[0]]['control'] = $rec[2];
					$this->m_Config[$rec[0]]['mandatory'] = $rec[3];
					$this->m_Config[$rec[0]]['dtype'] = $rec[4];
					$this->m_Config[$rec[0]]['maxlength'] = $rec[5];

					if(Mis_empty($rec[6]) != 1)
					{
						$sel_values = split(";", $rec[6]);
						$sel_labels = split(";", $rec[7]);

						$tmp_ar = array();
						foreach($sel_values as $selkey => $selval)
						{
							$tmp_ar[$selval] = $sel_labels[$selkey];
						}
						$this->m_Config[$rec[0]]['selectees'] = $tmp_ar;
					}
					$this->m_Config[$rec[0]]['default'] = split(";", $rec[8]);
				}
			}
			fclose($fp);

			// config を再構築するキャッシュファイルの作成
			$cache = new CCacheCtrl($this->m_NameCache);
			$cache->makeCache(array('this', 'm_Config'), $this->m_Config);
			$cache->flush();




			if($format_error == 1)
			{
				return 0;
			}else{
				return 1;
			}
		}

		// HTML名一覧の取得
		function GetNameList()
		{
			unset($ret);

			if(is_array($this->m_Config))
			{
				foreach(array_keys($this->m_Config) as $key)
				{
					$ret[] = $key;
				}
			}

			return $ret;
		}

		// 設定の取得
		function GetConfigInfo($name, $colname)
		{
			return $this->m_Config[$name][$colname];
		}
		// 設定の設定
		function SetConfigInfo($name, $params)
		{
			$cols = array(
				'name',
				'label',
				'control',
				'mandatory',
				'dtype',
				'maxlength',
				'selectees',
				'default'
			);

			foreach($cols as $param)
			{
				$this->m_Config[$name][$param] = $params[$param];
			}
		}
		// 設定の詳細設定
		function SetConfigInfoDetail($name, $params)
		{
			$cols = array(
				'name',
				'label',
				'control',
				'mandatory',
				'dtype',
				'maxlength',
				'selectees',
				'default'
			);

			foreach($params as $param => $value)
			{
				if(in_array($param, $cols))
				{
					$this->m_Config[$name][$param] = $value;
				}
			}
		}

		// 選択肢のラベル取得
		function GetSelecteeLabel($name, $key)
		{
			return $this->m_Config[$name]['selectees'][$key];
		}


		// 選択肢の動的設定
		function ReplaceSelectee($name, $selectees)
		{
			if(is_array($selectees))
			{
				$this->m_Config[$name]['selectees'] = $selectees;
			}
		}




		// 設定ファイルの形式をチェック
		function _chk_line($rec)
		{

			// 必須チェック
			$mandatories = array(0, 2, 3);
			foreach($mandatories as $val)
			{
				if(trim($rec[$val]) == "")
				{
					return 0;
				}
			}
			// 型チェック
			$numerics =array(2, 3, 4);
			foreach($numerics as $val)
			{
				if(Mis_empty($rec[$val]) != 1)
				{
					if(Mchk_IsNumber(trim($rec[$val])) != 1)
					{
						return 0;
					}
				}
			}
			// 選択肢数チェック
			if(Mis_empty($rec[6]) != 1)
			{
				$tmp_keys = split(";", $rec[6]);
				$tmp_vals = split(";", $rec[7]);

				if(!is_array($tmp_keys))
				{
					return 0;
				}
				if(!is_array($tmp_vals))
				{
					return 0;
				}
				if(count($tmp_keys) != count($tmp_vals))
				{
					return 0;
				}
			}

			return 1;
		}


	} // End of class CConfigFile definition.

?>