<?php

	// -------------------------------------------------------
	// CHidden.inc
	// CHiddenクラス
	// HIDDENタグ生成クラス
	// 2005.04.05
	// -------------------------------------------------------

	class CHidden
	{

		// メンバー変数
		var $m_Hidden;

		// コンストラクタ
		function CHidden()
		{
			unset($this->m_Hidden);
		}

		// 追加
		function Add($name, $value)
		{
			$in_ar = array($name => $value);
			$this->m_Hidden[] = $in_ar;
		}

		// 生成
		function Generate()
		{
			$strRet = "";
			if(is_array($this->m_Hidden))
			{
				foreach($this->m_Hidden as $val)
				{
					foreach($val as $inkey => $inval)
					{
						$strRet .= '<input type="hidden" name="' . $inkey . '" value="' . Mhtml_quote($inval) . '" />' . "\n";
					}
				}
			}

			return $strRet;

		}

		// クォートエスケープ
		function _escape_dquote($param)
		{
			$param = str_replace('"', '&quot;', $param);

			return $param;
		}


	} // End of class CHidden definition.

?>