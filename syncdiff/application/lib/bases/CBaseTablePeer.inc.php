<?php

	// CBaseTablePeer
	// データベース単一操作クラス（ベース）
	// 2005.07.09
	// ※このベースクラスでDBの差分を吸収

	// 列定義構造体
	// $m_Columns = array(
	//				<colname> = array(
	//								type => 0/1,		// 0:文字 1:数値
	//								len => 255, 		// 長さ
	//								pk => t/f,			// t: PK f:非PK
	//								auto => t/f,		// t: 自動採番 f: 非自動採番
	//								mandatory => t/f	// t: 必須 f:任意
	//							)
	// 				)


	class CBaseTablePeer extends CBaseObject
	{

		// メンバー変数
		var $m_Table;					// テーブル名
		var $m_Columns;					// フィールド定義
		var $m_Conn;					// データベース接続ハンドル
		var $m_Data;					// データ
		var $m_PKCols;					// プライマリーキー列
		var $m_AutoCol;					// 自動採番列

		// コンストラクタ
		function CBaseTablePeer($connection)
		{
			// 親クラスコンストラクタ
			$this->CBaseObject();

			// 初期化
			$this->m_Table = "";
			unset($this->m_Columns);
			unset($this->m_Data);

			// 接続ハンドル
			$this->m_Conn = $connection;


			// 初期値定義
			$this->_setParameters();

			// PK列取得
			$this->_getPKCols();

			// 自動採番列取得
			$this->_getAutoCol();

		}


		// 仮想関数
		function _setParameters()
		{
			// 派生クラスのこの関数にて、
			// $this->m_Table
			// $this->m_Columns
			// をセット

			return true;
		}

		function getColList()
		{
			unset($ret_ar);
			if(is_array($this->m_Columns))
			{
				foreach($this->m_Columns as $col => $rec)
				{
					$ret_ar[] = $col;
				}
			}

			return $ret_ar;
		}

		// PK取得
		function _getPKCols()
		{
			if(!is_array($this->m_Columns))
			{
				$this->_set_error('No columns defined.');
				return false;
			}
			// PK列取得
			unset($this->m_PKCols);
			foreach($this->m_Columns as $col => $def)
			{
				if($def['pk'])
				{
					$this->m_PKCols[] = $col;
				}
			}

			return true;
		}

		// 自動採番列取得
		function _getAutoCol()
		{
			if(!is_array($this->m_Columns))
			{
				$this->_set_error('No columns defined.');
				return false;
			}
			// PK列取得
			unset($this->m_AutoCol);
			foreach($this->m_Columns as $col => $def)
			{
				if($def['auto'])
				{
					$this->m_AutoCol = $col;
					break;
				}
			}

			return true;
		}



		// 属性
		function setAttr($col, $data)
		{
			if(!in_array($col, $this->m_Columns))
			{
//				$this->_set_error($col . ' does not exists.');
//				return false;
			}
			if(!$this->_chkParam($col, $data))
			{
				$this->_set_error($col . ' does not fullfill condition.');
				return false;
			}

			$this->m_Data[$col] = $data;
			return true;
		}
		function getAttr($col)
		{
			return $this->m_Data[$col];
		}





		// レコード取得
		function getByPK($pks)
		{
			// バリデーション
			if(!$this->_chkPKParam($pks))
			{
				return false;
			}

			$sql  = "";
			$sql .= " SELECT * FROM " . $this->m_Table . " ";
			$sql .= " WHERE ";

			$pk_clause = "";
			foreach($pks as $col => $val)
			{
				if(Mis_empty($pk_clause) == 1)
				{
					$pk_clause .= " " . $col . " = " . MSQL_Safe($val, $this->m_Columns[$col]['type']) . " ";
				}else{
					$pk_clause .= " AND ";
					$pk_clause .= " " . $col . " = " . MSQL_Safe($val, $this->m_Columns[$col]['type']) . " ";
				}
			}
			$sql .= $pk_clause;

			$result = $this->_doQuery($sql, $this->m_Conn);
			if(!$result)
			{
				$this->_set_error($this->_doError(_doError));
				return false;
			}

			if($this->_doCount($result) != 1)
			{
				unset($this->m_Data);
				return true;
			}

			$this->m_Data = $this->_doFetch($result, 0);

			return true;
		}



		// レコード更新
		function updateByPK($pks)
		{
			// バリデーション
			if(!$this->_chkPKParam($pks))
			{
				return false;
			}

			if(!is_array($this->m_Data))
			{
				$this->_set_error('no data for update');
				return false;
			}

			$sql  = "";
			$sql .= " UPDATE " . $this->m_Table . " ";
			$sql .= " SET ";

			$update_clause = "";
			foreach($this->m_Data as $col => $val)
			{
				if(Mis_empty($update_clause) == 1)
				{
					$update_clause  = " " . $col . " = " . MSQL_Safe($val, $this->m_Columns[$col]['type']) . " ";
				}else{
					$update_clause  .= " ,\n" . $col . " = " . MSQL_Safe($val, $this->m_Columns[$col]['type']) . " ";
				}
			}
			$sql .= $update_clause;

			$sql .= " WHERE ";

			$pk_clause = "";
			foreach($pks as $col => $val)
			{
				if(Mis_empty($pk_clause) == 1)
				{
					$pk_clause .= " " . $col . " = " . MSQL_Safe($val, $this->m_Columns[$col]['type']) . " ";
				}else{
					$pk_clause .= " AND ";
					$pk_clause .= " " . $col . " = " . MSQL_Safe($val, $this->m_Columns[$col]['type']) . " ";
				}
			}
			$sql .= $pk_clause;

			$result = $this->_doQuery($sql, $this->m_Conn);
			if(!$result)
			{
				$this->_set_error($this->_doError($this->m_Conn));
				return false;
			}

			return true;
		}

		// レコードインサート
		function insertData()
		{

			if(!$this->_chkEntireParam())
			{
				return false;
			}

			$sql  = "";
			$sql .= " INSERT INTO " . $this->m_Table . " ( ";

			$colname_lines = "";
			$colparam_lines = "";
			foreach($this->m_Columns as $col => $def)
			{
				if($def['auto'])
				{
					// 自動採番列は無視
					continue;
				}
				if(Mis_empty($colname_lines) == 1)
				{
					$colname_lines = " " . $col . " ";
				}else{
					$colname_lines .= ", " . $col . " ";
				}

				if(Mis_empty($colparam_lines) == 1)
				{
					$colparam_lines = " " . MSQL_Safe($this->m_Data[$col], $def['type']) . " ";
				}else{
					$colparam_lines .= ", " . MSQL_Safe($this->m_Data[$col], $def['type']) . " ";
				}
			}

			$sql .= " " . $colname_lines . " ";
			$sql .= " ) VALUES ( ";
			$sql .= " " . $colparam_lines . " ";
			$sql .= " ) ";



			$result = $this->_doQuery($sql, $this->m_Conn);
			if(!$result)
			{
				$this->_set_error($this->_doError($this->m_Conn));
				return false;
			}


			return true;
		}


		// PKパラメータチェック
		function _chkPKParam($pks)
		{
			if(!is_array($pks))
			{
				$this->_set_error('PK parameter mest be an array.');
				return false;
			}
			if(!is_array($this->m_PKCols))
			{
				$this->_set_error('PK definition mest be an array.');
				return false;
			}
			foreach($this->m_PKCols as $pk)
			{
				if(Mis_empty($pks[$pk]) == 1)
				{
					$this->_set_error('All PK parameter must not be empty.');
					return false;
				}
			}

			return true;
		}

		// 個別バリデーションチェック
		function _chkParam($col, $data)
		{
			// 必須検査
			if($this->m_Columns[$col]['mandatory'])
			{
				if(Mis_empty($data) == 1)
				{
					return false;
				}
			}

			// 数値検査
			if($this->m_Columns[$col]['type'] == '1')
			{
				$testee = str_replace('.', '', $data);
				if(Mis_empty($testee) != 1)
				{
					if(Mchk_IsNumber($testee) != 1)
					{
						return false;
					}
				}
			}

			// データ長検査
			if(strlen($data) > $this->m_Columns[$col]['len'])
			{
				return false;
			}

			return true;
		}

		// 全体バリデーションチェック
		function _chkEntireParam()
		{
			foreach($this->m_Columns as $col => $def)
			{
				if(($def['mandaotry']) && (!$def['auto']))
				{
					if(Mis_empty($this->m_Data[$col]) == 1)
					{
						$this->_set_error($col . ' is empty.');
						return false;
					}
				}

				if($def['type'] == '1')
				{
					$testee = str_replace('.', '', $this->m_Data[$col]);
					if(Mis_empty($testee) != 1)
					{
						if(Mchk_IsNumber($testee) != 1)
						{
							$this->_set_error($col . ' must be numeric.');
							return false;
						}
					}
				}

				if(strlen($this->m_Data[$col]) > $def['len'])
				{
					$this->_set_error($col . ' is too long.');
					return false;
				}
			}

			return true;
		}


		// ラッパ関数
		function _doQuery($sql, $link)
		{
			return mysql_query($sql, $link);
		}
		function _doError($link)
		{
			return mysql_error($link);
		}
		function _doCount($result)
		{
			return mysql_num_rows($result);
		}
		function _doFetch($result, $id)
		{
			return mysql_fetch_assoc($result);
		}



	}
?>