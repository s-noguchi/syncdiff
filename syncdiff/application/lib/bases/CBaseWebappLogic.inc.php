<?php

	// -----------------------------------------------
	// CBaseWebapp.inc
	// PHP Webアプリケーションフレームワークベースクラス
	// 2005.04.10
	// -----------------------------------------------

	define("CRT_ERRMSG_NODATA", 	1, FALSE);
	define("CRT_ERRMSG_NOSEL", 		2, FALSE);
	define("CRT_ERRMSG_INVTYPE", 	3, FALSE);
	define("CRT_ERRMSG_DATALEN", 	4, FALSE);
	define("CRT_ERRMSG_FILELEN", 	5, FALSE);
	define("CRT_ERRMSG_RANGE", 		6, FALSE);
	define("CRT_ERRMSG_INVEXT",		7, FALSE);
	define("CRT_ERRMSG_INVMETHOD",	8, FALSE);

	class CBaseWebappLogic extends CBaseObject
	{

		var $m_Method;			// メソッド

		var $m_Request;			// リクエストデータ
		var $m_Result;			// 結果データ

		var $m_InEncoding;		// リクエストデータエンコーディング
		var $m_IsMagicQuotes;	// MagicQuotesGPC
		var $m_IsMultiPart;		// ファイルアップロードフラグ
		var $m_IsFileDeletion;	// ファイルNull上書きフラグ
		var $m_BadExtentions;	// 禁止アップロード拡張子

		var $m_Config;			// 入力用フォーム設定オブジェクト
		var $m_ConfigFile;		// 入力用設定ファイル

		var $m_Validation;		// バリデーション結果オブジェクト
		var $m_InvalidErrMsg;	// バリデーションエラーメッセージ

		var $m_UploadTmpDir;	// アップロード一時フォルダ
		var $m_UploadDir;		// アップロードフォルダ
		var $m_UploadTmpURI;	// アップロード一時フォルダURI
		var $m_UploadURI;		// アップロードフォルダURI
		var $m_OkExts;			// 許可拡張子

		var $m_ModuleDir;		// モジュールディレクトリ

		var $m_Laby;			// デバッグオブジェクト

		var $m_ErrStruct;		// バリデーションエラーオブジェクト

		var $m_DBFileStore;		// アップロード先はDB
		var $m_DBFilePublish;	// アップロードファイルの公開可否


		// -----------------------------------------------
		// コンストラクタ
		// -----------------------------------------------
		function CBaseWebappLogic($config_file)
		{
		
			global $DISPATCH_CLASS_NAMES;
			global $g_name_task, $g_name_module;


			// スーパークラスコンストラクタ
			$this->CBaseObject();

			// 初期化
			unset($this->m_Data);
			unset($this->m_Config);

			$this->m_InvalidErrMsg = "";

			// 自動クォート処理確認
			if(get_magic_quotes_gpc() == 1)
			{
				$this->m_IsMagicQuotes = 1;
			}else{
				$this->m_IsMagicQuotes = 0;
			}

			// モジュールディレクトリ
			$tmp_module = split(';', XD_SUBSYS_DIR . '/' . $DISPATCH_CLASS_NAMES[$g_name_task][$g_name_module]);
			$this->m_ModuleDir = $tmp_module[0];


			// メソッド
			$this->m_Method = $_SERVER['REQUEST_METHOD'];

			// フォーム設定オブジェクト
			$this->m_Config = new CConfigFile();
			if((file_exists($config_file)) && (Mis_empty($config_file) != 1))
			{
				$this->m_ConfigFile = $config_file;
				$this->m_Config->SetConfigFile($this->m_ConfigFile);
				$this->m_Config->setConfigNames(XD_CACHE_PREFIX_CONFIG_IN . md5(XD_SUBSYS_ROOT), $g_name_task, $g_name_module);
			}

			// デバッグオブジェクト
			$this->m_Laby = new CTest();

			// エンコーディングデフォルト
			$this->m_InEncoding = 'EUC-JP';

			// 各種設定
			$this->m_IsMultiPart = 0;
			$this->m_IsFileDeletion = 0;
			$this->m_UploadTmpDir = "";
			$this->m_UploadDir = "";
			$this->m_UploadTmpURI = "";
			$this->m_UploadURI = "";
			$this->m_DBFileStore = false;
			unset($this->m_OkExts);

			// リダイレクトはデフォルトFALSE
			$this->m_Result['_redirect'] = false;

			return;
		}

		// -----------------------------------------------
		// フォーム定義ファイル設定
		// -----------------------------------------------
		function SetConfigFile($config_file)
		{
			if(Mis_empty($config_file) == 1)
			{
				$this->_set_error('Empty config_file parameter.');
				return false;
			}
			if(!file_exists($config_file))
			{
				$this->_set_error('No configuration file.');
				return false;
			}

			$this->m_ConfigFile = $config_file;
			$this->m_Config->SetConfigFile($this->m_ConfigFile);

			return true;
		}

		// -----------------------------------------------
		// フォーム情報の設定
		// -----------------------------------------------
		function SetFormConfig($name, $params)
		{
			$this->m_Config->SetConfigInfo($name, $params);
			return true;
		}
		// -----------------------------------------------
		// 入力データのエンコーディング
		// EUC-JP, SJIS, JIS
		// -----------------------------------------------
		function SetInEncoding($param)
		{
			$this->m_InEncoding = $param;
		}
		// -----------------------------------------------
		// 許可するメソッド
		// GET/POST/GETPOST
		// -----------------------------------------------
		function SetMethod($param)
		{
			$this->m_Method = $param;
		}


		// -----------------------------------------------
		// ファイルアップロード時の設定
		// $multi: 0通常 1multipart/form-data
		// $deletion: 0:Null上書きしない 1:Null上書きする
		// $tmpupdir: 一時ディレクトリ
		// $tmpupuri: 一時ディレクトリURI
		// $updir: アップロードディレクトリ
		// $updiruri: アップロードディレクトリURI
		// $exts: アップロード禁止拡張子
		// -----------------------------------------------
		function SetUploadConfig($multi, $deletion, $tmpupdir, $tmpupuri, $updir, $upuri, $exts)
		{
			$this->m_IsMultiPart = $multi;
			$this->m_IsFileDeletion = $deletion;
			if(is_array($exts))
			{
				$this->m_BadExtentions = $exts;
			}

			// 書き込みチェック
			if(Mis_empty($tmpupdir) != 1)
			{
				if(!is_writable($tmpupdir))
				{
					$this->_set_error('The directory is not writable.' . $tmpupdir);
					return 0;
				}
			}
			if(Mis_empty($updir) != 1)
			{
				if(!is_writable($updir))
				{
					$this->_set_error('The directory is not writable.' . $updir);
					return 0;
				}
			}

			$this->m_UploadTmpDir = $tmpupdir;
			$this->m_UploadTmpURI = $tmpupuri;
			$this->m_UploadDir = $updir;
			$this->m_UploadURI = $upuri;
			$this->m_OkExts = $exts;

			return 1;
		}

		// -----------------------------------------------
		// ファイルアップロード時の設定
		// （PostgreSQLに登録）
		// $publish: 公開可能フラグ
		// $exts: アップロード禁止拡張子
		// -----------------------------------------------
		function SetDBUploadConfig($publish, $exts)
		{
			$this->m_IsMultiPart = 1;
			$this->m_IsFileDeletion = 0;
			if(is_array($exts))
			{
				$this->m_BadExtentions = $exts;
			}

			$this->m_DBFileStore = true;
			$this->m_DBFilePublish = $publish;
			$this->m_OkExts = $exts;

			return 1;
		}


		// -----------------------------------------------
		// 処理メインフロー
		// -----------------------------------------------
		function doTask()
		{
			// フォーム初期設定
			if(!$this->initParam())
			{
				$this->handleError();
			}

			// 受信
			if(!$this->Bless())
			{
				$this->handleError();
			}

			// 受信直後変換
			if(!$this->_convert())
			{
				$this->handleError();
			}

			// 処理実行
			if(!$this->Execute())
			{
				$this->handleError();
			}

		}

		// -----------------------------------------------
		// パラメータイニシャライズ
		// -----------------------------------------------
		function initParam()
		{
			if((file_exists($this->m_ConfigFile)) && (Mis_empty($this->m_ConfigFile) != 1))
			{
				if(!$this->_read_config())
				{
					return false;
				}
			}
			return true;
		}


		// -----------------------------------------------
		// データ読み込み
		// 受信データからm_Request を作成
		// -----------------------------------------------
		function Bless()
		{

			if($this->m_Method == CRT_HTTP_METHOD_GET)
			{
				$_request = $_GET;
			}else if($this->m_Method == CRT_HTTP_METHOD_POST)
			{
				$_request = $_POST;
			}else if($this->m_Method == CRT_HTTP_METHOD_BOTH)
			{
				if($_SERVER['REQUEST_METHOD'] == CRT_HTTP_METHOD_GET)
				{
					$_request = $_GET;
				}else if($_SERVER['REQUEST_METHOD'] == CRT_HTTP_METHOD_POST)
				{
					$_request = $_POST;
				}
			}else{
				$this->_set_error('No method defined.');
				return false;
			}


			if(is_array($this->m_Config->GetNameList()))
			{
				foreach($this->m_Config->GetNameList() as $val)
				{
					if(($this->m_Config->GetConfigInfo($val, 'control') == WEBAPP_FORM_CONTROL_FILE) && ($this->m_IsMultiPart == 1))
					{
						// ファイルアップロードの場合
						$SizeImg = Read_ImgConfig($this->m_ModuleDir . "/img.cfg");
						if(is_array($SizeImg))
						{
							$ImgOk = Mis_upload_img($val, $this->m_OkExts, $SizeImg);
						}else{
							$ImgOk = 1;
						}

						if($ImgOk == 1)
						{
							if($this->m_DBFileStore)
							{
								$sTmp = RetrieveUpFileDB($val, $this->m_UploadTmpDir, $this->m_DBFilePublish, $this->m_OkExts);
							}else{
								$sTmp = RetrieveUpFile($val, $this->m_UploadTmpDir, $this->m_OkExts);
							}
						}else{
							$sTmp = 'inv';
						}

						if($sTmp == 'inv')
						{
							$this->_set_InvalidErrMsg($this->errMsgGen(CRT_ERRMSG_INVEXT, array('name' => $this->m_Config->GetConfigInfo($val, 'label'))));
							$sTmp = "";
						}
						if($this->m_IsFileDeletion == 1)
						{
							$this->m_Request[$val] = $sTmp;
						}else{
							if(Mis_empty($sTmp) != 1)
							{
								$this->m_Request[$val] = $sTmp;
							}
						}
					}else if(($this->m_Config->GetConfigInfo($val, 'control') == WEBAPP_FORM_CONTROL_LIST) || ($this->m_Config->GetConfigInfo($val, 'control') == WEBAPP_FORM_CONTROL_CHECK))
					{
						// 複数選択項目の場合
						if(is_array($_request[$val]))
						{
							foreach($_request[$val] as $inkey => $inval)
							{
								$this->m_Request[$val][$inkey] = RequestRegEx($inval, $this->m_IsMagicQuotes, $this->m_InEncoding);
							}
						}
					}else{
						// 一般項目
						$sTmp = "";
						$this->m_Request[$val] = RequestRegEx($_request[$val], $this->m_IsMagicQuotes, $this->m_InEncoding);

					}
				}
			}

			return true;
		}

		// -----------------------------------------------
		// コンバート
		// 入力チェック前の値変換
		// -----------------------------------------------
		function _convert()
		{
			if(is_array($this->m_Request))
			{
				foreach($this->m_Request as $key => $val)
				{
					$this->defaultConvert($key);
				}
			}
			return true;
		}

		// -----------------------------------------------
		// 入力チェック
		// -----------------------------------------------
		function InputValidation()
		{
			$errmsg = "";

			if(!is_array($this->m_Config->GetNameList()))
			{
				return true;
			}

			// メソッドチェック
			$current_method = $_SERVER['REQUEST_METHOD'];
			if(!eregi($current_method, $this->m_Method))
			{
				$errmsg .= $this->errMsgGen(CRT_ERRMSG_INVMETHOD, array());
				$this->_set_InvalidErrMsg($errmsg);
				$this->m_ErrStruct['_generic'][] = $this->errMsgGen(CRT_ERRMSG_INVMETHOD, array());
				return false;
			}


			// 必須入力チェック
			foreach($this->m_Config->GetNameList() as $key)
			{
				if($this->m_Config->GetConfigInfo($key, 'mandatory') == WEBAPP_FORM_MAND_YES)
				{
					if(($this->m_Config->GetConfigInfo($key, 'control') == WEBAPP_FORM_CONTROL_CHECK) || ($this->m_Config->GetConfigInfo($key, 'control') == WEBAPP_FORM_CONTROL_LIST))
					{
						// 複数値
						if(!is_array($this->m_Request[$key]))
						{
							$errmsg .= $this->errMsgGen(CRT_ERRMSG_NOSEL, array('name' => $this->m_Config->GetConfigInfo($key, 'label')));
							$this->m_ErrStruct[$key][] = $this->errMsgGen(CRT_ERRMSG_NOSEL, array('name' => $this->m_Config->GetConfigInfo($key, 'label')));
						}else{
							$flg_find = 0;
							foreach($this->m_Request[$key] as $tmpval)
							{
								if(Mis_empty($tmpval) != 1)
								{
									$flg_find = 1;
									break;
								}
							}
							if($flg_find == 0)
							{
								$errmsg .= $this->errMsgGen(CRT_ERRMSG_NOSEL, array('name' => $this->m_Config->GetConfigInfo($key, 'label')));
								$this->m_ErrStruct[$key][] = $this->errMsgGen(CRT_ERRMSG_NOSEL, array('name' => $this->m_Config->GetConfigInfo($key, 'label')));
							}
						}
					}else{
						// 単一値
						if(Mis_empty($this->m_Request[$key]) == 1)
						{
							$errmsg .= $this->errMsgGen(CRT_ERRMSG_NODATA, array('name' => $this->m_Config->GetConfigInfo($key, 'label')));
							$this->m_ErrStruct[$key][] = $this->errMsgGen(CRT_ERRMSG_NODATA, array('name' => $this->m_Config->GetConfigInfo($key, 'label')));
						}
					}
				}
			}


			// データ長チェック
			foreach($this->m_Config->GetNameList() as $key)
			{
				$chk_len = $this->m_Config->GetConfigInfo($key, 'maxlength');

				if(($this->m_Config->GetConfigInfo($key, 'control') == WEBAPP_FORM_CONTROL_CHECK) || ($this->m_Config->GetConfigInfo($key, 'control') == WEBAPP_FORM_CONTROL_LIST))
				{
					// 複数値の場合はチェックしない
					continue;
				}else if($this->m_Config->GetConfigInfo($key, 'control') == WEBAPP_FORM_CONTROL_FILE)
				{
					if(file_exists($_FILES[$key]['tmp_name']))
					{
						if(filesize($_FILES[$key]['tmp_name']) > $chk_len)
						{
							$errmsg .= $this->errMsgGen(CRT_ERRMSG_FILELEN, array('name' => $this->m_Config->GetConfigInfo($key, 'label'), 'len' => $chk_len));
							$this->m_ErrStruct[$key] = $this->errMsgGen(CRT_ERRMSG_FILELEN, array('name' => $this->m_Config->GetConfigInfo($key, 'label'), 'len' => $chk_len));
							// $errmsg .= $this->m_Config->GetConfigInfo($key, 'label') . 'が、ファイルサイズ制限（' . $chk_len . '）バイトを超えています<BR>';
						}
					}
				}else{
					if(strlen($this->m_Request[$key]) > $chk_len)
					{
						$zk_types = array(
							WEBAPP_FORM_VALIDATION_ZEN,
							WEBAPP_FORM_VALIDATION_HIRA,
							WEBAPP_FORM_VALIDATION_KATA,
							'0'
						);
						if(in_array($this->m_Config->GetConfigInfo($key, 'dtype'), $zk_types))
						{
							$len = (int)($chk_len / 3);
							$zh = '全角';
							$unit = '文字';
						}else{
							$len = $chk_len;
							$zh = '半角';
							$unit = '文字';
						}
						$errmsg .= $this->errMsgGen(CRT_ERRMSG_DATALEN, array('name' => $this->m_Config->GetConfigInfo($key, 'label'), 'len' => $len, 'unit' => $unit, 'zh' => $zh));
						$this->m_ErrStruct[$key][] = $this->errMsgGen(CRT_ERRMSG_DATALEN, array('name' => $this->m_Config->GetConfigInfo($key, 'label'), 'len' => $len, 'unit' => $unit));
						// $errmsg .= $this->m_Config->GetConfigInfo($key, 'label') . 'は、' . $chk_len . 'バイトまで入力できます<BR>';
					}
				}
			}

			// データ型チェック
			foreach($this->m_Config->GetNameList() as $key)
			{
				if(($this->m_Config->GetConfigInfo($key, 'control') == WEBAPP_FORM_CONTROL_CHECK) || ($this->m_Config->GetConfigInfo($key, 'control') == WEBAPP_FORM_CONTROL_LIST) || ($this->m_Config->GetConfigInfo($key, 'control') == WEBAPP_FORM_CONTROL_FILE))
				{
					// 複数値,ファイルの場合はチェックしない
					continue;
				}else{

					if($this->m_Config->GetConfigInfo($key, 'dtype') == WEBAPP_FORM_VALIDATION_NUM)
					{
						// 数値チェック
						if(Mis_empty($this->m_Request[$key]) != 1)
						{
							if(!Mchk_IsNumber($this->m_Request[$key]))
							{
								$errmsg .= $this->errMsgGen(CRT_ERRMSG_INVTYPE, array('name' => $this->m_Config->GetConfigInfo($key, 'label'), 'zhk' => '半角数字'));
								$this->m_ErrStruct[$key][] = $this->errMsgGen(CRT_ERRMSG_INVTYPE, array('name' => $this->m_Config->GetConfigInfo($key, 'label'), 'zhk' => '半角数字'));
							}
						}
					}

					if($this->m_Config->GetConfigInfo($key, 'dtype') == WEBAPP_FORM_VALIDATION_ALP)
					{
						// アルファベットのみ
						if(Mis_empty($this->m_Request[$key]) != 1)
						{
							if(Mchk_IsAlpha($this->m_Request[$key]) != 1)
							{
								$errmsg .= $this->errMsgGen(CRT_ERRMSG_INVTYPE, array('name' => $this->m_Config->GetConfigInfo($key, 'label'), 'zhk' => '半角英字'));
								$this->m_ErrStruct[$key][] = $this->errMsgGen(CRT_ERRMSG_INVTYPE, array('name' => $this->m_Config->GetConfigInfo($key, 'label'), 'zhk' => '半角英字'));
							}
						}
					}

					if($this->m_Config->GetConfigInfo($key, 'dtype') == WEBAPP_FORM_VALIDATION_ALN)
					{
						// アルファベットと数字
						if(Mis_empty($this->m_Request[$key]) != 1)
						{
							if(Mchk_IsAlNum($this->m_Request[$key]) != 1)
							{
								$errmsg .= $this->errMsgGen(CRT_ERRMSG_INVTYPE, array('name' => $this->m_Config->GetConfigInfo($key, 'label'), 'zhk' => '半角英数字'));
								$this->m_ErrStruct[$key][] = $this->errMsgGen(CRT_ERRMSG_INVTYPE, array('name' => $this->m_Config->GetConfigInfo($key, 'label'), 'zhk' => '半角英数字'));
							}
						}
					}

					if($this->m_Config->GetConfigInfo($key, 'dtype') == WEBAPP_FORM_VALIDATION_ASC)
					{
						// US-ASCII
						if(Mis_empty($this->m_Request[$key]) != 1)
						{
							if(Mchk_IsAlNumAll($this->m_Request[$key]) != 1)
							{
								$errmsg .= $this->errMsgGen(CRT_ERRMSG_INVTYPE, array('name' => $this->m_Config->GetConfigInfo($key, 'label'), 'zhk' => '半角英数字'));
								$this->m_ErrStruct[$key][] = $this->errMsgGen(CRT_ERRMSG_INVTYPE, array('name' => $this->m_Config->GetConfigInfo($key, 'label'), 'zhk' => '半角英数字'));
							}
						}
					}

					if($this->m_Config->GetConfigInfo($key, 'dtype') == WEBAPP_FORM_VALIDATION_MAIL)
					{
						// メールアドレス
						if(Mis_empty($this->m_Request[$key]) != 1)
						{
							if(Mchk_IsMailaddr($this->m_Request[$key]) != 1)
							{
								$errmsg .= $this->errMsgGen(CRT_ERRMSG_INVTYPE, array('name' => $this->m_Config->GetConfigInfo($key, 'label')));
								$this->m_ErrStruct[$key][] = $this->errMsgGen(CRT_ERRMSG_INVTYPE, array('name' => $this->m_Config->GetConfigInfo($key, 'label')));
							}
						}
					}

					if($this->m_Config->GetConfigInfo($key, 'dtype') == WEBAPP_FORM_VALIDATION_ZEN)
					{
						// 全角
						if(Mis_empty($this->m_Request[$key]) != 1)
						{
							if(Mchk_IsMultiByte($this->m_Request[$key]) != 1)
							{
								$errmsg .= $this->errMsgGen(CRT_ERRMSG_INVTYPE, array('name' => $this->m_Config->GetConfigInfo($key, 'label'), 'zhk' => '全角文字'));
								$this->m_ErrStruct[$key][] = $this->errMsgGen(CRT_ERRMSG_INVTYPE, array('name' => $this->m_Config->GetConfigInfo($key, 'label'), 'zhk' => '全角文字'));
							}
						}
					}

					if($this->m_Config->GetConfigInfo($key, 'dtype') == WEBAPP_FORM_VALIDATION_HIRA)
					{
						// 全角ひらがな
						if(Mis_empty($this->m_Request[$key]) != 1)
						{
							if(Mchk_IsHiragana($this->m_Request[$key]) != 1)
							{
								$errmsg .= $this->errMsgGen(CRT_ERRMSG_INVTYPE, array('name' => $this->m_Config->GetConfigInfo($key, 'label'), 'zhk' => '全角ひらがな'));
								$this->m_ErrStruct[$key][] = $this->errMsgGen(CRT_ERRMSG_INVTYPE, array('name' => $this->m_Config->GetConfigInfo($key, 'label'), 'zhk' => '全角ひらがな'));
							}
						}
					}

					if($this->m_Config->GetConfigInfo($key, 'dtype') == WEBAPP_FORM_VALIDATION_KATA)
					{
						// 全角カタカナ
						if(Mis_empty($this->m_Request[$key]) != 1)
						{
							if(Mchk_IsKatakana($this->m_Request[$key]) != 1)
							{
								$errmsg .= $this->errMsgGen(CRT_ERRMSG_INVTYPE, array('name' => $this->m_Config->GetConfigInfo($key, 'label'), 'zhk' => '全角カタカナ'));
								$this->m_ErrStruct[$key][] = $this->errMsgGen(CRT_ERRMSG_INVTYPE, array('name' => $this->m_Config->GetConfigInfo($key, 'label'), 'zhk' => '全角カタカナ'));
							}
						}
					}
				}
			}

			// 選択肢チェック
			foreach($this->m_Config->GetNameList() as $key)
			{
				if(
					($this->m_Config->GetConfigInfo($key, 'control') == WEBAPP_FORM_CONTROL_CHECK) ||
					($this->m_Config->GetConfigInfo($key, 'control') == WEBAPP_FORM_CONTROL_LIST)  ||
					($this->m_Config->GetConfigInfo($key, 'control') == WEBAPP_FORM_CONTROL_RADIO) ||
					($this->m_Config->GetConfigInfo($key, 'control') == WEBAPP_FORM_CONTROL_SELECT)
				){
					// 選択肢項目の場合
					$selectees = $this->m_Config->GetConfigInfo($key, 'selectees');

					if(is_array($this->m_Request[$key]))
					{
						foreach($this->m_Request[$key] as $selval)
						{
							if(Mis_empty($selectees[$selval]) == 1)
							{
								$errmsg .= $this->errMsgGen(CRT_ERRMSG_RANGE, array('name' => $this->m_Config->GetConfigInfo($key, 'label')));
								$this->m_ErrStruct[$key][] = $this->errMsgGen(CRT_ERRMSG_RANGE, array('name' => $this->m_Config->GetConfigInfo($key, 'label')));
								break;
							}
						}
					}else if(Mis_empty($this->m_Request[$key]) != 1)
					{
						if(Mis_empty($selectees[$this->m_Request[$key]]) == 1)
						{

							$errmsg .= $this->errMsgGen(CRT_ERRMSG_RANGE, array('name' => $this->m_Config->GetConfigInfo($key, 'label')));
							$this->m_ErrStruct[$key][] = $this->errMsgGen(CRT_ERRMSG_RANGE, array('name' => $this->m_Config->GetConfigInfo($key, 'label')));
							break;
						}
					}
				}
			}

			// カスタムエラーチェック
			foreach($this->m_Config->GetNameList() as $key)
			{
				$eMsg = "";
				if(!$this->customValidation($key, $eMsg))
				{
					$errmsg .= $eMsg;
					$this->m_ErrStruct[$key][] = $eMsg;
				}
			}


			// 追加のバリデーションチェック
			// 派生クラスで実装
			$errmsg = $this->additionalInputValidation($errmsg, 'SEL');
			if(Mis_empty($errmsg) != 1)
			{
				$this->_set_InvalidErrMsg($errmsg);
				return false;
			}

			return true;

		}

		// -----------------------------------------------
		// エラーメッセージ生成
		// メッセージを変更する場合はオーバーライトしてください。
		// -----------------------------------------------
		function errMsgGen($type, $params)
		{
			switch($type)
			{
				case CRT_ERRMSG_NODATA:
					$sysmsg = '<$name$>をご入力ください<br />';
					break;
				case CRT_ERRMSG_NOSEL:
					$sysmsg = '<$name$>が選択されていません<br />';
					break;
				case CRT_ERRMSG_INVTYPE:
					if($params['name'] == 'tel')
					{
						$sysmsg = '<$name$>は半角数字、ハイフンで入力してください<br />';
					}else if(Mis_empty($params['zhk']) != 1)
					{
						$sysmsg = '<$name$>は' . $params['zhk'] . 'で入力してください<br />';
					}else{
						$sysmsg = '<$name$>の形式が正しくありません<br />';
					}
					break;
				case CRT_ERRMSG_DATALEN:
					$sysmsg = '<$name$>は<$zh$><$len$><$unit$>まで入力できます<br />';
					break;
				case CRT_ERRMSG_FILELEN:
					$sysmsg = '<$name$>がファイルサイズ制限（<$len$>バイト）を超えています<br />';
					break;
				case CRT_ERRMSG_RANGE:
					$sysmsg = '<$name$>に想定外の入力を検出しました<br />';
					break;
				case CRT_ERRMSG_INVEXT:
					$sysmsg = 'アップロードされたファイルの拡張子が正しくありません（<$name$>）<br />';
					break;
				case CRT_ERRMSG_INVMETHOD:
					$sysmsg = '許可されていないメソッドアクセスです<br />';
					break;
				default:
					$sysmsg = '予期しないエラー<br />';
					break;
			}

			$msg = new CTagTemplate($sysmsg, CTAGTEMPLATE_INTYPE_DATA, CTAGTEMPLATE_ENCODE_UTF8);
			if(is_array($params))
			{
				foreach($params as $name => $value)
				{
					$msg->Replace($name, $value);
				}
			}

			return $msg->Generate();
		}



		// -----------------------------------------------
		// コンテキスト返却
		// -----------------------------------------------
		function getContext()
		{
			return $this->m_Result;
		}

		// -----------------------------------------------
		// デバッグオブジェクト返却
		// -----------------------------------------------
		function getLabyrinthos()
		{
			return $this->m_Laby;
		}

		// -----------------------------------------------
		// 内部リダイレクト
		// 別のモジュールのViewクラスを呼び出す
		// -----------------------------------------------
		function viewRedirect($task, $module, $params)
		{
			$this->m_Result['_redirect'] = true;
			$this->m_Result['_task'] = $task;
			$this->m_Result['_module'] = $module;
			if(is_array($params))

			{
				foreach($params as $key => $val)
				{
					$this->m_Result[$key] = $val;
				}
			}

			return true;
		}


		// -----------------------------------------------
		// バリデーションエラー設定
		function _set_InvalidErrMsg($msg)
		{
			$this->m_InvalidErrMsg .= $msg;
		}

		// -----------------------------------------------
		// 設定ファイル読み込み
		function _read_config()
		{
			if(!$this->m_Config->ReadConfig())
			{
				$this->_set_error($this->m_Config->GetError());
				return false;
			}

			return true;
		}

		// 処理実行（ビジネスロジック）
		// サブクラスで実装
		function Execute()
		{
			// この関数でm_Result 配列に結果表示用のデータを作成


			// パラメータコピー
			if(is_array($this->m_Request))
			{
				foreach($this->m_Request as $key => $val)
				{
					$this->m_Result[$key] = $val;
				}
			}

			// 標準入力チェック
			if(!$this->inputValidation())
			{
				return $this->handleInvalidate();
			}

			return true;

		}

		// カスタムバリデーションチェッカ
		// サブクラスで実装
		function customValidation($key, &$msg)
		{
			// カスタム入力チェック
			return true;
		}
		// デフォルトコンバータ
		// サブクラスで実装
		function defaultConvert($name)
		{
			// 入力チェック前の変換
		}
		// バリデーションエラーハンドラ
		// サブクラスで実装
		function handleInvalidate()
		{
			// 入力エラーがあった場合の処理を記述
		}
		// 一般エラーハンドラ
		// サブクラスで実装
		function handleError()
		{
			// ユーザー通知エラーの処理を記述
		}
		// システムエラーハンドラ
		// サブクラスで実装
		function handleSysError()
		{
			// システムエラーの処理を記述
		}

	} // End of class CBaseWebapp.definition.

?>