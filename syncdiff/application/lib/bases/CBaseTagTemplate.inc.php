<?php

	// -------------------------------------------------------
	// CBaseTagTemplate.inc
	// CBaseTagTemplateクラス
	// 効率のよい置換ルーチンとコンテナ置換を実装した
	// 簡易テンプレートエンジン基底クラス
	// 2005.04.05 のぐち
	// -------------------------------------------------------

	define("CTAGTEMPLATE_INTYPE_FILE", 1, FALSE);
	define("CTAGTEMPLATE_INTYPE_DATA", 2, FALSE);

	// ショートカット
	define("CTT_FILE", 1, FALSE);
	define("CTT_DATA", 2, FALSE);

	define("CTAGTEMPLATE_ENCODE_EUC", 'EUC-JP', FALSE);
	define("CTAGTEMPLATE_ENCODE_JIS", 'JIS', FALSE);
	define("CTAGTEMPLATE_ENCODE_SJIS", 'SJIS', FALSE);
	define("CTAGTEMPLATE_ENCODE_UTF8", 'UTF-8', FALSE);

	// ショートカット
	define("CTT_EUC", 'EUC-JP', FALSE);
	define("CTT_JIS", 'JIS', FALSE);
	define("CTT_SJIS", 'SJIS', FALSE);
	define("CTT_UTF8", 'UTF-8', FALSE);
	define("CTT_UTF-8", 'UTF-8', FALSE);

	define("CTAGTEMPLATE_PH_START", '<\$', FALSE);
	define("CTAGTEMPLATE_PH_END", '\$>', FALSE);
	define("CTAGTEMPLATE_PHR_START", '<$', FALSE);
	define("CTAGTEMPLATE_PHR_END", '$>', FALSE);



	class CBaseTagTemplate extends CBaseObject
	{

		// メンバー変数
		var $m_Body;			// スカラ格納
		var $m_Lines;			// 細切れデータ
		var $m_PlaceHolders;	// PH位置
		var $m_PHNames;			// PH名と位置の関係
		var $m_NameCache;		// キャッシュ名

		var $m_BaseEncode;		// 文字エンコーディング


		function CBaseTagTemplate($indata, $intype, $enc, $cache="")
		{
			// エンコーディング
			$this->m_BaseEncode = $enc;

			if($intype == CTAGTEMPLATE_INTYPE_FILE)
			{
				if(Mis_empty($cache) != 1)
				{
					$this->m_NameCache = $cache;
					// キャッシュを読むか再読込か
					if(file_exists(XD_CACHE_DIR . '/' . $this->m_NameCache . '.php'))
					{
						// 最終更新時刻を取得
						$cachefile_stat = stat(XD_CACHE_DIR . '/' . $this->m_NameCache . '.php');
						$tmplfile_stat = stat($indata);

						if($cachefile_stat['mtime'] >= $tmplfile_stat['mtime'])
						{
							// キャッシュを読み込む
							require_once(XD_CACHE_DIR . '/' . $this->m_NameCache . '.php');
							return;
						}
					}
				}
			}



			if($intype == CTAGTEMPLATE_INTYPE_FILE)
			{
				if(!file_exists($indata))
				{
					$this->_set_error('No such file');
					return;
				}

				$fp = fopen($indata, "r");
				if(!$fp)
				{
					$this->_set_error('File I/O Exception for ' . $indata);
					return;
				}
				while($line = fgets($fp, 4096))
				{
					if($this->m_BaseEncode != CTAGTEMPLATE_ENCODE_UTF8)
					{
						$line = mb_convert_encoding($line, CTAGTEMPLATE_ENCODE_UTF8, $this->m_BaseEncode);
					}
					$this->m_Body .= $line;
				}
				fclose($fp);

			}else if($intype == CTAGTEMPLATE_INTYPE_DATA)
			{
				// 文字コード調整
				if($this->m_BaseEncode != CTAGTEMPLATE_ENCODE_UTF8)
				{
					$sTmp = mb_convert_encoding($indata, CTAGTEMPLATE_ENCODE_UTF8, $this->m_BaseEncode);
				}else{
					$sTmp = $indata;
				}
				$this->m_Body = $sTmp;
			}else{
				$this->_set_error('Invaid INTYPE code');
				return;
			}


			$this->Split();

			return;
		}

		// プレースホルダーの抜き出し
		function Split()
		{
			unset($this->m_Lines);
			unset($this->m_PlaceHolders);
			unset($matches);

			// 走査位置
			$current_pos = 0;

			// 検証文字列（初回は全体）
			$testee_str = $this->m_Body;

			// 「<$」と、「$>」で挟まれた文字とその前後を切り分ける
			while(ereg(CTAGTEMPLATE_PH_START . '[^<>]+' . CTAGTEMPLATE_PH_END, $testee_str, $matches))
			{
				// PHよりも前の文字列
				$strpart_b = substr($testee_str, 0, strpos($testee_str, $matches[0]));
				// PHよりも後ろの文字列
				$strpart_a = substr($testee_str, strpos($testee_str, $matches[0]) + strlen($matches[0]));

				$this->m_Lines[$current_pos] = $strpart_b;
				$current_pos++;
				$this->m_Lines[$current_pos] = $matches[0];

				// プレースホルダ位置の記憶
				$this->m_PlaceHolders[] = $current_pos;

				// プレースホルダの位置と名前の記憶
				$phpos = array();
				//$phpos['name'] = substr($matches[0], strlen(CTAGTEMPLATE_PH_START), strlen($matches[0]) - (strlen(CTAGTEMPLATE_PH_START) + strlen(CTAGTEMPLATE_PH_END)));
				//$phpos['name'] = substr($matches[0], strlen(CTAGTEMPLATE_PH_START), strlen($matches[0]) - 4);
				$phpos['name'] = $matches[0];
				$phpos['pos'] = $current_pos;
				$this->m_PHNames[] = $phpos;

				// インクリメント
				$current_pos++;

				// 走査文字列の置換
				$testee_str = $strpart_a;
			}

			if($testee_str != "")
			{
				$this->m_Lines[] = $testee_str;
			}

			// キャッシュ作成
			if(Mis_empty($this->m_NameCache) != 1)
			{
				$cachefile = new CCacheCtrl($this->m_NameCache);
				$cachefile->makeCache(array('this', 'm_Lines'), $this->m_Lines);
				$cachefile->makeCache(array('this', 'm_PlaceHolders'), $this->m_PlaceHolders);
				$cachefile->makeCache(array('this', 'm_PHNames'), $this->m_PHNames);
				$cachefile->flush();
			}


		}

		// コンテナ取得(複数)
		function GetContainerAll($ph)
		{
			$start_pos = "";
			$end_pos = "";
			if(is_array($this->m_PlaceHolders))
			{
				$cnt=0;
				foreach($this->m_PlaceHolders as $pos)
				{
					if(eregi(CTAGTEMPLATE_PH_START . $ph . CTAGTEMPLATE_PH_END, $this->m_Lines[$pos]))
					{
						$cnt++;
						$start_pos[$cnt] = $pos;
					}

					if(eregi(CTAGTEMPLATE_PH_START . '/' . $ph . CTAGTEMPLATE_PH_END, $this->m_Lines[$pos]))
					{
						$end_pos[$cnt] = $pos;
						//break;
					}
				}
			}

			if((!is_array($start_pos)) || (!is_array($end_pos)))
			{
				return "";
			}

			//同じ名前の数
			$count = count($start_pos);

			if(is_array($this->m_Lines))
			{
				for($i = 1; $i<=$count ; $i++)
				{
					$strRet = "";
					for($iCnt = ($start_pos[$i] + 1); $iCnt <= ($end_pos[$i] - 1); $iCnt++)
					{
						$strRet .= $this->m_Lines[$iCnt];
					}
					$strRetlist[$i] = $strRet;
				}
			}

			return $strRetlist;
		}

		// コンテナ置換(複数)
		function ReplaceContainerAll($ph, $repto)
		{
			$start_pos = "";
			$end_pos = "";

			if(is_array($this->m_PlaceHolders))
			{
				$cnt=0;
				foreach($this->m_PlaceHolders as $pos)
				{
					if(eregi(CTAGTEMPLATE_PH_START . $ph . CTAGTEMPLATE_PH_END, $this->m_Lines[$pos]))
					{
						$cnt++;
						$start_pos[$cnt] = $pos;
					}

					if(eregi(CTAGTEMPLATE_PH_START . '/' . $ph . CTAGTEMPLATE_PH_END, $this->m_Lines[$pos]))
					{
						$end_pos[$cnt] = $pos;
					}
				}
			}


			if((!is_array($start_pos)) || (!is_array($end_pos)))
			{
				return false;
			}

			if(is_array($this->m_Lines))
			{
				$count = count($start_pos);

				for($i = 1 ; $i <= $count ; $i++)
				{
					for($iCnt = $start_pos[$i]; $iCnt <= $end_pos[$i]; $iCnt++)
					{
						$this->m_Lines[$iCnt] = "";
					}
				}
			}

			for($i=1 ; $i<=$count ; $i++)
			{
				$this->m_Lines[$start_pos[$i]] = $repto[$i];
			}

			return true;
		}

		// コンテナ取得
		function GetContainer($ph)
		{
			$start_pos = "";
			$end_pos = "";
			if(is_array($this->m_PlaceHolders))
			{
				foreach($this->m_PlaceHolders as $pos)
				{
					if(eregi(CTAGTEMPLATE_PH_START . $ph . CTAGTEMPLATE_PH_END, $this->m_Lines[$pos]))
					{
						$start_pos = $pos;
					}

					if(eregi(CTAGTEMPLATE_PH_START . '/' . $ph . CTAGTEMPLATE_PH_END, $this->m_Lines[$pos]))
					{
						$end_pos = $pos;
						break;
					}
				}
			}

			if(($start_pos == "") || ($end_pos == ""))
			{
				return "";
			}

			$strRet = "";
			if(is_array($this->m_Lines))
			{
				for($iCnt = ($start_pos + 1); $iCnt <= ($end_pos - 1); $iCnt++)
				{
					$strRet .= $this->m_Lines[$iCnt];
				}
			}

			return $strRet;
		}

		// コンテナ置換
		function ReplaceContainer($ph, $repto)
		{
			$start_pos = "";
			$end_pos = "";
			if(is_array($this->m_PlaceHolders))
			{
				foreach($this->m_PlaceHolders as $pos)
				{
					if(eregi(CTAGTEMPLATE_PH_START . $ph . CTAGTEMPLATE_PH_END, $this->m_Lines[$pos]))
					{
						$start_pos = $pos;
					}

					if(eregi(CTAGTEMPLATE_PH_START . '/' . $ph . CTAGTEMPLATE_PH_END, $this->m_Lines[$pos]))
					{
						$end_pos = $pos;
						break;
					}
				}
			}

			if(($start_pos == "") || ($end_pos == ""))
			{
				return false;
			}

			if(is_array($this->m_Lines))
			{
				for($iCnt = $start_pos; $iCnt <= $end_pos; $iCnt++)
				{
					$this->m_Lines[$iCnt] = "";
				}
			}
			$this->m_Lines[$start_pos] = $repto;

			return true;
		}

		// 置換処理
		function Replace($ph, $repto)
		{
			if(is_array($this->m_PlaceHolders))
			{
				foreach($this->m_PlaceHolders as $pos)
				{
					$this->m_Lines[$pos] = str_replace(CTAGTEMPLATE_PHR_START . $ph . CTAGTEMPLATE_PHR_END, $repto, $this->m_Lines[$pos]);
				}
			}
		}
		// 共通置換処理
		function CommonReplace()
		{
		}

		// <$xxx$> <$/xxx$> ブロックをそのまま表示（複数）
		function ContainerActivateAll($ph)
		{
			$area = $this->GetContainerAll($ph);
			$this->ReplaceContainerAll($ph, $area);
		}

		// <$xxx$> <$/xxx$> を非表示（複数）
		function ContainerDeactivateAll($ph)
		{
			$this->ReplaceContainerAll($ph, '');
		}

		// <$xxx$> <$/xxx$> ブロックをそのまま表示
		function ContainerActivate($ph)
		{
			$area = $this->GetContainer($ph);
			$this->ReplaceContainer($ph, $area);
		}
		// <$xxx$> <$/xxx$> を非表示
		function ContainerDeactivate($ph)
		{
			$this->ReplaceContainer($ph, '');
		}



		// 書き出し
		// EUC書き出し
		function GenerateEUC()
		{
			$sRet = "";
			if(is_array($this->m_Lines))
			{
				foreach($this->m_Lines as $line)
				{
					$sRet .= $line;
				}
			}

			$sRet = mb_convert_encoding($sRet, 'EUC-JP', $this->m_BaseEncode);
			return $sRet;
		}

		// 文字コード書き出し複数
		function GenerateAll($enc = "")
		{
			if(Mis_empty($enc) == 1)
			{
				$enc = $this->m_BaseEncode;
			}
			$sRet = "";
			if(is_array($this->m_Lines))
			{
				foreach($this->m_Lines as $lines)
				{
					foreach($lines as $line)
					{
						$sRet .= $line;
					}

				}
			}

			if($this->m_BaseEncode != CTAGTEMPLATE_ENCODE_UTF8)
			{
				$sRet = mb_convert_encoding($sRet, $this->m_BaseEncode, CTAGTEMPLATE_ENCODE_UTF8);
			}


			return $sRet;
		}


		// 文字コード書き出し
		function Generate($enc = "")
		{
			if(Mis_empty($enc) == 1)
			{
				$enc = $this->m_BaseEncode;
			}
			$sRet = "";
			if(is_array($this->m_Lines))
			{
				foreach($this->m_Lines as $line)
				{
					$sRet .= $line;
				}
			}

			if($this->m_BaseEncode != CTAGTEMPLATE_ENCODE_UTF8)
			{
				$sRet = mb_convert_encoding($sRet, $this->m_BaseEncode, CTAGTEMPLATE_ENCODE_UTF8);
			}

			return $sRet;
		}

		// HTML書き出し
		function PrintHTML()
		{
			if($this->m_BaseEncode == CTAGTEMPLATE_ENCODE_UTF8)
			{
				header("Content-type: text/html;charset=UTF-8");
			}elseif($this->m_BaseEncode == CTAGTEMPLATE_ENCODE_EUC)
			{
				header("Content-type: text/html;charset=EUC-JP");
			}elseif($this->m_BaseEncode == CTAGTEMPLATE_ENCODE_SJIS)
			{
				header("Content-type: text/html;charset=Shift_JIS");
			}else if($this->m_BaseEncode == CTAGTEMPLATE_ENCODE_JIS)
			{
				header("Content-type: text/html;charset=iso-2022-jp");
			}

			echo $this->Generate();
		}


	} // End of class CBaseTagTemplate definition.

?>