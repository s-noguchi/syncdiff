<?php

	// -----------------------------------------------
	// CBaseObject.inc
	// PHP Webアプリケーションフレームワーク
	// 基底クラス
	// 2005.04.10
	// -----------------------------------------------

	class CBaseObject
	{

		// メンバー変数
		var $m_IsError;			// エラーフラグ
		var $m_Error;			// エラー文字列

		// コンストラクタ
		function CBaseObject()
		{
			$this->m_IsError = 0;
			$this->m_Error = "";
		}

		// エラー取得
		function IsError()
		{
			return $this->m_IsError;
		}
		function GetError()
		{
			return $this->m_Error;
		}
		// エラー設定
		function _set_error($msg = "", $loglevel = 1)
		{

			$this->m_IsError = 1;
			$this->m_Error .= $msg;

			trigger_error('onSetError:' . $msg);

		}

	} // End of class CBaseObject definition.

?>