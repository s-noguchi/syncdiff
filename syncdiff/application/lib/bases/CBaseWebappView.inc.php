<?php

	// -----------------------------------------------
	// CBaseWebappView.inc
	// PHP Webアプリケーションフレームワークベースクラス
	// 2005.04.10
	// -----------------------------------------------

	class CBaseWebappView extends CBaseObject
	{

		var $m_Result;			// 結果データ

		var $m_Template;		// 出力テンプレートオブジェクト

		var $m_Config;			// 入力用フォーム設定オブジェクト
		var $m_ConfigFile;		// 入力用設定ファイル
		var $m_TemplateFile;	// テンプレートファイル

		var $m_TemplateEncoding;

		var $m_Laby;			// デバッグオブジェクト

		// -----------------------------------------------
		// コンストラクタ
		// -----------------------------------------------
		function CBaseWebappView($result, $config_file, $template_file, &$test_obj)
		{
			global $g_name_task, $g_name_module;

			// スーパークラスコンストラクタ
			$this->CBaseObject();

			// 初期化
			unset($this->m_Data);
			unset($this->m_Config);
			unset($this->m_Template);

			// フォーム設定オブジェクト
			$this->m_Config = new CConfigFile();
			if((file_exists($config_file)) && (Mis_empty($config_file) != 1))
			{
				$this->m_ConfigFile = $config_file;
				$this->m_Config->SetConfigFile($this->m_ConfigFile);
				$this->m_Config->SetConfigNames(XD_CACHE_PREFIX_CONFIG_OUT . md5(XD_SUBSYS_ROOT), $g_name_task, $g_name_module);

			}

			// テンプレート
			if((file_exists($template_file)) && (Mis_empty($template_file) != 1))
			{
				$this->m_TemplateFile = $template_file;
			}

			// デバッグオブジェクト
			$this->m_Laby = $test_obj;


			// 結果オブジェクトの格納
			$this->m_Result = $result;

			return;
		}

		// -----------------------------------------------
		// フォーム定義ファイル設定
		// -----------------------------------------------
		function SetConfigFile($config_file)
		{
			if(Mis_empty($config_file) == 1)
			{
				$this->_set_error('Empty config_file parameter.');
				return false;
			}
			if(!file_exists($config_file))
			{
				$this->_set_error('No configuration file.');
				return false;
			}

			$this->m_ConfigFile = $config_file;
			$this->m_Config->SetConfigFile($this->m_ConfigFile);

			return true;
		}

		// -----------------------------------------------
		// フォーム情報の設定
		// -----------------------------------------------
		function SetFormConfig($name, $params)
		{
			$this->m_Config->SetConfigInfo($name, $params);
			return true;
		}

		// -----------------------------------------------
		// デバッグオブジェクトの取得
		// -----------------------------------------------
		function getLabyrinthos()
		{
			return $this->m_Laby;
		}

		// -----------------------------------------------
		// 処理メインフロー
		// -----------------------------------------------
		function doTask()
		{
			// フォーム初期設定
			if(!$this->initParam())
			{
				$this->handleError();
			}

			// テンプレートオブジェクト作成
			if(!$this->createTemplate())
			{
				$this->handleError();
			}

			// 処理実行
			if(!$this->Display())
			{
				$this->handleError();
			}
		}

		function SetTemplateEncoding($enc)
		{
			$this->m_TemplateEncoding = $enc;
		}

		// -----------------------------------------------
		// パラメータイニシャライズ
		// -----------------------------------------------
		function initParam()
		{
			if((file_exists($this->m_ConfigFile)) && (Mis_empty($this->m_ConfigFile) != 1))
			{
				if(!$this->_read_config())
				{
					return false;
				}
			}


			return true;
		}

		// -----------------------------------------------
		// テンプレートオブジェクト作成
		// -----------------------------------------------
		function createTemplate()
		{
			global $g_name_task, $g_name_module;

			// テンプレートがある場合はオブジェクト作成
			if(Mis_empty($this->m_TemplateFile) != 1)
			{
				if(file_exists($this->m_TemplateFile))
				{
					if(Mis_empty($this->m_TemplateEncoding) == 1)
					{
						$this->m_TemplateEncoding = CTAGTEMPLATE_ENCODE_SJIS;
					}
					// キャッシュ名
					$cachename = XD_CACHE_PREFIX_TEMPLATE . md5(XD_SUBSYS_ROOT) . '_' . $g_name_task . '_' . $g_name_module;
					// この関数でm_Result 配列に結果表示用のデータを作成
					$this->m_Template = new CTagTemplate($this->m_TemplateFile, CTAGTEMPLATE_INTYPE_FILE, $this->m_TemplateEncoding, $cachename);

				}
			}

			return true;

		}

		// -----------------------------------------------
		// フォーム用置換ルーチン
		// -----------------------------------------------
		function DispForm($tmpl, $ignores)
		{

			if(is_array($this->m_Config->GetNameList()))
			{

				foreach($this->m_Config->GetNameList() as $colname)
				{
					// 置換しないタグが指定されていれば次へ
					if(is_array($ignores))
					{
						if(in_array($colname, $ignores))
						{
							continue;
						}
					}

					if($this->m_Config->GetConfigInfo($colname, 'control') == WEBAPP_FORM_CONTROL_RADIO)
					{
						// ラジオボタンの場合（新）
						if(is_array($this->m_Config->GetConfigInfo($colname, 'selectees')))
						{
							foreach(array_keys($this->m_Config->GetConfigInfo($colname, 'selectees')) as $radio_value)
							{
								$tag = $colname . '_' . $radio_value;
								if($this->m_Result[$colname] == $radio_value)
								{
									$tmpl->Replace($tag, ' checked="checked" ');
								}else{
									$tmpl->Replace($tag, '');
								}
							}

						}

						// ラジオボタンの場合（旧）
						// $parts = $this->_html_radio($this->m_Config->GetConfigInfo($colname, 'selectees'), $colname, $this->m_Result[$colname]);
						// $tmpl->Replace($colname, $parts);
						continue;
					}

					if($this->m_Config->GetConfigInfo($colname, 'control') == WEBAPP_FORM_CONTROL_CHECK)
					{
						// チェックボックスの場合（新）
						if(is_array($this->m_Config->GetConfigInfo($colname, 'selectees')))
						{
							foreach(array_keys($this->m_Config->GetConfigInfo($colname, 'selectees')) as $check_value)
							{
								$tag = $colname . '_' . $check_value;
								if((is_array($this->m_Result[$colname])) && (in_array($check_value, $this->m_Result[$colname])))
								{
									$tmpl->Replace($tag, ' checked="checked" ');
								}else{
									$tmpl->Replace($tag, '');
								}
							}

						}

						// チェックボックスの場合（旧）
						// $parts = $this->_html_check($this->m_Config->GetConfigInfo($colname, 'selectees'), $colname, $this->m_Result[$colname]);
						// $tmpl->Replace($colname, $parts);
						continue;
					}

					if($this->m_Config->GetConfigInfo($colname, 'control') == WEBAPP_FORM_CONTROL_SELECT)
					{
						// プルダウンの場合
						$parts = $this->_html_select($this->m_Config->GetConfigInfo($colname, 'selectees'), $colname, $this->m_Result[$colname]);
						$tmpl->Replace($colname, $parts);
						continue;
					}

					if($this->m_Config->GetConfigInfo($colname, 'control') == WEBAPP_FORM_CONTROL_LIST)
					{
						// リストボックスの場合
						$parts = $this->_html_list($this->m_Config->GetConfigInfo($colname, 'selectees'), $colname, $this->m_Result[$colname]);
						$tmpl->Replace($colname, $parts);
						continue;
					}

					if($this->m_Config->GetConfigInfo($colname, 'control') == WEBAPP_FORM_CONTROL_FILE)
					{
						// ファイルアップロードの場合
						$parts = $this->_html_file($colname, $this->m_Result[$colname]);
						$tmpl->Replace($colname, $parts);
						continue;
					}

					// 上記以外のテキストコントロール
					$tmpl->Replace($colname, htmlspecialchars($this->m_Result[$colname], ENT_QUOTES));
					continue;
				}
			}

			return $tmpl;
		}

		// -----------------------------------------------
		// 確認用置換ルーチン
		// -----------------------------------------------
		function DispConf($tmpl, $ignores)
		{

			if(is_array($this->m_Config->GetNameList()))
			{
				foreach($this->m_Config->GetNameList() as $colname)
				{
					// 置換しないタグが指定されていれば次へ
					if(is_array($ignores))
					{
						if(in_array($colname, $ignores))
						{
							continue;
						}
					}

					if($this->m_Config->GetConfigInfo($colname, 'control') == WEBAPP_FORM_CONTROL_RADIO)
					{
						// ラジオボタンの場合
						$tmpl->Replace($colname, $this->_html_radio_disp($this->m_Config->GetConfigInfo($colname, 'selectees'), $this->m_Result[$colname]));
						continue;
					}

					if($this->m_Config->GetConfigInfo($colname, 'control') == WEBAPP_FORM_CONTROL_CHECK)
					{
						// チェックボックスの場合
						$tmpl->Replace($colname, $this->_html_check_disp($this->m_Config->GetConfigInfo($colname, 'selectees'), $this->m_Result[$colname]));
						continue;
					}

					if($this->m_Config->GetConfigInfo($colname, 'control') == WEBAPP_FORM_CONTROL_SELECT)
					{
						// プルダウンの場合
						$tmpl->Replace($colname, $this->_html_select_disp($this->m_Config->GetConfigInfo($colname, 'selectees'), $this->m_Result[$colname]));
						continue;
					}

					if($this->m_Config->GetConfigInfo($colname, 'control') == WEBAPP_FORM_CONTROL_LIST)
					{
						// リストボックスの場合
						$tmpl->Replace($colname, $this->_html_list_disp($this->m_Config->GetConfigInfo($colname, 'selectees'), $this->m_Result[$colname]));
						continue;
					}

					if($this->m_Config->GetConfigInfo($colname, 'control') == WEBAPP_FORM_CONTROL_FILE)
					{
						// ファイルアップロードの場合
						$tmpl->Replace($colname, $this->_html_file_disp($this->m_Result[$colname]));
						continue;
					}

					if($this->m_Config->GetConfigInfo($colname, 'control') == WEBAPP_FORM_CONTROL_TEXTAREA)
					{
						// テキストエリアの場合（改行処理）
						$tmpl->Replace($colname, Mhtmlspecialchars($this->m_Result[$colname]));
						continue;
					}

					// 上記以外のテキストコントロール
					$tmpl->Replace($colname, htmlspecialchars($this->m_Result[$colname], ENT_QUOTES));
					continue;
				}
			}

			return $tmpl;
		}


		// -----------------------------------------------
		// HIDDEN値の作成
		// -----------------------------------------------
		function GetHiddenTag($hidden, $ignores)
		{

			if(is_array($this->m_Config->GetNameList()))
			{
				foreach($this->m_Config->GetNameList() as $colname)
				{
					// 置換しないタグが指定されていれば次へ
					if(is_array($ignores))
					{
						if(in_array($colname, $ignores))
						{
							continue;
						}
					}

					if(($this->m_Config->GetConfigInfo($colname, 'control') == WEBAPP_FORM_CONTROL_CHECK) || ($this->m_Config->GetConfigInfo($colname, 'control') == WEBAPP_FORM_CONTROL_LIST))
					{
						if(is_array($this->m_Result[$colname]))
						{
							foreach($this->m_Result[$colname] as $multival)
							{
								$hidden->Add($colname . '[]', $multival);
							}
						}
					}else{
						$hidden->Add($colname, $this->m_Result[$colname]);
					}
				}
			}

			return $hidden;
		}

		// -----------------------------------------------
		// バリデーションエラー設定
		function _set_InvalidErrMsg($msg)
		{
			$this->m_InvalidErrMsg .= $msg;
		}

		// -----------------------------------------------
		// 設定ファイル読み込み
		function _read_config()
		{
			if(!$this->m_Config->ReadConfig())
			{
				$this->_set_error($this->m_Config->GetError());
				return false;
			}

			return true;
		}

		// -----------------------------------------------
		// 自動置換
		// $tmpl テンプレートエンジンオブジェクト
		// $data 連想多重配列
		function AutoReplace(&$tmpl, &$data)
		{
			// data は多重配列
			if(is_array($data))
			{
				foreach($data as $key => $val)
				{
					if(!is_array($val))
					{
						// 単一
						$rep_name = array($key);
						$this->ReplaceCallback($rep_name, $key, $val, $tmpl);
					}else{
						// 複数
						$this->_replaceMultiple($rep_name, $key, $val, $tmpl);
					}
				}
			}
		}

		function _replaceMultiple($names, $name, $value, &$tmpl)
		{
			// テンプレートパーツ
			$entry_master = $tmpl->GetContainer($name);

			// 行数ループ
			$strLines = "";
			if(is_array($value))
			{
				foreach($value as $line_id => $line)
				{
					// 行テンプレート準備
					$entry = new CTagTemplate($entry_master, CTAGTEMPLATE_INTYPE_DATA, CTAGTEMPLATE_ENCODE_EUC);

					// 列数ループ
					if(is_array($line))
					{
						foreach($line as $repkey => $repval)
						{
							$keys = $names;
							$keys[] = $line_id;
							$keys[] = $repkey;

							// 置換コールバック
							$this->ReplaceCallback($keys, $repkey, $repval, $entry);
						}
					}
					$strLines .= $entry->Generate();
				}
			}

			$this->ReplaceContainerCallback($names, $name, $strLines, $tmpl);
		}


		// -----------------------------------------------
		// 自動置換コールバック
		// $names 連想配列キーの配列
		// $value 置換する値
		// $tmpl テンプレートエンジンオブジェクト
		//
		// ※ここに記載されいているのはデフォルトの動作で、
		// ※オーバーライドして利用します。
		function ReplaceCallback($names, $name, $value, &$tmpl)
		{
			$tmpl->Replace($name, htmlspecialchars($value));
			return;
		}
		// -----------------------------------------------
		// 自動置換コールバック （コンテナ）
		// $names 連想配列キーの配列
		// $value 置換する値
		// $tmpl テンプレートエンジンオブジェクト
		//
		// ※ここに記載されいているのはデフォルトの動作で、
		// ※オーバーライドして利用します。
		function ReplaceContainerCallback($names, $name, $value, &$tmpl)
		{
			$tmpl->ReplaceContainer($name, $value);
			return;
		}


		// -----------------------------------------------
		// HTMLパーツ生成
		// ラジオボタン
		function _html_radio($pairs, $name, $value)
		{
			$radio = "";
			foreach($pairs as $key => $val)
			{
				if($key == $value)
				{
					$radio .= '<input type="radio" name="' . $name . '" value="' . $key . '" checked>' . htmlspecialchars($val) . '<br>';
				}else{
					$radio .= '<input type="radio" name="' . $name . '" value="' . $key . '">' . htmlspecialchars($val) . '<br>';
				}
			}
			return $radio;
		}
		// -----------------------------------------------
		// HTMLパーツ生成（確認）
		// ラジオボタン
		function _html_radio_disp($pairs, $value)
		{
			return htmlspecialchars($pairs[$value]);
		}

		// -----------------------------------------------
		// HTMLパーツ生成
		// チェックボックス
		function _html_check($pairs, $name, $value)
		{
			$check = "";
			foreach($pairs as $key => $val)
			{
				if(is_array($value))
				{
					if(in_array($key, $value))
					{
						$check .= '<input type="checkbox" name="' . $name . '[]" value="' . $key . '" checked>' . htmlspecialchars($val) . '<br>';
					}else{
						$check .= '<input type="checkbox" name="' . $name . '[]" value="' . $key . '">' . htmlspecialchars($val) . '<br>';
					}
				}else{
					$check .= '<input type="checkbox" name="' . $name . '[]" value="' . $key . '">' . htmlspecialchars($val) . '<br>';
				}
			}
			return $check;
		}
		// -----------------------------------------------
		// HTMLパーツ生成（確認）
		// チェックボックス
		function _html_check_disp($pairs, $value)
		{
			$sTmp = "";
			if(is_array($value))
			{
				foreach($value as $val)
				{
					$sTmp .= htmlspecialchars($pairs[$val]) . '<BR>';
				}
			}
			return $sTmp;
		}

		// -----------------------------------------------
		// HTMLパーツ生成
		// プルダウン
		function _html_select($pairs, $name, $value)
		{
			$select = '<select name="' . $name . '">' . "\n";
			$select .= '<option value=""></option>' . "\n";

			if(is_array($pairs))
			{
				foreach($pairs as $key => $val)
				{
					if($key == $value)
					{
						$select .= '<option value="' . $key . '" selected="selected" >' . htmlspecialchars($val) . '</option>' . "\n";
					}else{
						$select .= '<option value="' . $key . '">' . htmlspecialchars($val) . '</option>' . "\n";
					}
				}
			}
			$select .= '</select>';
			return $select;
		}
		// -----------------------------------------------
		// HTMLパーツ生成（確認）
		// プルダウン
		function _html_select_disp($pairs, $value)
		{
			return htmlspecialchars($pairs[$value]);
		}

		// -----------------------------------------------
		// HTMLパーツ生成
		// リスト
		function _html_list($pairs, $name, $value)
		{
			$select = '<select name="' . $name . '[]" size="5" multiple>' . "\n";

			foreach($pairs as $key => $val)
			{
				if(is_array($value))
				{
					if(in_array($key, $value))
					{
						$select .= '<option value="' . $key . '" selected="selected" >' . htmlspecialchars($val) . '</option>' . "\n";
					}else{
						$select .= '<option value="' . $key . '">' . htmlspecialchars($val) . '</option>' . "\n";

					}
				}else{
					$select .= '<option value="' . $key . '">' . htmlspecialchars($val) . '</option>' . "\n";
				}
			}
			$select .= '</select>';
			return $select;
		}
		// -----------------------------------------------
		// HTMLパーツ生成（確認）
		// リスト
		function _html_list_disp($pairs, $value)
		{
			$sTmp = "";
			if(is_array($value))
			{
				foreach($value as $val)
				{
					$sTmp .= htmlspecialchars($pairs[$val]) . '<BR>';
				}
			}
			return $sTmp;
		}

		// -----------------------------------------------
		// HTMLパーツ生成
		// ファイル
		// ※この関数は基本的に抽象関数的にサンプル記述しますので、適宜
		// ※オーバーライトしてください
		function _html_file($name, $value)
		{
			$sTag = "";
			if(Mis_empty($value) != 1)
			{
				$sTag .= '<a href="' . $uri . '/' . $value . '">ダウンロード</a><br>';
				$sTag .= '<input type="checkbox" name="del_' . $name . '" value="1">削除する<br>';
			}
			$sTag .= '<input type="file" name="' . $name . '">';

			return $sTag;
		}
		// -----------------------------------------------
		// HTMLパーツ生成（確認）
		// ファイル
		// ※この関数は基本的に抽象関数的にサンプル記述しますので、適宜
		// ※オーバーライトしてください
		function _html_file_disp($value)
		{
			$sTag = "";
			if(Mis_empty($value) != 1)
			{
				$sTag = '<a href="' . $uri . '/' . $value . '">確認</a>';
			}else{
				$sTag = "";
			}

			return $sTag;
		}



		// 表示処理（View）
		// サブクラスで実装
		function Display()
		{

			// 自動置換
			// $this->AutoReplace($this->m_Template, $this->m_Result);


		}
		// バリデーションエラーハンドラ
		// サブクラスで実装
		function handleInvalidate()
		{
			// 入力エラーがあった場合の処理を記述
		}
		// 一般エラーハンドラ
		// サブクラスで実装
		function handleError()
		{
			// ユーザー通知エラーの処理を記述
		}
		// システムエラーハンドラ
		// サブクラスで実装
		function handleSysError()
		{
			// システムエラーの処理を記述
		}

	} // End of class CBaseWebapp.definition.

?>