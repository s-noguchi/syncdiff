<?php

	// -----------------------------------------------
	// CBaseWebappDispatch.inc
	// PHP Webアプリケーションフレームワークベースクラス
	// 2005.04.10
	// -----------------------------------------------



	class CBaseWebappDispatch extends CBaseObject
	{

		var $m_Task;			// タスク名
		var $m_Module;			// モジュール名
		var $m_Names;			// クラス一覧

		var $m_ClassId;			// クラスID
		var $m_ClassFileLogic;	// ロジッククラスファイル名
		var $m_ClassFileView;	// ビュークラスファイル名

		var $m_Config_IN;		// INパラメータ定義ファイル
		var $m_Config_OUT;		// OUTパラメータ定義ファイル

		var $m_TemplateFile;	// テンプレートファイル

		var $m_Logic;			// ロジックオブジェクト
		var $m_View;			// ビューオブジェクト
		var $m_FlgMark;			// フラグマーク

		var $m_Labyrinthos;		// ロジックオブジェクトテストケース

		function CBaseWebappDispatch($task, $module, $names)
		{
			if((Mis_empty($task) == 1) || (Mis_empty($module) == 1))
			{
				$this->_set_error('no task or module specified');
				return;
			}


			$this->m_Task = $task;
			$this->m_Module = $module;
			$this->m_Names = $names;

			$tmp_array = split(';', $this->m_Names[$this->m_Task][$this->m_Module]);
			if(!is_array($tmp_array))
			{
				$this->_set_error('no task or module specified');
				return;
			}

			$this->m_ClassId = $tmp_array[0];
			$this->m_FlgMark = $tmp_array[1];

			$this->m_ClassFileLogic = XD_SUBSYS_DIR . '/' . $this->m_ClassId . '/CLogic.inc.php';
			$this->m_ClassFileView = XD_SUBSYS_DIR . '/' . $this->m_ClassId . '/CView.inc.php';
			$this->m_ClassFileTest = XD_SUBSYS_DIR . '/' . $this->m_ClassId . '/CTest.inc.php';
			$this->m_Config_IN = XD_SUBSYS_DIR . '/' . $this->m_ClassId . '/' . $this->m_ClassId . '_in.cfg';
			$this->m_Config_OUT = XD_SUBSYS_DIR . '/' . $this->m_ClassId . '/' . $this->m_ClassId . '_out.cfg';
			$this->m_TemplateFile = XD_SUBSYS_DIR . '/' . $this->m_ClassId . '/' . $this->m_ClassId . '.html';


		}

		function dispatchTest()
		{
			require $this->m_ClassFileTest;

		}

		function dispatchLogic()
		{
			require $this->m_ClassFileLogic;

			$this->m_Logic = new CLogic($this->m_Config_IN);
			$this->m_Labyrinthos = $this->m_Logic->getLabyrinthos();
			return $this->m_Logic->getContext();
		}

		function dispatchView($context)
		{
			require $this->m_ClassFileView;
			$this->m_View = new CView($context, $this->m_Config_OUT, $this->m_TemplateFile, $this->m_Labyrinthos);
			$this->m_Labyrinthos = $this->m_View->getLabyrinthos();
//			$this->m_Labyrinthos->Terminate();
			return $this->m_View->IsError();
		}
	}
?>