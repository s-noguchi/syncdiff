<?php

	// -----------------------------------------------
	// CBaseWebapp.inc
	// PHP Webアプリケーションフレームワークベースクラス
	// 2005.04.10
	// -----------------------------------------------

	class CBaseWebappLabyrinthos extends CBaseObject
	{

		var $m_Ariadne;			// アリアドネのスレッド（通過ログ）
		var $m_Minotaur;		// アサーション結果


		// -----------------------------------------------
		// コンストラクタ
		// -----------------------------------------------
		function CBaseWebappLabyrinthos()
		{
			unset($this->m_Ariadne);
			unset($this->m_Minotaur);

			return;
		}


		// -----------------------------------------------
		// 通過ログ追記（別名）
		// -----------------------------------------------
		function mark($msg, &$ext)
		{
			$this->markThread($msg, $ext);
		}
		// -----------------------------------------------
		// 通過ログ追記
		// -----------------------------------------------
		function markThread($msg, &$ext)
		{
			unset($thobj);
			$thobj['msg'] = $msg;
			$thobj['timestamp'] = date('Y-m-d H:i:s');
			if(is_array($ext))
			{
				foreach($ext as $key => $val)
				{
					if(is_array($val))
					{
						$sVal = "";
						foreach($val as $inkey => $inval)
						{
							$sVal .= $inkey . '=>' . $inval . ";";
						}
					}else{
						$sVal = $val;
					}

					$thobj['ext'][$key] = $sVal;
				}
			}else{
				$thobj['ext'] = $ext;
			}

			$this->m_Ariadne[] = $thobj;

		}

		function getAriadne()
		{
			return $this->m_Ariadne;
		}

		// 終了時に実行される
		function Terminate()
		{
			return true;
		}

	} // End of class CBaseWebappLabyrinthos definition.

?>