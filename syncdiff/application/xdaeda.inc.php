<?php

	// xdaeda.inc.php
	// フレームワーク構成ファイル定義

	// 当ファイル名
	define("XD_BASE_INCLUSION", 'xdaeda.inc.php', FALSE);
	
	
	// Timezone;
	date_default_timezone_set('Asia/Tokyo');
	

	// 当ディレクトリ
	$g_xdeada_dir = str_replace(XD_BASE_INCLUSION, '', __FILE__);
	$g_xdeada_dir = preg_replace('/\/$/si', "", $g_xdeada_dir);
	
	
	// PHP5暫定対応
	$HTTP_GET_VARS = $_GET;
	$HTTP_POST_VARS = $_POST;
	$HTTP_SERVER_VARS = $_SERVER;
	$HTTP_ENV_VARS = $_ENV;
	$HTTP_COOKIE_VARS  = $_COOKIE;
	$HTTP_POST_FILES = $_FILES;
	if (!empty($_SESSION)){
		$HTTP_SESSION_VARS = $_SESSION;
	}else{
		$HTTP_SESSION_VARS = array();
	}



	// アプリケーション固有クラス
	define("XD_APPCLASS_DIR", $g_xdeada_dir . '/classes', FALSE);
	// データフォルダ
	define("XD_DATA_DIR", $g_xdeada_dir . '/data', FALSE);
	// 列挙型（ラベル）フォルダ
	define("XD_LABEL_DIR", $g_xdeada_dir . '/labels', FALSE);
	// フレームワークライブラリフォルダ
	define("XD_LIB_DIR", $g_xdeada_dir . '/lib', FALSE);
	// フレームワークベースディレクトリ
	define("XD_BASE_DIR", XD_LIB_DIR . '/bases', FALSE);
	// フレームワーク汎用ディレクトリ
	define("XD_COMMON_DIR", XD_LIB_DIR . '/common', FALSE);
	// フレームワーク汎用クラスディレクトリ
	define("XD_CLASSES_DIR", XD_COMMON_DIR . '/classes', FALSE);
	// フレームワーク汎用ユーティリティディレクトリ
	define("XD_UTIL_DIR", XD_COMMON_DIR . '/utils', FALSE);
	// モジュールディレクトリ
	define("XD_MODULES_DIR", $g_xdeada_dir . '/modules', FALSE);
	// スクリプトディレクトリ
	define("XD_SCRIPT_DIR", $g_xdeada_dir . '/script', FALSE);
	// 共通テンプレートディレクトリ
	define("XD_TEMPLATE_DIR", $g_xdeada_dir . '/template', FALSE);
	// 一時ディレクトリ
	define("XD_TEMP_DIR", $g_xdeada_dir . '/tmp', FALSE);
	// キャッシュディレクトリ
	define("XD_CACHE_DIR", XD_TEMP_DIR . '/cache', FALSE);

	



	// 定数定義
	require_once XD_LIB_DIR . '/const.inc.php';


	// フレームワークベースクラス
	require_once XD_BASE_DIR . '/CBaseObject.inc.php';
	require_once XD_BASE_DIR . '/CBaseTagTemplate.inc.php';
	require_once XD_BASE_DIR . '/CBaseWebappDispatch.inc.php';
	require_once XD_BASE_DIR . '/CBaseWebappLogic.inc.php';
	require_once XD_BASE_DIR . '/CBaseWebappView.inc.php';
	require_once XD_BASE_DIR . '/CBaseWebappLabyrinthos.inc.php';

	// 中間フレームワークベースクラス
	require_once XD_MODULES_DIR . '/CCommonTagTemplate.inc.php';
	require_once XD_MODULES_DIR . '/CCommonWebappDispatch.inc.php';
	require_once XD_MODULES_DIR . '/CCommonWebappLabyrinthos.inc.php';
	require_once XD_MODULES_DIR . '/CCommonWebappLogic.inc.php';
	require_once XD_MODULES_DIR . '/CCommonWebappView.inc.php';


	// フレームワークユーティリティ
	require_once XD_UTIL_DIR . '/cmn_util.inc.php';
	//require_once XD_UTIL_DIR . '/WA_Auth.inc.php';
	require_once XD_UTIL_DIR . '/error.inc.php';
	require_once XD_CLASSES_DIR . '/CConfigFile.inc.php';
	require_once XD_CLASSES_DIR . '/CMailSend.inc.php';
	require_once XD_CLASSES_DIR . '/CMailSendEx.inc.php';
	require_once XD_CLASSES_DIR . '/CHidden.inc.php';
	require_once XD_CLASSES_DIR . '/CCacheCtrl.inc.php';
	require_once XD_CLASSES_DIR . '/CFileMgr.inc.php';

	



?>
