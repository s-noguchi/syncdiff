<?php
	/**
	 * webapp.php
	 *
	 * メイン実行ファイルです。ブラウザから直接呼び出されるのは
	 * このPHPスクリプトです。
	 *
	 **/

	set_time_limit(0);
	setlocale(LC_ALL, 'ja_JP.UTF-8');

	/**
	 * PHP設定
	 * 主にiniパラメータやコールバックを設定
	 */
	ini_set('mbstring.http_input', 'pass');
	ini_set('mbstring.http_output', 'pass');
	ini_set('mbstring.script_encoding', 'UTF-8');
	ini_set('display_errors', 'On');
	ini_set('error_reporting',E_ERROR | E_WARNING | E_PARSE);

	/**
	 * フレームワークルート
	 * 通常「application」フォルダの位置を記入
	 */
	define("XD_ROOT", './application');
	
	/**
	 * サブシステムルート
	 * 各サブシステムのモジュールフォルダを記入
	 */
	define("XD_SUBSYS_ROOT", '/sample');




	// インクルード
	require_once XD_ROOT . '/xdaeda.inc.php';
	// サブシステムルート
	require_once XD_MODULES_DIR . XD_SUBSYS_ROOT . '/subsystem.inc.php';


	// コンテキスト取得
	$path_info = $_SERVER['PATH_INFO'];
	$path_info = ereg_replace("^/", "", $path_info);
	$path_info = ereg_replace("/$", "", $path_info);
	$ctx_array = split("/", $path_info);
	$name_task = $ctx_array[0];
	$name_module = $ctx_array[1];

	// ---------------------------------------------------------------
	// モジュール定義配列 デフォルトコンテキスト

	require_once XD_SUBSYS_DIR . '/modules.inc.php';
	// ---------------------------------------------------------------

	// モジュールない場合
	if(Mis_empty($DISPATCH_CLASS_NAMES[$name_task][$name_module]) == 1)
	{
		issue_404();
		exit;
	}

	// タスク名／モジュール名／フラグを格納
	// フラグは、カテゴリのような使い方で、DB接続処理やセッションの省略などの
	// 処理わけに利用する

	$g_name_task = $name_task;
	$g_name_module = $name_module;
	$ltmpar = split(';', $DISPATCH_CLASS_NAMES[$name_task][$name_module]);
	if(is_array($ltmpar))
	{
		$g_name_flgmark = $ltmpar[1];
	}else{
		$g_name_flgmark = "";
	}

	// セッション関連
	require_once(XD_UTIL_DIR . '/session.inc.php');

	// データベース関連
	require_once(XD_UTIL_DIR . '/db.inc.php');


	// モジュール呼び出し
	$dispatcher = new CWebappDispatch($name_task, $name_module);
	$ariadne = $dispatcher->dispatchTest();
	$context = $dispatcher->dispatchLogic();

	// 内部リダイレクト
	if($context['_redirect'] == 1)
	{
		$name_task = $context['_task'];
		$name_module = $context['_module'];
		$g_name_task = $name_task;
		$g_name_module = $name_module;

		$dispatcher = new CWebappDispatch($name_task, $name_module);
	}

	$dispatcher->dispatchView($context);
	exit;



?>